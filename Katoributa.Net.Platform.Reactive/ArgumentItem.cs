﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive
{
    /// <summary>
    /// 引数付きの通知項目。
    /// </summary>
    /// <typeparam name="T">引数の型。</typeparam>
    public class ArgumentItem<T> : NotificationItem
    {
        /// <summary>
        /// ArgumentItemのインスタンスを初期化します。
        /// </summary>
        public ArgumentItem()
        {
        }

        /// <summary>
        /// コードと引数を指定してArgumentItemのインスタンスを初期化します。
        /// </summary>
        /// <param name="code">コード。</param>
        /// <param name="args">引数。</param>
        public ArgumentItem(string code, T args)
            : base(code)
        {
            this.Args = args;
        }

        /// <summary>
        /// 引数。
        /// </summary>
        public T Args { get; set; }
    }
}
