﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive
{
    /// <summary>
    /// 進捗を通知する項目。
    /// </summary>
    public class ProgressNotificationItem : NotificationItem
    {
        /// <summary>
        /// ProgressNotificationItemのインスタンスを初期化します。
        /// </summary>
        public ProgressNotificationItem()
        {
        }

        /// <summary>
        /// 各種要素を指定してProgressNotificationItemのインスタンスを初期化します。
        /// </summary>
        /// <param name="code">コード。</param>
        /// <param name="progressName">進捗名。</param>
        /// <param name="totalCount">総数。</param>
        /// <param name="finishedCount">終了した件数。</param>
        /// <param name="finishedItem">終了した項目。</param>
        public ProgressNotificationItem(string code, string progressName, int totalCount, int finishedCount, object finishedItem)
            : base(code)
        {
            this.ProgressName = progressName;
            this.TotalCount = totalCount;
            this.FinishedCount = finishedCount;
            this.FinishedItem = finishedItem;
        }

        /// <summary>
        /// 進捗名。
        /// </summary>
        public string ProgressName { get; set; }

        /// <summary>
        /// 総数。
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 終了した件数。
        /// </summary>
        public int FinishedCount { get; set; }

        /// <summary>
        /// 終了した項目。
        /// </summary>
        public object FinishedItem { get; set; }
    }
}
