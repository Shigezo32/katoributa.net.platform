﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive.Extensions
{
    /// <summary>
    /// INotifyCollectionChangedに対して追加する便利機能群です。
    /// </summary>
    public static class INotifyCollectionChangedExtension
    {
        /// <summary>
        /// CollectionChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="notifyCollectionChanged">INotifyCollectionChanged。</param>
        /// <returns>IObservable&lt;NotifyCollectionChangedEventArgs&gt;。</returns>
        public static IObservable<NotifyCollectionChangedEventArgs> CollectionChangedAsObservable(this INotifyCollectionChanged notifyCollectionChanged)
        {
            return Observable.FromEventPattern<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                        h => h.Invoke,
                        h => notifyCollectionChanged.CollectionChanged += h, h => notifyCollectionChanged.CollectionChanged -= h)
                    .Select(x => x.EventArgs);
        }
    }
}
