﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive.Extensions
{
    /// <summary>
    /// INotifyPropertyChangedに対して追加する便利機能群です。
    /// </summary>
    public static class INotifyPropertyChangedExtension
    {
        /// <summary>
        /// PropertyChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="notifyPropertyChanged">INotifyPropertyChanged。</param>
        /// <returns>IObservable&lt;PropertyChangedEventArgs&gt;。</returns>
        public static IObservable<PropertyChangedEventArgs> PropertyChangedAsObservable(this INotifyPropertyChanged notifyPropertyChanged)
        {
            return Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
                        h => h.Invoke,
                        h => notifyPropertyChanged.PropertyChanged += h, h => notifyPropertyChanged.PropertyChanged -= h)
                    .Select(x => x.EventArgs);
        }

        /// <summary>
        /// PropertyChangedイベントのEventPatternをObservableに変換します。
        /// </summary>
        /// <param name="notifyPropertyChanged">INotifyPropertyChanged。</param>
        /// <returns>IObservable&lt;EventPattern&lt;PropertyChangedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<PropertyChangedEventArgs>> PropertyChangedEventPatternAsObservable(this INotifyPropertyChanged notifyPropertyChanged)
        {
            return Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
                        h => h.Invoke,
                        h => notifyPropertyChanged.PropertyChanged += h, h => notifyPropertyChanged.PropertyChanged -= h)
                    .Select(x => x);
        }
    }
}
