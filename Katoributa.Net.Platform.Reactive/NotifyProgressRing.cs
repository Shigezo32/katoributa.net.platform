﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive
{
    /// <summary>
    /// ProgressRingに通知するための機能を有します。
    /// </summary>
    public class NotifyProgressRing : IDisposable
    {
        /// <summary>
        /// このインスタンスがDispose済みかどうかを表すフラグ。
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// NotifyProgressRingのインスタンスを初期化します。
        /// </summary>
        private NotifyProgressRing()
        {
            Mediator.GetObserver().OnNext(ProgressRingNotificationItem.ShowProgressRingNotificationItem);
        }

        /// <summary>
        /// ProgressRingを表示します。
        /// </summary>
        /// <returns>ProgressRingを非表示にするためのIDisposable。</returns>
        public static IDisposable Show()
        {
            return new NotifyProgressRing();
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            _disposed = true;
            if (disposing)
            {
                // マネージリソースの解放処理
                Mediator.GetObserver().OnNext(ProgressRingNotificationItem.HideProgressRingNotificationItem);
            }
            // アンマネージリソースの解放処理
        }
    }
}
