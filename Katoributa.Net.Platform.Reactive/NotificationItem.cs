﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive
{
    /// <summary>
    /// 通知項目。
    /// </summary>
    public class NotificationItem
    {
        /// <summary>
        /// NotificationItemのインスタンスを初期化します。
        /// </summary>
        public NotificationItem()
        {
        }

        /// <summary>
        /// コードを指定してNotificationItemのインスタンスを初期化します。
        /// </summary>
        /// <param name="code">コード。</param>
        public NotificationItem(string code)
            : this()
        {
            this.Code = code;
        }

        /// <summary>
        /// コード。
        /// </summary>
        public string Code { get; set; }
    }
}
