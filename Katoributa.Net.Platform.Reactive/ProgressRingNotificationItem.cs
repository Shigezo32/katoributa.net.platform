﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive
{
    /// <summary>
    /// ProgressRingに関係する通知項目。
    /// </summary>
    public class ProgressRingNotificationItem : NotificationItem
    {
        /// <summary>
        /// コードを指定してProgressRingNotificationItemのインスタンスを初期化します
        /// </summary>
        /// <param name="code">コード。</param>
        private ProgressRingNotificationItem(string code)
            : base(code)
        {
        }

        /// <summary>
        /// ProgressRingを表示するための通知項目。
        /// </summary>
        public static ProgressRingNotificationItem ShowProgressRingNotificationItem
        {
            get
            {
                if (_showProgressRingNotificationItem == null)
                {
                    _showProgressRingNotificationItem = new ProgressRingNotificationItem("ShowProgressRing");
                }
                return _showProgressRingNotificationItem;
            }
        }
        private static ProgressRingNotificationItem _showProgressRingNotificationItem;

        /// <summary>
        /// ProgressRingを隠すための通知項目。
        /// </summary>
        public static ProgressRingNotificationItem HideProgressRingNotificationItem
        {
            get
            {
                if (_hideProgressRingNotificationItem == null)
                {
                    _hideProgressRingNotificationItem = new ProgressRingNotificationItem("HideProgressRing");
                }
                return _hideProgressRingNotificationItem;
            }
        }
        private static ProgressRingNotificationItem _hideProgressRingNotificationItem;
    }
}
