﻿using System;
using System.Collections.Concurrent;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Katoributa.Net.Platform.Reactive
{
    /// <summary>
    /// 仲介者。
    /// </summary>
    public static class Mediator
    {
        /// <summary>
        /// 標準で使用されるKey。
        /// </summary>
        private static readonly object _defaultKey = new Object();

        /// <summary>
        /// Subjectを管理するDictionary。
        /// </summary>
        private static readonly ConcurrentDictionary<object, Subject<NotificationItem>> _subjectDictionary = new ConcurrentDictionary<object, Subject<NotificationItem>>();

        /// <summary>
        /// Observerを取得します。
        /// </summary>
        /// <returns>Observer。</returns>
        public static IObserver<NotificationItem> GetObserver()
        {
            return GetObserver(_defaultKey);
        }

        /// <summary>
        /// Keyを指定してObserverを取得します。
        /// </summary>
        /// <param name="key">Key。</param>
        /// <returns>Observer。</returns>
        public static IObserver<NotificationItem> GetObserver(object key)
        {
            if (_subjectDictionary.ContainsKey(key))
            {
                return _subjectDictionary[key];
            }
            var subject = new Subject<NotificationItem>();
            if (!_subjectDictionary.TryAdd(key, subject))
            {
                return _subjectDictionary[key];
            }
            return subject;
        }

        /// <summary>
        /// Observableを取得します。
        /// </summary>
        /// <returns>Observable。</returns>
        public static IObservable<NotificationItem> GetObservable()
        {
            return GetObservable(_defaultKey);
        }

        /// <summary>
        /// Keyを指定してObservableを取得します。
        /// </summary>
        /// <param name="key">Key。</param>
        /// <returns>Observable。</returns>
        public static IObservable<NotificationItem> GetObservable(object key)
        {
            var subject = GetObserver(key) as Subject<NotificationItem>;
            return subject.AsObservable();
        }

        /// <summary>
        /// Observer/Observableを解除します。
        /// </summary>
        public static void Detach()
        {
            Detach(_defaultKey);
        }

        /// <summary>
        /// Keyを指定してObserver/Observableを解除します。
        /// </summary>
        /// <param name="key">Key。</param>
        public static void Detach(object key)
        {
            if (_subjectDictionary.ContainsKey(key))
            {
                Subject<NotificationItem> subject = null;
                if (_subjectDictionary.TryRemove(key, out subject))
                {
                    subject.Dispose();
                }
            }
        }
    }
}
