﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Reactive
{
    /// <summary>
    /// メッセージ項目。
    /// </summary>
    public class MessageItem : NotificationItem
    {
        /// <summary>
        /// MessageItemのインスタンスを初期化します。
        /// </summary>
        public MessageItem()
        {
        }

        /// <summary>
        /// コードとメッセージを指定してMessageItemのインスタンスを初期化します。
        /// </summary>
        /// <param name="code">コード。</param>
        /// <param name="message">メッセージ。</param>
        public MessageItem(string code, string message)
            : base(code)
        {
            this.Message = message;
        }

        /// <summary>
        /// メッセージ。
        /// </summary>
        public string Message { get; set; }
    }
}
