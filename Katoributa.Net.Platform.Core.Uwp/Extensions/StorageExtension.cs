﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// ストレージに関して追加する便利機能群です。
    /// </summary>
    public static class StorageExtension
    {
        /// <summary>
        /// 指定したフォルダが存在しているかどうかを確認します。
        /// </summary>
        /// <param name="target">基準となるStorageFolder。</param>
        /// <param name="name">確認するサブフォルダー名 (または現在のフォルダーを基準とするパス)。</param>
        /// <returns>サブフォルダーが存在する場合はtrue、存在しない場合はfalse。</returns>
        public static async Task<bool> ExistsFolder(this StorageFolder target, string name)
        {
            try
            {
                var folder = await target.GetFolderAsync(name);
                return true;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }

        /// <summary>
        /// 指定したファイルが存在しているかどうかを確認します。
        /// </summary>
        /// <param name="target">基準となるStorageFolder。</param>
        /// <param name="name">確認するファイル名 (または現在のフォルダーを基準とするパス)。</param>
        /// <returns>ファイルが存在する場合はtrue、存在しない場合はfalse。</returns>
        public static async Task<bool> ExistsFile(this StorageFolder target, string name)
        {
            try
            {
                var folder = await target.GetFileAsync(name);
                return true;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }
    }
}
