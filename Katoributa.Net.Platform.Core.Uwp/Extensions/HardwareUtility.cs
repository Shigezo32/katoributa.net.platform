﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.System.Profile;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// ハードウェアに関係する便利メソッド群です。
    /// </summary>
    public class HardwareUtility : IHardwareUtility
    {
        /// <summary>
        /// 現在のハードウェアを表すハードウェア識別子を取得します。
        /// </summary>
        /// <returns>ハードウェア識別子。</returns>
        public string GetHardwareId()
        {
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var dataReader = DataReader.FromBuffer(token.Id);
            var buffer = new byte[token.Id.Length];
            dataReader.ReadBytes(buffer);
            return BitConverter.ToString(buffer);
        }
    }
}
