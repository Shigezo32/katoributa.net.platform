﻿using Windows.Networking.Connectivity;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// ネットワークに関係する便利メソッド群です。
    /// </summary>
    public class NetworkUtility : INetworkUtility
    {
        /// <summary>
        /// インターネットへ接続しているかどうかを表します。
        /// </summary>
        /// <returns>インターネットに接続している場合はtrue。それ以外の場合はfalse。</returns>
        public bool IsInternetAccess()
        {
            foreach (var profile in NetworkInformation.GetConnectionProfiles())
            {
                var level = profile.GetNetworkConnectivityLevel();
                switch (level)
                {
                    case NetworkConnectivityLevel.ConstrainedInternetAccess:
                    case NetworkConnectivityLevel.InternetAccess:
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// ネットワークへ接続しているかどうかを表します。
        /// </summary>
        /// <returns>ネットワークに接続している場合はtrue。それ以外の場合はfalse。</returns>
        public bool IsNetworkAccess()
        {
            foreach (var profile in NetworkInformation.GetConnectionProfiles())
            {
                var level = profile.GetNetworkConnectivityLevel();
                switch (level)
                {
                    case NetworkConnectivityLevel.ConstrainedInternetAccess:
                    case NetworkConnectivityLevel.InternetAccess:
                    case NetworkConnectivityLevel.LocalAccess:
                        return true;
                }
            }
            return false;
        }
    }
}
