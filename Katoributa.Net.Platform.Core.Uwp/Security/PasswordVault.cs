﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Security
{
    /// <summary>
    /// 資格情報用の資格情報保管ボックスを表します。
    /// </summary>
    public class PasswordVault : IPasswordVault
    {
        /// <summary>
        /// 実際に利用するPasswordVault。
        /// </summary>
        private readonly Windows.Security.Credentials.PasswordVault _passwordVault = new Windows.Security.Credentials.PasswordVault();

        /// <summary>
        /// PasswordVaultのインスタンスを作成します。
        /// </summary>
        public PasswordVault()
        {
        }

        /// <summary>
        /// 資格情報保管ボックスに資格情報を追加します。
        /// </summary>
        /// <param name="credential">追加する資格情報。</param>
        public void Add(PasswordCredential credential)
        {
            _passwordVault.Add(credential.ToOriginal());
        }

        /// <summary>
        /// 指定したリソースに一致する資格情報に対応する資格情報保管ボックスを検索します。
        /// </summary>
        /// <param name="resource">検索対象のリソース。</param>
        /// <returns>資格情報オブジェクトの一覧。</returns>
        public IReadOnlyList<PasswordCredential> FindAllByResource(string resource)
        {
            return new ReadOnlyCollection<PasswordCredential>(_passwordVault.FindAllByResource(resource).Select(x => x.ToReplica()).ToList());
        }

        /// <summary>
        /// 資格情報保管ボックスから資格情報を削除します。
        /// </summary>
        /// <param name="credential">削除する資格情報。</param>
        public void Remove(PasswordCredential credential)
        {
            if (string.IsNullOrEmpty(credential.Password))
            {
                _passwordVault.Remove(_passwordVault.Retrieve(credential.Resource, credential.UserName));
            }
            else
            {
                _passwordVault.Remove(credential.ToOriginal());
            }
        }

        /// <summary>
        /// 資格情報保管ボックスから資格情報を読み取ります。
        /// </summary>
        /// <param name="resource">資格情報が使用されるリソース。</param>
        /// <param name="userName">資格情報に含まれていなければならないユーザー名。</param>
        /// <returns>すべてのデータを含む返された資格情報。</returns>
        public PasswordCredential Retrieve(string resource, string userName)
        {
            return _passwordVault.Retrieve(resource, userName).ToReplica();
        }
    }
}
