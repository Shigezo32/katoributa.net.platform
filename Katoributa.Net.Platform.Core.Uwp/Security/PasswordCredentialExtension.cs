﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Security
{
    /// <summary>
    /// PasswordCredentialに対して追加する便利機能群です。
    /// </summary>
    internal static class PasswordCredentialExtension
    {
        /// <summary>
        /// 指定したPasswordCredentialをオリジナルに変換します。
        /// </summary>
        /// <param name="target"><see cref="PasswordCredential"/>。</param>
        public static Windows.Security.Credentials.PasswordCredential ToOriginal(this PasswordCredential target)
        {
            if (target == null)
            {
                return null;
            }
            return new Windows.Security.Credentials.PasswordCredential(target.Resource, target.UserName, target.Password);
        }

        /// <summary>
        /// 指定したPasswordCredentialを複製に変換します。
        /// </summary>
        /// <param name="target"><see cref="Windows.Security.Credentials.PasswordCredential"/>。</param>
        public static PasswordCredential ToReplica(this Windows.Security.Credentials.PasswordCredential target)
        {
            if (target == null)
            {
                return null;
            }
            return new PasswordCredential(target.Resource, target.UserName, target.Password);
        }
    }
}
