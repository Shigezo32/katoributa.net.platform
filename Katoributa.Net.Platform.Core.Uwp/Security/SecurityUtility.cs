﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;

namespace Katoributa.Net.Platform.Core.Security
{
    /// <summary>
    /// セキュリティに関係する便利メソッド群です。
    /// </summary>
    public class SecurityUtility : ISecurityUtility
    {
        /// <summary>
        /// SecurityUtilityのインスタンスを初期化します。
        /// </summary>
        public SecurityUtility()
        {
            SaltKey = "Katoributa.Net.Platform.Core";
        }

        /// <summary>
        /// ソルトを作るための秘密の文字列。
        /// </summary>
        public string SaltKey { get; set; }

        /// <summary>
        /// パスワード用のSaltを取得します。
        /// </summary>
        /// <param name="id">ID。</param>
        /// <returns>パスワード用のSalt。</returns>
        public string GetSalt(string id)
        {
            return SaltKey + id;
        }

        /// <summary>
        /// 指定した文字列のSHA1を計算します。
        /// </summary>
        /// <param name="value">SHA1を計算する文字列。</param>
        /// <returns>SHA1。</returns>
        public byte[] ToSha1Hash(string value)
        {
            var algorithm = HashAlgorithmProvider.OpenAlgorithm("SHA1");
            var buffer = CryptographicBuffer.ConvertStringToBinary(value, BinaryStringEncoding.Utf8);
            var hash = algorithm.HashData(buffer);
            return WindowsRuntimeBufferExtensions.ToArray(hash);
        }

        /// <summary>
        /// 指定したバイト配列のSHA256を計算します。
        /// </summary>
        /// <param name="array">SHA256を計算するバイト配列。</param>
        /// <returns>SHA256。</returns>
        public byte[] ToSha256Hash(byte[] array)
        {
            var algorithm = HashAlgorithmProvider.OpenAlgorithm("SHA256");
            var buffer = WindowsRuntimeBufferExtensions.AsBuffer(array);
            var hash = algorithm.HashData(buffer);
            return WindowsRuntimeBufferExtensions.ToArray(hash);
        }

        /// <summary>
        /// 指定した文字列のSHA256を計算します。
        /// </summary>
        /// <param name="value">SHA256を計算する文字列。</param>
        /// <returns>SHA256。</returns>
        public byte[] ToSha256Hash(string value)
        {
            var algorithm = HashAlgorithmProvider.OpenAlgorithm("SHA256");
            var buffer = CryptographicBuffer.ConvertStringToBinary(value, BinaryStringEncoding.Utf8);
            var hash = algorithm.HashData(buffer);
            return WindowsRuntimeBufferExtensions.ToArray(hash);
        }

        /// <summary>
        /// 指定した文字列を指定した回数SHA256で計算したハッシュ値を取得します。
        /// </summary>
        /// <param name="value">SHA256を計算する文字列。</param>
        /// <param name="count">回数。</param>
        /// <returns>SHA256。</returns>
        public byte[] GetHash(string value, int count)
        {
            var hashed = ToSha256Hash(value);
            for (var i = 1; i < count; i++)
            {
                hashed = ToSha256Hash(hashed);
            }
            return hashed;
        }

        /// <summary>
        /// 指定されたパスワードとソルトをハッシュ化した文字列を返す
        /// </summary>
        /// <param name="pass">生パスワード</param>
        /// <param name="salt"></param>
        /// <returns>ハッシュ化されたパスワード</returns>
        public string GetHashedPassword(string pass, string salt)
        {
            return Convert.ToBase64String(GetHash(salt + pass, 1000));
        }
    }
}
