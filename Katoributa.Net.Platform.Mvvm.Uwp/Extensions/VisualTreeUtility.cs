﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;

namespace Katoributa.Net.Platform.Mvvm.Extensions
{
    /// <summary>
    /// VisualTreeに関係する便利メソッド群です。
    /// </summary>
    public static class VisualTreeUtility
    {
        /// <summary>
        /// 指定したオブジェクトのVisualTreeの子要素を取得します。
        /// </summary>
        /// <typeparam name="T">取得する子要素の型。</typeparam>
        /// <param name="obj">子要素を取得するオブジェクト。</param>
        /// <returns>子要素。</returns>
        public static T FindVisualChild<T>(this DependencyObject obj) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                var child = VisualTreeHelper.GetChild(obj, i);

                if (child is T)
                {
                    return (T)child;
                }
                else
                {
                    child = FindVisualChild<T>(child);
                    if (child != null)
                    {
                        return (T)child;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 指定したオブジェクトのVisualTreeの子要素を取得します。
        /// </summary>
        /// <typeparam name="T">取得する子要素の型。</typeparam>
        /// <param name="obj">子要素を取得するオブジェクト。</param>
        /// <returns>子要素の一覧。</returns>
        public static IEnumerable<T> FindVisualChildren<T>(this DependencyObject obj) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                var child = VisualTreeHelper.GetChild(obj, i);
                if (child is T)
                {
                    yield return (T)child;
                }
                else
                {
                    var children = FindVisualChildren<T>(child);
                    foreach (var item in children)
                    {
                        yield return (T)item;
                    }
                }
            }
        }

        /// <summary>
        /// 指定したオブジェクトのVisualTreeの親要素を取得します。
        /// </summary>
        /// <typeparam name="T">取得する親要素の型。</typeparam>
        /// <param name="obj">親要素を取得するオブジェクト。</param>
        /// <returns>親要素。</returns>
        public static T FindVisualParent<T>(this DependencyObject obj) where T : DependencyObject
        {
            var parent = VisualTreeHelper.GetParent(obj);
            if (parent != null)
            {
                if (parent is T)
                {
                    return (T)parent;
                }
                else
                {
                    parent = FindVisualParent<T>(parent);
                    if (parent != null)
                    {
                        return (T)parent;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 指定したオブジェクトの親となるPopupを取得します。
        /// </summary>
        /// <param name="obj">Popupを取得するオブジェクト。</param>
        /// <returns>Popup。</returns>
        public static Popup FindPopup(this DependencyObject obj)
        {
            var element = obj as FrameworkElement;
            if (element != null && element.Parent is Popup)
            {
                return element.Parent as Popup;
            }

            var parent = VisualTreeHelper.GetParent(obj);
            if (parent != null)
            {
                return FindPopup(parent);
            }
            return null;
        }
    }
}
