﻿using System;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Documents;

namespace Katoributa.Net.Platform.Mvvm.Extensions
{
    /// <summary>
    /// フライアウトに関係する便利メソッド群です。
    /// </summary>
    /// <remarks>
    /// 下記サイトを参考。
    /// http://d.hatena.ne.jp/okazuki/20120826/FlyoutDeepDive
    /// </remarks>
    public static class FlyoutUtility
    {
        /// <summary>
        /// 画面の中央にフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TFlyoutContent>(TFlyoutContent content)
            where TFlyoutContent : FrameworkElement
        {
            return CreateFlyout(null as FrameworkElement, content, PlacementMode.Center, 0, 0);
        }

        /// <summary>
        /// 画面からの場所を指定してフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TFlyoutContent>(TFlyoutContent content, PlacementMode placement)
            where TFlyoutContent : FrameworkElement
        {
            return CreateFlyout(null as FrameworkElement, content, placement, 0, 0);
        }

        /// <summary>
        /// BottomAppBar上にフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TTarget">上部にフライアウトを表示するターゲットの型。</typeparam>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="appBar">フライアウトを表示する対象のコントロールの乗っているBottomAppBar。</param>
        /// <param name="target">上部にフライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TTarget, TFlyoutContent>(AppBar appBar, TTarget target, TFlyoutContent content)
            where TTarget : FrameworkElement
            where TFlyoutContent : FrameworkElement
        {
            // 余白を考慮した画面幅
            //content.Width = Math.Min(Window.Current.Bounds.Width - RightAndLeftMargin * 2, content.Width);

            var popup = new Popup
            {
                Child = content,
                IsLightDismissEnabled = true
            };

            var pt = AdjustPosition(target, content, PlacementMode.TopRight, 20, 20);
            Canvas.SetTop(popup, pt.Y);
            Canvas.SetLeft(popup, pt.X);

            // ソフトウェアキーボードの表示時の再計算処理を行う。
            var pane = InputPane.GetForCurrentView();
            TypedEventHandler<InputPane, InputPaneVisibilityEventArgs> inputPaneVisibilityChanged = (_, e) =>
            {
                if (e.EnsuredFocusedElementInView)
                {
                    // ソフトウェアキーボードが隠れるときはフライアウトの位置をリセットする
                    Canvas.SetTop(popup, pt.Y);
                    Canvas.SetLeft(popup, pt.X);
                    return;
                }

                // ソフトウェアキーボードが表示された時
                Canvas.SetTop(popup, pt.Y - e.OccludedRect.Height);
                Canvas.SetLeft(popup, pt.X);
            };

            // フライアウトが表示されたらソフトウェアキーボードの状態監視を行う。
            popup.Opened += (_, __) =>
            {
                try
                {
                    pane.Showing += inputPaneVisibilityChanged;
                    pane.Hiding += inputPaneVisibilityChanged;
                }
                catch
                {
                }
            };

            // フライアウトが表示されなくなったタイミングでソフトウェアキーボードの状態監視を辞める。
            popup.Closed += (_, __) =>
            {
                pane.Showing -= inputPaneVisibilityChanged;
                pane.Hiding -= inputPaneVisibilityChanged;
            };

            // AppBarが閉じられたらフライアウトも閉じる
            if (appBar != null)
            {
                EventHandler<object> closed = null;
                closed = (_, __) =>
                {
                    popup.IsOpen = false;
                    appBar.Closed -= closed;
                };
                appBar.Closed += closed;
            }

            return popup;
        }

        /// <summary>
        /// 場所を指定してフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TTarget, TFlyoutContent>(TTarget target, TFlyoutContent content, PlacementMode placement)
            where TTarget : FrameworkElement
            where TFlyoutContent : FrameworkElement
        {
            return CreateFlyout(target, content, placement, 0, 0);
        }

        /// <summary>
        /// 場所を指定してフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="topAndBottomMargin">上下の余白。</param>
        /// <param name="leftAndRightMargin">左右の余白。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TTarget, TFlyoutContent>(TTarget target, TFlyoutContent content, PlacementMode placement
                 , double topAndBottomMargin, double leftAndRightMargin)
            where TTarget : FrameworkElement
            where TFlyoutContent : FrameworkElement
        {
            return CreateFlyout(target, content, placement, topAndBottomMargin, leftAndRightMargin, false);
        }

        /// <summary>
        /// 場所を指定してフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="topAndBottomMargin">上下の余白。</param>
        /// <param name="leftAndRightMargin">左右の余白。</param>
        /// <param name="isAutoMoveOnScreenKeyboardOverlaps">キーボードと重なったときに自動で移動するか。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TTarget, TFlyoutContent>(TTarget target, TFlyoutContent content, PlacementMode placement
                 , double topAndBottomMargin, double leftAndRightMargin, bool isAutoMoveOnScreenKeyboardOverlaps)
            where TTarget : FrameworkElement
            where TFlyoutContent : FrameworkElement
        {
            var popup = new Popup
            {
                Child = content,
                IsLightDismissEnabled = true
            };

            var pt = AdjustPosition(target, content, placement, topAndBottomMargin, leftAndRightMargin);
            Canvas.SetTop(popup, pt.Y);
            Canvas.SetLeft(popup, pt.X);

            if (!isAutoMoveOnScreenKeyboardOverlaps)
            {
                return popup;
            }

            var pane = InputPane.GetForCurrentView();
            TypedEventHandler<InputPane, InputPaneVisibilityEventArgs> inputPaneVisibilityChanged = (o, e) =>
                {
                    if (e.OccludedRect.Height == 0)
                    {
                        Canvas.SetTop(popup, pt.Y);
                        //Canvas.SetLeft(popup, pt.X);
                        return;
                    }

                    var child = popup.Child as FrameworkElement;
                    var height = child != null ? child.ActualHeight : 0;

                    if (pt.Y + height > e.OccludedRect.Top)
                    {
                        Canvas.SetTop(popup, pt.Y - (pt.Y + height - e.OccludedRect.Top));
                        //Canvas.SetLeft(popup, pt.X);
                    }
                };
            popup.Opened += (o, e) =>
                {
                    try
                    {
                        pane.Showing += inputPaneVisibilityChanged;
                        pane.Hiding += inputPaneVisibilityChanged;
                    }
                    catch
                    {
                    }
                };
            popup.Closed += (o, e) =>
                {
                    pane.Showing -= inputPaneVisibilityChanged;
                    pane.Hiding -= inputPaneVisibilityChanged;
                };
            return popup;
        }

        /// <summary>
        /// 場所を指定してフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TFlyoutContent>(TextElement target, TFlyoutContent content, PlacementMode placement)
            where TFlyoutContent : FrameworkElement
        {
            return CreateFlyout(target, content, placement, 0, 0);
        }

        /// <summary>
        /// 場所を指定してフライアウトを表示するためのPopupを作成します。
        /// </summary>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="topAndBottomMargin">上下の余白。</param>
        /// <param name="leftAndRightMargin">左右の余白。</param>
        /// <returns>フライアウト表示用のPopup</returns>
        public static Popup CreateFlyout<TFlyoutContent>(TextElement target, TFlyoutContent content, PlacementMode placement
                 , double topAndBottomMargin, double leftAndRightMargin)
            where TFlyoutContent : FrameworkElement
        {
            var popup = new Popup
            {
                Child = content,
                IsLightDismissEnabled = true
            };

            var pt = AdjustPosition(target, content, placement, topAndBottomMargin, leftAndRightMargin);
            Canvas.SetTop(popup, pt.Y);
            Canvas.SetLeft(popup, pt.X);
            return popup;
        }

        /// <summary>
        /// 場所を指定してX座標を計算します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentWidth">フライアウト内に表示するコンテンツの幅。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>X座標。</returns>
        private static double CalcurateXPoint<TTarget>(TTarget target, double contentWidth, PlacementMode placement, double margin)
            where TTarget : FrameworkElement
        {
            if (target == null)
            {
                switch (placement)
                {
                    case PlacementMode.Left:
                    case PlacementMode.LeftTop:
                    case PlacementMode.LeftBottom:
                    case PlacementMode.TopLeft:
                    case PlacementMode.BottomLeft:
                        return margin;
                    case PlacementMode.Right:
                    case PlacementMode.TopRight:
                    case PlacementMode.RightTop:
                    case PlacementMode.RightBottom:
                    case PlacementMode.BottomRight:
                        return Window.Current.Bounds.Width - (contentWidth + margin);
                    default:
                        return (Window.Current.Bounds.Width - contentWidth + margin) / 2;
                }
            }
            var gt = target.TransformToVisual(null);
            var pt = gt.TransformPoint(new Point());

            switch (placement)
            {
                case PlacementMode.Left:
                case PlacementMode.LeftTop:
                case PlacementMode.LeftBottom:
                    return pt.X -= contentWidth + margin;
                case PlacementMode.Right:
                case PlacementMode.RightTop:
                case PlacementMode.RightBottom:
                    return pt.X += target.ActualWidth + margin;
                case PlacementMode.Top:
                case PlacementMode.Bottom:
                case PlacementMode.Center:
                    return pt.X += (target.ActualWidth - contentWidth + margin) / 2;
                default:
                    return pt.X + margin;
            }
        }

        /// <summary>
        /// 場所を指定してX座標を計算します。
        /// </summary>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentWidth">フライアウト内に表示するコンテンツの幅。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>X座標。</returns>
        private static double CalcurateXPoint(TextElement target, double contentWidth, PlacementMode placement, double margin)
        {
            var parent = target.ElementStart.VisualParent;
            if (target == null || parent == null)
            {
                switch (placement)
                {
                    case PlacementMode.Left:
                    case PlacementMode.LeftTop:
                    case PlacementMode.LeftBottom:
                    case PlacementMode.TopLeft:
                    case PlacementMode.BottomLeft:
                        return margin;
                    case PlacementMode.Right:
                    case PlacementMode.TopRight:
                    case PlacementMode.RightTop:
                    case PlacementMode.RightBottom:
                    case PlacementMode.BottomRight:
                        return Window.Current.Bounds.Width - (contentWidth + margin);
                    default:
                        return (Window.Current.Bounds.Width - contentWidth + margin) / 2;
                }
            }
            var rect = GetTextElementRect(target);
            var gt = parent.TransformToVisual(null);
            var pt = gt.TransformPoint(new Point(rect.Left, rect.Top));

            switch (placement)
            {
                case PlacementMode.Left:
                case PlacementMode.LeftTop:
                case PlacementMode.LeftBottom:
                    return pt.X -= contentWidth + margin;
                case PlacementMode.Right:
                case PlacementMode.RightTop:
                case PlacementMode.RightBottom:
                    return pt.X += rect.Width + margin;
                case PlacementMode.Top:
                case PlacementMode.Bottom:
                case PlacementMode.Center:
                    return pt.X += (rect.Width - contentWidth + margin) / 2;
                default:
                    return pt.X + margin;
            }
        }

        /// <summary>
        /// TextElementの矩形を取得します。
        /// </summary>
        /// <param name="element">TextElement。</param>
        /// <returns>TextElementの矩形。</returns>
        private static Rect GetTextElementRect(TextElement element)
        {
            var startRect = element.ElementStart.GetCharacterRect(LogicalDirection.Forward);
            var endRect = element.ElementEnd.GetCharacterRect(LogicalDirection.Backward);
            var rect = new Rect(startRect.Left, startRect.Y, endRect.Right - startRect.Left, endRect.Bottom - startRect.Top);
            return rect;
        }

        /// <summary>
        /// 場所を指定してY座標を計算します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentHeight">フライアウト内に表示するコンテンツの高さ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>Y座標。</returns>
        private static double CalcurateYPoint<TTarget>(TTarget target, double contentHeight, PlacementMode placement, double margin)
            where TTarget : FrameworkElement
        {
            if (target == null)
            {
                switch (placement)
                {
                    case PlacementMode.Top:
                    case PlacementMode.LeftTop:
                    case PlacementMode.TopLeft:
                    case PlacementMode.TopRight:
                    case PlacementMode.RightTop:
                        return margin;
                    case PlacementMode.Bottom:
                    case PlacementMode.LeftBottom:
                    case PlacementMode.RightBottom:
                    case PlacementMode.BottomLeft:
                    case PlacementMode.BottomRight:
                        return Window.Current.Bounds.Height - (contentHeight + margin);
                    default:
                        return (Window.Current.Bounds.Height - contentHeight + margin) / 2;
                }
            }
            var gt = target.TransformToVisual(null);
            var pt = gt.TransformPoint(new Point());

            switch (placement)
            {
                case PlacementMode.Top:
                case PlacementMode.TopLeft:
                case PlacementMode.TopRight:
                    return pt.Y -= contentHeight + margin;
                case PlacementMode.Bottom:
                case PlacementMode.BottomLeft:
                case PlacementMode.BottomRight:
                    return pt.Y += target.ActualHeight + margin;
                case PlacementMode.Left:
                case PlacementMode.Right:
                case PlacementMode.Center:
                    return pt.Y += (target.ActualHeight - contentHeight + margin) / 2;
                default:
                    return pt.Y + margin;
            }
        }

        /// <summary>
        /// 場所を指定してY座標を計算します。
        /// </summary>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentHeight">フライアウト内に表示するコンテンツの高さ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>Y座標。</returns>
        private static double CalcurateYPoint(TextElement target, double contentHeight, PlacementMode placement, double margin)
        {
            var parent = target.ElementStart.VisualParent;
            if (target == null || parent == null)
            {
                switch (placement)
                {
                    case PlacementMode.Top:
                    case PlacementMode.LeftTop:
                    case PlacementMode.TopLeft:
                    case PlacementMode.TopRight:
                    case PlacementMode.RightTop:
                        return margin;
                    case PlacementMode.Bottom:
                    case PlacementMode.LeftBottom:
                    case PlacementMode.RightBottom:
                    case PlacementMode.BottomLeft:
                    case PlacementMode.BottomRight:
                        return Window.Current.Bounds.Height - (contentHeight + margin);
                    default:
                        return (Window.Current.Bounds.Height - contentHeight + margin) / 2;
                }
            }
            var rect = GetTextElementRect(target);
            var gt = parent.TransformToVisual(null);
            var pt = gt.TransformPoint(new Point());

            switch (placement)
            {
                case PlacementMode.Top:
                case PlacementMode.TopLeft:
                case PlacementMode.TopRight:
                    return pt.Y -= contentHeight + margin;
                case PlacementMode.Bottom:
                case PlacementMode.BottomLeft:
                case PlacementMode.BottomRight:
                    return pt.Y += rect.Height + margin;
                case PlacementMode.Left:
                case PlacementMode.Right:
                case PlacementMode.Center:
                    return pt.Y += (rect.Height - contentHeight + margin) / 2;
                default:
                    return pt.Y + margin;
            }
        }

        /// <summary>
        /// 画面外にはみ出さないよう調整されたX座標を取得します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentWidth">フライアウト内に表示するコンテンツの幅。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>X座標。</returns>
        private static double AdjustXPoint<TTarget>(TTarget target, double contentWidth, PlacementMode placement, double margin)
            where TTarget : FrameworkElement
        {
            var x = CalcurateXPoint(target, contentWidth, placement, margin);
            switch (placement)
            {
                case PlacementMode.Left:
                case PlacementMode.LeftTop:
                case PlacementMode.LeftBottom:
                    if (x < 0)
                    {
                        x = CalcurateXPoint(target, contentWidth, PlacementMode.RightTop, margin);
                        if (x + contentWidth > Window.Current.Bounds.Width)
                        {
                            x = 0;
                        }
                    }
                    return x;
                case PlacementMode.Right:
                case PlacementMode.RightTop:
                case PlacementMode.RightBottom:
                    if (x + contentWidth > Window.Current.Bounds.Width)
                    {
                        x = CalcurateXPoint(target, contentWidth, PlacementMode.LeftTop, margin);
                        if (x < 0)
                        {
                            x = Window.Current.Bounds.Width - contentWidth;
                        }
                    }
                    return x;
                case PlacementMode.TopRight:
                case PlacementMode.BottomRight:
                    if (target == null)
                    {
                        return x;
                    }
                    return Math.Max(x + target.ActualWidth - contentWidth - margin, 0);
                default:
                    if (x < 0)
                    {
                        x = 0;
                    }
                    else if (x + contentWidth > Window.Current.Bounds.Width)
                    {
                        x = Window.Current.Bounds.Width - contentWidth;
                    }
                    return x;
            }
        }

        /// <summary>
        /// 画面外にはみ出さないよう調整されたX座標を取得します。
        /// </summary>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentWidth">フライアウト内に表示するコンテンツの幅。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>X座標。</returns>
        private static double AdjustXPoint(TextElement target, double contentWidth, PlacementMode placement, double margin)
        {
            var x = CalcurateXPoint(target, contentWidth, placement, margin);
            switch (placement)
            {
                case PlacementMode.Left:
                case PlacementMode.LeftTop:
                case PlacementMode.LeftBottom:
                    if (x < 0)
                    {
                        x = CalcurateXPoint(target, contentWidth, PlacementMode.RightTop, margin);
                        if (x + contentWidth > Window.Current.Bounds.Width)
                        {
                            x = 0;
                        }
                    }
                    return x;
                case PlacementMode.Right:
                case PlacementMode.RightTop:
                case PlacementMode.RightBottom:
                    if (x + contentWidth > Window.Current.Bounds.Width)
                    {
                        x = CalcurateXPoint(target, contentWidth, PlacementMode.LeftTop, margin);
                        if (x < 0)
                        {
                            x = Window.Current.Bounds.Width - contentWidth;
                        }
                    }
                    return x;
                case PlacementMode.TopRight:
                case PlacementMode.BottomRight:
                    if (target == null)
                    {
                        return x;
                    }
                    var rect = GetTextElementRect(target);
                    return Math.Max(x + rect.Width - contentWidth - margin, 0);
                default:
                    if (x < 0)
                    {
                        x = 0;
                    }
                    else if (x + contentWidth > Window.Current.Bounds.Width)
                    {
                        x = Window.Current.Bounds.Width - contentWidth;
                    }
                    return x;
            }
        }

        /// <summary>
        /// 画面外にはみ出さないよう調整されたY座標を取得します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentHeight">フライアウト内に表示するコンテンツの高さ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>Y座標。</returns>
        private static double AdjustYPoint<TTarget>(TTarget target, double contentHeight, PlacementMode placement, double margin)
            where TTarget : FrameworkElement
        {
            var y = CalcurateYPoint(target, contentHeight, placement, margin);
            switch (placement)
            {
                case PlacementMode.Top:
                case PlacementMode.TopLeft:
                case PlacementMode.TopRight:
                    if (y < 0)
                    {
                        y = CalcurateYPoint(target, contentHeight, PlacementMode.BottomLeft, margin);
                        if (y + contentHeight > Window.Current.Bounds.Height)
                        {
                            y = 0;
                        }
                    }
                    return y;
                case PlacementMode.Bottom:
                case PlacementMode.BottomLeft:
                case PlacementMode.BottomRight:
                    if (y + contentHeight > Window.Current.Bounds.Height)
                    {
                        y = CalcurateYPoint(target, contentHeight, PlacementMode.TopLeft, margin);
                        if (y < 0)
                        {
                            y = Window.Current.Bounds.Height - contentHeight;
                        }
                    }
                    return y;
                case PlacementMode.LeftBottom:
                case PlacementMode.RightBottom:
                    var actualHeight = target == null ? Window.Current.Bounds.Height : target.ActualHeight;
                    return Math.Max(y + actualHeight - contentHeight - margin, 0);
                default:
                    if (y < 0)
                    {
                        y = 0;
                    }
                    else if (y + contentHeight > Window.Current.Bounds.Height)
                    {
                        y = Window.Current.Bounds.Height - contentHeight;
                    }
                    return y;
            }
        }

        /// <summary>
        /// 画面外にはみ出さないよう調整されたY座標を取得します。
        /// </summary>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="contentHeight">フライアウト内に表示するコンテンツの高さ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="margin">余白。</param>
        /// <returns>Y座標。</returns>
        private static double AdjustYPoint(TextElement target, double contentHeight, PlacementMode placement, double margin)
        {
            var y = CalcurateYPoint(target, contentHeight, placement, margin);
            switch (placement)
            {
                case PlacementMode.Top:
                case PlacementMode.TopLeft:
                case PlacementMode.TopRight:
                    if (y < 0)
                    {
                        y = CalcurateYPoint(target, contentHeight, PlacementMode.BottomLeft, margin);
                        if (y + contentHeight > Window.Current.Bounds.Height)
                        {
                            y = 0;
                        }
                    }
                    return y;
                case PlacementMode.Bottom:
                case PlacementMode.BottomLeft:
                case PlacementMode.BottomRight:
                    if (y + contentHeight > Window.Current.Bounds.Height)
                    {
                        y = CalcurateYPoint(target, contentHeight, PlacementMode.TopLeft, margin);
                        if (y < 0)
                        {
                            y = Window.Current.Bounds.Height - contentHeight;
                        }
                    }
                    return y;
                case PlacementMode.LeftBottom:
                case PlacementMode.RightBottom:
                    var rect = GetTextElementRect(target);
                    var actualHeight = target == null ? Window.Current.Bounds.Height : rect.Height;
                    return Math.Max(y + actualHeight - contentHeight - margin, 0);
                default:
                    if (y < 0)
                    {
                        y = 0;
                    }
                    else if (y + contentHeight > Window.Current.Bounds.Height)
                    {
                        y = Window.Current.Bounds.Height - contentHeight;
                    }
                    return y;
            }
        }

        //HACK:Shigeta contentのMarginも考慮するよう変更。
        /// <summary>
        /// 画面外にはみ出さないよう調整された座標を取得します。
        /// </summary>
        /// <typeparam name="TTarget">フライアウトを表示するターゲットの型。</typeparam>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="topAndBottomMargin">上下の余白。</param>
        /// <param name="leftAndRightMargin">左右の余白。</param>
        /// <returns>調整された座標。</returns>
        private static Point AdjustPosition<TTarget, TFlyoutContent>(TTarget target, TFlyoutContent content, PlacementMode placement
            , double topAndBottomMargin, double leftAndRightMargin)
            where TTarget : FrameworkElement
            where TFlyoutContent : FrameworkElement
        {
            var width = content.Width;
            var height = content.Height;
            if (double.IsNaN(width) || double.IsNaN(height))
            {
                content.Measure(new Size(double.IsNaN(width) ? double.MaxValue : width, double.IsNaN(height) ? double.MaxValue : height));
                width = double.IsNaN(width) ? content.DesiredSize.Width : width;
                height = double.IsNaN(height) ? content.DesiredSize.Height : height;
            }
            var x = (int)AdjustXPoint(target, width, placement, leftAndRightMargin);
            var y = (int)AdjustYPoint(target, height, placement, topAndBottomMargin);
            return new Point(x, y);
        }

        /// <summary>
        /// 画面外にはみ出さないよう調整された座標を取得します。
        /// </summary>
        /// <typeparam name="TFlyoutContent">フライアウト内に表示するコンテンツの型。</typeparam>
        /// <param name="target">フライアウトを表示するターゲット。</param>
        /// <param name="content">フライアウト内に表示するコンテンツ。</param>
        /// <param name="placement">フライアウトを表示する場所。</param>
        /// <param name="topAndBottomMargin">上下の余白。</param>
        /// <param name="leftAndRightMargin">左右の余白。</param>
        /// <returns>調整された座標。</returns>
        private static Point AdjustPosition<TFlyoutContent>(TextElement target, TFlyoutContent content, PlacementMode placement
            , double topAndBottomMargin, double leftAndRightMargin)
            where TFlyoutContent : FrameworkElement
        {
            var width = content.Width;
            var height = content.Height;
            if (double.IsNaN(width) || double.IsNaN(height))
            {
                content.Measure(new Size(double.IsNaN(width) ? double.MaxValue : width, double.IsNaN(height) ? double.MaxValue : height));
                width = double.IsNaN(width) ? content.DesiredSize.Width : width;
                height = double.IsNaN(height) ? content.DesiredSize.Height : height;
            }
            var x = (int)AdjustXPoint(target, width, placement, leftAndRightMargin);
            var y = (int)AdjustYPoint(target, height, placement, topAndBottomMargin);
            return new Point(x, y);
        }
    }
}
