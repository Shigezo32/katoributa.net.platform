﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Mvvm.Extensions
{
    /// <summary>
    /// Popupを表示する画面上の場所。
    /// </summary>
    public enum PlacementMode
    {
        /// <summary>
        /// 右端とtargetの左端、中央とtargetの中央を揃えます。
        /// </summary>
        Left,
        /// <summary>
        /// 右端とtargetの左端、上端とtargetの上端を揃えます。
        /// </summary>
        LeftTop,
        /// <summary>
        /// 右端とtargetの左端、下端とtargetの下端を揃えます。
        /// </summary>
        LeftBottom,
        /// <summary>
        /// 中央とtargetの中央、下端とtargetの上端を揃えます。
        /// </summary>
        Top,
        /// <summary>
        /// 左端とtargetの左端、下端とtargetの上端を揃えます。
        /// </summary>
        TopLeft,
        /// <summary>
        /// 右端とtargetの右端、下端とtargetの上端を揃えます。
        /// </summary>
        TopRight,
        /// <summary>
        /// 左端とtargetの右端、中央とtargetの中央を揃えます。
        /// </summary>
        Right,
        /// <summary>
        /// 左端とtargetの右端、上端とtargetの上端を揃えます。
        /// </summary>
        RightTop,
        /// <summary>
        /// 左端とtargetの右端、下端とtargetの下端を揃えます。
        /// </summary>
        RightBottom,
        /// <summary>
        /// 中央とtargetの中央、上端とtargetの下端を揃えます。
        /// </summary>
        Bottom,
        /// <summary>
        /// 左端とtargetの左端、上端とtargetの下端を揃えます。
        /// </summary>
        BottomLeft,
        /// <summary>
        /// 右端とtargetの右端、上端とtargetの下端を揃えます。
        /// </summary>
        BottomRight,
        /// <summary>
        /// targetを中心として配置します。
        /// </summary>
        Center,
    }
}
