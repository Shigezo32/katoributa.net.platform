﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Katoributa.Net.Platform.Mvvm.Extensions
{
    /// <summary>
    /// VisualStateに関係する便利メソッド群です。
    /// </summary>
    public static class VisualStateUtility
    {
        /// <summary>
        /// 指定したVisualStateGroupの現在の状態を取得します。
        /// </summary>
        /// <param name="visualStatesHost">VisualStateGroupを持つ要素。</param>
        /// <param name="stateGroupName">VisualStateGroupの名前。</param>
        /// <returns>現在のVisualState。</returns>
        public static VisualState GetCurrentState(this FrameworkElement visualStatesHost, string stateGroupName) 
        {
            var stateGroups = VisualStateManager.GetVisualStateGroups(visualStatesHost);
            VisualStateGroup stateGroup = null;
            if (!string.IsNullOrEmpty(stateGroupName))
            {
                stateGroup = stateGroups.FirstOrDefault(g => g.Name == stateGroupName);
            }
            if (stateGroup != null)
            {
                return stateGroup.CurrentState;
            }
            return null;
        }
    }
}
