﻿using System;
using System.Reactive.Linq;
using Katoributa.Net.Platform.Reactive.Extensions;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace Katoributa.Net.Platform.Mvvm.Extensions
{
    /// <summary>
    /// ドラッグ＆ドロップに関係する便利機能群です。
    /// </summary>
    public static class DragAndDropUtility
    {
        #region MoveByDragging

        /// <summary>
        /// ドラッグして移動する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachMoveByDragging(FrameworkElement dragElement)
        {
            return AttachMoveByDragging(dragElement, dragElement, Window.Current.Content as FrameworkElement, true);
        }

        /// <summary>
        /// ドラッグして移動する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="moveTarget">移動する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachMoveByDragging(FrameworkElement dragElement, FrameworkElement moveTarget)
        {
            return AttachMoveByDragging(dragElement, moveTarget, Window.Current.Content as FrameworkElement, true);
        }

        /// <summary>
        /// ドラッグして移動する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="moveTarget">移動する対象の要素。</param>
        /// <param name="canvas">座標を計算するキャンバス。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachMoveByDragging(FrameworkElement dragElement, FrameworkElement moveTarget, FrameworkElement canvas)
        {
            return AttachMoveByDragging(dragElement, moveTarget, canvas, true);
        }

        /// <summary>
        /// ドラッグして移動する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="moveTarget">移動する対象の要素。</param>
        /// <param name="canvas">座標を計算するキャンバス。</param>
        /// <param name="canOutsideMoved">キャンバス外への移動を許容するか。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachMoveByDragging(FrameworkElement dragElement, FrameworkElement moveTarget, FrameworkElement canvas, bool canOutsideMoved)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            var virtualPosition = new Point();
            var drag = dragElement.PointerPressedAsObservable().Do(x =>
                            {
                                dragElement.CapturePointer(x.Pointer);
                                virtualPosition = new Point(Canvas.GetLeft(moveTarget), Canvas.GetTop(moveTarget));
                            })
                        .SelectMany(_ => dragElement.PointerMovedAsObservable())
                        .TakeUntil(dragElement.PointerReleasedAsObservable().Do(x => dragElement.ReleasePointerCapture(x.Pointer)))
                        .Select(x => x.GetCurrentPoint(canvas).Position);

            return drag.Zip(drag.Skip(1), (x, y) => new { Prev = x, Next = y })
                .Select(_ => new { HorizontalChange = _.Next.X - _.Prev.X, VerticalChange = _.Next.Y - _.Prev.Y })
                .Where(x => x.HorizontalChange != 0 || x.VerticalChange != 0)
                .Repeat()
                .Subscribe(_ =>
                    {
                        virtualPosition = new Point(virtualPosition.X + _.HorizontalChange, virtualPosition.Y + _.VerticalChange);
                        var x = virtualPosition.X;
                        var y = virtualPosition.Y;
                        if (!canOutsideMoved)
                        {
                            var container = moveTarget;
                            if (container is Popup)
                            {
                                container = (container as Popup).Child as FrameworkElement;
                            }
                            var minX = 0 - moveTarget.Margin.Left;
                            var minY = 0 - moveTarget.Margin.Top;
                            var maxX = canvas.ActualWidth - container.ActualWidth - moveTarget.Margin.Left;
                            var maxY = canvas.ActualHeight - container.ActualHeight - moveTarget.Margin.Top;
                            x = virtualPosition.X < minX ? minX : virtualPosition.X > maxX ? maxX : virtualPosition.X;
                            y = virtualPosition.Y < minY ? minY : virtualPosition.Y > maxY ? maxY : virtualPosition.Y;
                        }
                        Canvas.SetLeft(moveTarget, x);
                        Canvas.SetTop(moveTarget, y);
                    });
        }

        #endregion

        #region ResizeByDragging

        /// <summary>
        /// ドラッグしてサイズを変更するときの引数。
        /// </summary>
        private class ResizeDraggingArgs
        {
            /// <summary>
            /// ResizeDraggingArgsのインスタンスを初期化します。
            /// </summary>
            /// <param name="baseSize">基準となるサイズ。</param>
            /// <param name="horizontalChange">水平方向の変更。</param>
            /// <param name="verticalChange">垂直方向の変更。</param>
            public ResizeDraggingArgs(Size baseSize, double horizontalChange, double verticalChange)
            {
                this.BaseSize = baseSize;
                this.HorizontalChange = horizontalChange;
                this.VerticalChange = verticalChange;
            }

            /// <summary>
            /// サイズを変更する対象の基準となるサイズ。
            /// </summary>
            public Size BaseSize { private set; get; }

            /// <summary>
            /// 水平方向の変更。
            /// </summary>
            public double HorizontalChange { private set; get; }

            /// <summary>
            /// 垂直方向の変更。
            /// </summary>
            public double VerticalChange { private set; get; }
        }

        /// <summary>
        /// ドラッグする対象の要素をドラッグしてサイズを変更するObservableに変換します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IObservable&lt;ResizeDraggingArgs&gt;。</returns>
        private static IObservable<ResizeDraggingArgs> AsResizeByDraggingObservable(this FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            var canvas = Window.Current.Content;
            var baseSize = new Size();
            var startPosition = new Point();
            var drag = dragElement.PointerPressedAsObservable().Do(x =>
                            {
                                dragElement.CapturePointer(x.Pointer);
                                baseSize = new Size(resizeTarget.ActualWidth, resizeTarget.ActualHeight);
                                startPosition = x.GetCurrentPoint(canvas).Position;
                            })
                        .SelectMany(_ => dragElement.PointerMovedAsObservable())
                        .TakeUntil(dragElement.PointerReleasedAsObservable().Do(x => dragElement.ReleasePointerCapture(x.Pointer)));

            return drag.Select(x => x.GetCurrentPoint(canvas).Position)
                    .Select(_ => new ResizeDraggingArgs(baseSize, _.X - startPosition.X, _.Y - startPosition.Y))
                    .Where(x => x.HorizontalChange != 0 || x.VerticalChange != 0)
                    .Repeat();
        }

        /// <summary>
        /// 左のサイズを変更します。
        /// </summary>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <param name="args">ドラッグしてサイズを変更するときの引数。</param>
        private static void ResizeLeft(FrameworkElement resizeTarget, ResizeDraggingArgs args)
        {
            var minWidth = 20 + resizeTarget.Margin.Left + resizeTarget.Margin.Right;
            minWidth = double.IsNaN(resizeTarget.MinWidth) || resizeTarget.MinWidth < minWidth ? minWidth : resizeTarget.MinWidth;
            var width = Math.Max(minWidth, args.BaseSize.Width - args.HorizontalChange);
            if (!double.IsInfinity(resizeTarget.MaxWidth))
            {
                width = Math.Min(width, resizeTarget.MaxWidth);
            }
            var container = resizeTarget.Parent is Popup ? resizeTarget.Parent as UIElement : resizeTarget;
            var left = Canvas.GetLeft(container);
            var targetWidth = double.IsNaN(resizeTarget.Width) ? args.BaseSize.Width : resizeTarget.Width;
            Canvas.SetLeft(container, left + targetWidth - width);
            resizeTarget.Width = width;
        }

        /// <summary>
        /// 上のサイズを変更します。
        /// </summary>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <param name="args">ドラッグしてサイズを変更するときの引数。</param>
        private static void ResizeTop(FrameworkElement resizeTarget, ResizeDraggingArgs args)
        {
            var minHeight = 20 + resizeTarget.Margin.Top + resizeTarget.Margin.Bottom;
            minHeight = double.IsNaN(resizeTarget.MinHeight) || resizeTarget.MinHeight < minHeight ? minHeight : resizeTarget.MinHeight;
            var height = Math.Max(minHeight, args.BaseSize.Height - args.VerticalChange);
            if (!double.IsInfinity(resizeTarget.MaxHeight))
            {
                height = Math.Min(height, resizeTarget.MaxHeight);
            }
            var container = resizeTarget.Parent is Popup ? resizeTarget.Parent as UIElement : resizeTarget;
            var top = Canvas.GetTop(container);
            var targetHeight = double.IsNaN(resizeTarget.Height) ? args.BaseSize.Height : resizeTarget.Height;
            Canvas.SetTop(container, top + targetHeight - height);
            resizeTarget.Height = height;
        }

        /// <summary>
        /// 右のサイズを変更します。
        /// </summary>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <param name="args">ドラッグしてサイズを変更するときの引数。</param>
        private static void ResizeRight(FrameworkElement resizeTarget, ResizeDraggingArgs args)
        {
            var minWidth = 20 + resizeTarget.Margin.Left + resizeTarget.Margin.Right;
            minWidth = double.IsNaN(resizeTarget.MinWidth) || resizeTarget.MinWidth < minWidth ? minWidth : resizeTarget.MinWidth;
            var width = Math.Max(minWidth, args.BaseSize.Width + args.HorizontalChange);
            if (!double.IsInfinity(resizeTarget.MaxWidth))
            {
                width = Math.Min(width, resizeTarget.MaxWidth);
            }
            resizeTarget.Width = width;
        }

        /// <summary>
        /// 下のサイズを変更します。
        /// </summary>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <param name="args">ドラッグしてサイズを変更するときの引数。</param>
        private static void ResizeBottom(FrameworkElement resizeTarget, ResizeDraggingArgs args)
        {
            var minHeight = 20 + resizeTarget.Margin.Top + resizeTarget.Margin.Bottom;
            minHeight = double.IsNaN(resizeTarget.MinHeight) || resizeTarget.MinHeight < minHeight ? minHeight : resizeTarget.MinHeight;
            var height = Math.Max(minHeight, args.BaseSize.Height + args.VerticalChange);
            if (!double.IsInfinity(resizeTarget.MaxHeight))
            {
                height = Math.Min(height, resizeTarget.MaxHeight);
            }
            resizeTarget.Height = height;
        }

        /// <summary>
        /// ドラッグして左のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeLeftByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeLeft(resizeTarget, x);
                        });
        }

        /// <summary>
        /// ドラッグして上のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeTopByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeTop(resizeTarget, x);
                        });
        }

        /// <summary>
        /// ドラッグして右のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeRightByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeRight(resizeTarget, x);
                        });
        }

        /// <summary>
        /// ドラッグして下のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeBottomByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeBottom(resizeTarget, x);
                        });
        }

        /// <summary>
        /// ドラッグして左と上のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeLeftTopByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeLeft(resizeTarget, x);
                            ResizeTop(resizeTarget, x);
                        });
        }

        /// <summary>
        /// ドラッグして右と上のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeRightTopByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeTop(resizeTarget, x);
                            ResizeRight(resizeTarget, x);
                        });
        }

        /// <summary>
        /// ドラッグして右と下のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeRightBottomByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeRight(resizeTarget, x);
                            ResizeBottom(resizeTarget, x);
                        });
        }

        /// <summary>
        /// ドラッグして左と下のサイズを変更する機能を添付します。
        /// </summary>
        /// <param name="dragElement">ドラッグする対象の要素。</param>
        /// <param name="resizeTarget">サイズを変更する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachResizeLeftBottomByDragging(FrameworkElement dragElement, FrameworkElement resizeTarget)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            return dragElement.AsResizeByDraggingObservable(resizeTarget)
                    .Subscribe(x =>
                        {
                            ResizeLeft(resizeTarget, x);
                            ResizeBottom(resizeTarget, x);
                        });
        }

        #endregion

        #region DrawTheLineByHandwriting

        /// <summary>
        /// 基本の線の色。
        /// </summary>
        private static readonly Brush basicLineBrush = new SolidColorBrush(Colors.Black);

        /// <summary>
        /// 基本の筆圧が最大時の線の太さ。
        /// </summary>
        private static readonly double basicMaxStrokeThickness = 20;

        /// <summary>
        /// 手書きで線を引く機能を添付します。
        /// </summary>
        /// <param name="canvas">線を引く対象のキャンバス。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachDrawTheLineByHandwriting(Canvas canvas)
        {
            return AttachDrawTheLineByHandwriting(canvas, canvas, basicLineBrush, basicMaxStrokeThickness);
        }

        /// <summary>
        /// 手書きで線を引く機能を添付します。
        /// </summary>
        /// <param name="canvas">線を引く対象のキャンバス。</param>
        /// <param name="startElement">手書きを開始する対象の要素。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachDrawTheLineByHandwriting(Canvas canvas, FrameworkElement startElement)
        {
            return AttachDrawTheLineByHandwriting(canvas, startElement, basicLineBrush, basicMaxStrokeThickness);
        }

        /// <summary>
        /// 手書きで線を引く機能を添付します。
        /// </summary>
        /// <param name="canvas">線を引く対象のキャンバス。</param>
        /// <param name="startElement">手書きを開始する対象の要素。</param>
        /// <param name="lineBrush">線の色。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachDrawTheLineByHandwriting(Canvas canvas, FrameworkElement startElement, Brush lineBrush)
        {
            return AttachDrawTheLineByHandwriting(canvas, startElement, lineBrush, basicMaxStrokeThickness);
        }

        /// <summary>
        /// 手書きで線を引く機能を添付します。
        /// </summary>
        /// <param name="canvas">線を引く対象のキャンバス。</param>
        /// <param name="startElement">手書きを開始する対象の要素。</param>
        /// <param name="lineBrush">線の色。</param>
        /// <param name="maxStrokeThickness">筆圧が最大時の線の太さ。</param>
        /// <returns>IDisposable。</returns>
        public static IDisposable AttachDrawTheLineByHandwriting(Canvas canvas, FrameworkElement startElement, Brush lineBrush, double maxStrokeThickness)
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return null;
            var pointerId = 0u;
            var prevPosition = new Point();
            var drag = startElement.PointerPressedAsObservable()
                .Where(x => pointerId == 0)
                .Do(x =>
                    {
                        startElement.CapturePointer(x.Pointer);
                        var pointer = x.GetCurrentPoint(canvas);
                        pointerId = pointer.PointerId;
                        prevPosition = pointer.Position;
                    })
                .SelectMany(_ => startElement.PointerMovedAsObservable().Where(x => x.Pointer.PointerId == pointerId))
                .TakeUntil(startElement.PointerReleasedAsObservable().Where(x => x.Pointer.PointerId == pointerId).Do(x =>
                    {
                        startElement.ReleasePointerCapture(x.Pointer);
                        pointerId = 0;
                    }))
                .Select(x => x.GetCurrentPoint(canvas));

            return drag.Where(x => (Math.Sqrt(Math.Pow(x.Position.X - prevPosition.X, 2) + Math.Pow(x.Position.Y - prevPosition.Y, 2))) > 0)
                .Repeat()
                .Subscribe(_ =>
                    {
                        var line = new Line();
                        line.X1 = prevPosition.X;
                        line.Y1 = prevPosition.Y;
                        line.X2 = _.Position.X;
                        line.Y2 = _.Position.Y;
                        line.StrokeStartLineCap = PenLineCap.Round;
                        line.StrokeEndLineCap = PenLineCap.Round;
                        line.StrokeThickness = maxStrokeThickness * _.Properties.Pressure;  //線の太さ×筆圧
                        line.Stroke = lineBrush;
                        canvas.Children.Add(line);
                        prevPosition = _.Position;
                    });
        }

        #endregion
    }
}
