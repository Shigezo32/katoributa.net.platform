﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Katoributa.Net.Platform.Mvvm.Extensions;
using Microsoft.Xaml.Interactivity;
using Windows.UI.Xaml;

namespace Katoributa.Net.Platform.Mvvm.Behaviors
{
    /// <summary>
    /// 対象の要素の親となるPopupを閉じるAction。
    /// </summary>
    public class CloseParentPopupAction : DependencyObject, IAction
    {
        /// <summary>
        /// 対象の要素の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty TargetProperty =
            DependencyProperty.Register("Target", typeof(FrameworkElement), typeof(CloseParentPopupAction), new PropertyMetadata(null));

        /// <summary>
        /// 対象の要素。
        /// </summary>
        public FrameworkElement Target
        {
            get { return (FrameworkElement)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }

        /// <summary>
        /// アクションを実行します。
        /// </summary>
        /// <param name="sender">ビヘイビアーによりアクションに渡される要素。</param>
        /// <param name="parameter">ビヘイビアーによりアクションに渡されるパラメータ。</param>
        /// <returns>アクションの結果。</returns>
        public object Execute(object sender, object parameter)
        {
            var element = sender as FrameworkElement;
            if (this.Target != null)
            {
                element = this.Target;
            }
            if (element == null) { return false; }

            var parentPopup = element.FindPopup();
            if (parentPopup != null)
            {
                parentPopup.IsOpen = false;
            }
            return true;
        }
    }
}
