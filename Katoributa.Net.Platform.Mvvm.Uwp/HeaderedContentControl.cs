﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// 単一のコンテンツを含みヘッダーを持つコントロール。
    /// </summary>
    public class HeaderedContentControl : DisposableContentControl
    {
        /// <summary>
        /// HeaderedContentControlのインスタンスを初期化します。
        /// </summary>
        public HeaderedContentControl()
        {
            //HACK:Shigeta DefaultStyleKeyを設定するとデザイナでエラーが発生する。。
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return;
            this.DefaultStyleKey = typeof(HeaderedContentControl);
        }

        /// <summary>
        /// ヘッダーの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(object), typeof(HeaderedContentControl), new PropertyMetadata(null, OnHeaderChanged));

        /// <summary>
        /// ヘッダーの依存関係プロパティが変更されたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private static void OnHeaderChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as HeaderedContentControl;
            control.OnHeaderChanged(e.OldValue, e.NewValue);
        }

        /// <summary>
        /// Headerプロパティが変更されたときに呼び出されます。
        /// </summary>
        /// <param name="oldHeader">Headerプロパティの変更前の値。</param>
        /// <param name="newHeader">Headerプロパティの変更後の値。</param>
        protected virtual void OnHeaderChanged(object oldHeader, object newHeader)
        {
        }

        /// <summary>
        /// ヘッダー。
        /// </summary>
        public object Header
        {
            get { return (object)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        /// <summary>
        /// ヘッダーを表示するために使用するデータテンプレートの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty HeaderTemplateProperty =
            DependencyProperty.Register("HeaderTemplate", typeof(DataTemplate), typeof(HeaderedContentControl), new PropertyMetadata(null, OnHeaderTemplateChanged));

        /// <summary>
        /// ヘッダーを表示するために使用するデータテンプレートの依存関係プロパティが変更されたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private static void OnHeaderTemplateChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as HeaderedContentControl;
            control.OnHeaderTemplateChanged(e.OldValue as DataTemplate, e.NewValue as DataTemplate);
        }

        /// <summary>
        /// HeaderTemplateプロパティが変更されたときに呼び出されます。
        /// </summary>
        /// <param name="oldHeaderTemplate">HeaderTemplateプロパティの変更前の値。</param>
        /// <param name="newHeaderTemplate">HeaderTemplateプロパティの変更後の値。</param>
        protected virtual void OnHeaderTemplateChanged(DataTemplate oldHeaderTemplate, DataTemplate newHeaderTemplate)
        {
        }

        /// <summary>
        /// ヘッダーを表示するために使用するデータテンプレート。
        /// </summary>
        public DataTemplate HeaderTemplate
        {
            get { return (DataTemplate)GetValue(HeaderTemplateProperty); }
            set { SetValue(HeaderTemplateProperty, value); }
        }

        /// <summary>
        /// ヘッダーに適用するDataTemplateを変更する選択オブジェクトの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty HeaderTemplateSelectorProperty =
            DependencyProperty.Register("HeaderTemplateSelector", typeof(DataTemplateSelector), typeof(HeaderedContentControl), new PropertyMetadata(null, OnHeaderTemplateSelectorChanged));

        /// <summary>
        /// ヘッダーに適用するDataTemplateを変更する選択オブジェクトの依存関係プロパティが変更されたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private static void OnHeaderTemplateSelectorChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as HeaderedContentControl;
            control.OnHeaderTemplateSelectorChanged(e.OldValue as DataTemplateSelector, e.NewValue as DataTemplateSelector);
        }

        /// <summary>
        /// HeaderTemplateSelectorプロパティが変更されたときに呼び出されます。
        /// </summary>
        /// <param name="oldHeaderTemplateSelector">HeaderTemplateSelectorプロパティの変更前の値。</param>
        /// <param name="newHeaderTemplateSelector">HeaderTemplateSelectorプロパティの変更後の値。</param>
        protected virtual void OnHeaderTemplateSelectorChanged(DataTemplateSelector oldHeaderTemplateSelector, DataTemplateSelector newHeaderTemplateSelector)
        {
        }

        /// <summary>
        /// ヘッダーに適用するDataTemplateを変更する選択オブジェクト。
        /// </summary>
        public DataTemplateSelector HeaderTemplateSelector
        {
            get { return (DataTemplateSelector)GetValue(HeaderTemplateSelectorProperty); }
            set { SetValue(HeaderTemplateSelectorProperty, value); }
        }

        /// <summary>
        /// ヘッダーに適用するTransitionスタイル要素のコレクションの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty HeaderTransitionsProperty =
            DependencyProperty.Register("HeaderTransitions", typeof(TransitionCollection), typeof(HeaderedContentControl), new PropertyMetadata(null));

        /// <summary>
        /// ヘッダーに適用するTransitionスタイル要素のコレクション。
        /// </summary>
        public TransitionCollection HeaderTransitions
        {
            get { return (TransitionCollection)GetValue(HeaderTransitionsProperty); }
            set { SetValue(HeaderTransitionsProperty, value); }
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
            }
        }
    }
}
