﻿using System;
using System.Collections.Generic;
using Katoributa.Net.Platform.Mvvm.Commands;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// レイアウトの変更などに対応したページです。
    /// </summary>
    [Windows.Foundation.Metadata.WebHostHidden]
    public class LayoutAwarePage : DisposablePage
    {
        /// <summary>
        /// スナップ状態の幅。
        /// </summary>
        protected double SnappedWidth = 320;

        /// <summary>
        /// LayoutAwarePageのインスタンスを初期化します。
        /// </summary>
        public LayoutAwarePage()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) { return; }
            this.SizeChanged += LayoutAwarePage_SizeChanged;
            this.DataContextChanged += LayoutAwarePage_DataContextChanged;
        }

        /// <summary>
        /// LayoutAwarePageがLoadされたあとに発生します。
        /// </summary>
        /// <param name="e">イベントのデータ。</param>
        protected override void OnLoaded(RoutedEventArgs e)
        {
            // キーボードおよびマウスのナビゲーションは、ウィンドウ全体を使用する場合のみ適用されます
            if (this.ActualHeight == Window.Current.Bounds.Height
                && this.ActualWidth == Window.Current.Bounds.Width)
            {
                // ウィンドウで直接待機するため、フォーカスは不要です
                Window.Current.CoreWindow.Dispatcher.AcceleratorKeyActivated += CoreDispatcher_AcceleratorKeyActivated;
                Window.Current.CoreWindow.PointerPressed += CoreWindow_PointerPressed;
            }
#if WINDOWS_UWP
            SystemNavigationManager.GetForCurrentView().BackRequested += SystemNavigationManager_BackRequested;
#endif
        }

        #region ナビゲーション サポート

        /// <summary>
        /// 前の状態に戻るコマンド。
        /// </summary>
        public RelayCommand GoBackCommand
        {
            get
            {
                if (_goBackCommand == null)
                {
                    _goBackCommand = new RelayCommand(GoBack, CanGoBack);
                }
                return _goBackCommand;
            }
        }
        private RelayCommand _goBackCommand;

        /// <summary>
        /// 前の状態に戻ります。
        /// </summary>
        protected virtual void GoBack()
        {
            if (CanGoBack()) this.Frame.GoBack();
        }

        /// <summary>
        /// 前の状態に戻れるかどうか確認します。
        /// </summary>
        /// <returns>前の状態に戻れる場合はtrue、戻れない場合はfalse。</returns>
        protected virtual bool CanGoBack()
        {
            var logicalPageBack = this.UsingLogicalPageNavigation();
            var physicalPageBack = this.Frame != null && this.Frame.CanGoBack;
            return logicalPageBack || physicalPageBack;
        }

        /// <summary>
        /// 最上位のページに戻るコマンド。
        /// </summary>
        public RelayCommand GoHomeCommand
        {
            get
            {
                if (_goHomeCommand == null)
                {
                    _goHomeCommand = new RelayCommand(GoHome, CanGoHome);
                }
                return _goHomeCommand;
            }
        }
        private RelayCommand _goHomeCommand;

        /// <summary>
        /// 最上位のページに戻ります。
        /// </summary>
        protected virtual void GoHome()
        {
            // ナビゲーション フレームを使用して最上位のページに戻ります
            if (this.Frame != null)
            {
                while (this.Frame.CanGoBack) this.Frame.GoBack();
            }
        }

        /// <summary>
        /// 最上位のページに戻れるかどうか確認します。
        /// </summary>
        /// <returns>最上位のページに戻れる場合はtrue、戻れない場合はfalse。</returns>
        protected virtual bool CanGoHome()
        {
            return CanGoBack();
        }

        /// <summary>
        /// 次の状態に進むコマンド。
        /// </summary>
        public RelayCommand GoForwardCommand
        {
            get
            {
                if (_goForwardCommand == null)
                {
                    _goForwardCommand = new RelayCommand(GoForward, CanGoBack);
                }
                return _goForwardCommand;
            }
        }
        private RelayCommand _goForwardCommand;

        /// <summary>
        /// 次のページに進みます。
        /// </summary>
        protected virtual void GoForward()
        {
            // ナビゲーション フレームを使用して次のページに進みます
            if (CanForward()) this.Frame.GoForward();
        }

        /// <summary>
        /// 次のページに進めるかどうか確認します。
        /// </summary>
        /// <returns>次のページに進める場合はtrue、戻れない場合はfalse。</returns>
        protected virtual bool CanForward()
        {
            return this.Frame != null && this.Frame.CanGoForward;
        }

        /// <summary>
        /// このページがアクティブで、ウィンドウ全体を使用する場合、Alt キーの組み合わせなどのシステム キーを含む、
        /// キーボード操作で呼び出されます。ページがフォーカスされていないときでも、
        /// ページ間のキーボード ナビゲーションの検出に使用されます。
        /// </summary>
        /// <param name="sender">イベントをトリガーしたインスタンス。</param>
        /// <param name="args">イベントが発生する条件を説明するイベント データ。</param>
        private void CoreDispatcher_AcceleratorKeyActivated(CoreDispatcher sender, AcceleratorKeyEventArgs args)
        {
            var virtualKey = args.VirtualKey;

            // 左方向キーや右方向キー、または専用に設定した前に戻るキーや次に進むキーを押した場合のみ、
            // 詳細を調査します
            if ((args.EventType == CoreAcceleratorKeyEventType.SystemKeyDown ||
                args.EventType == CoreAcceleratorKeyEventType.KeyDown) &&
                (virtualKey == VirtualKey.Left || virtualKey == VirtualKey.Right ||
                (int)virtualKey == 166 || (int)virtualKey == 167))
            {
                var coreWindow = Window.Current.CoreWindow;
                var downState = CoreVirtualKeyStates.Down;
                bool menuKey = (coreWindow.GetKeyState(VirtualKey.Menu) & downState) == downState;
                bool controlKey = (coreWindow.GetKeyState(VirtualKey.Control) & downState) == downState;
                bool shiftKey = (coreWindow.GetKeyState(VirtualKey.Shift) & downState) == downState;
                bool noModifiers = !menuKey && !controlKey && !shiftKey;
                bool onlyAlt = menuKey && !controlKey && !shiftKey;

                if (((int)virtualKey == 166 && noModifiers) ||
                    (virtualKey == VirtualKey.Left && onlyAlt))
                {
                    // 前に戻るキーまたは Alt キーを押しながら左方向キーを押すと前に戻ります
                    args.Handled = true;
                    this.GoBackCommand.Execute();
                }
                else if (((int)virtualKey == 167 && noModifiers) ||
                    (virtualKey == VirtualKey.Right && onlyAlt))
                {
                    // 次に進むキーまたは Alt キーを押しながら右方向キーを押すと次に進みます
                    args.Handled = true;
                    this.GoForwardCommand.Execute();
                }
            }
        }

        /// <summary>
        /// このページがアクティブで、ウィンドウ全体を使用する場合、マウスのクリック、タッチ スクリーンのタップなどの
        /// 操作で呼び出されます。ページ間を移動するため、マウス ボタンのクリックによるブラウザー スタイルの
        /// 次に進むおよび前に戻る操作の検出に使用されます。
        /// </summary>
        /// <param name="sender">イベントをトリガーしたインスタンス。</param>
        /// <param name="args">イベントが発生する条件を説明するイベント データ。</param>
        private void CoreWindow_PointerPressed(CoreWindow sender, PointerEventArgs args)
        {
            var properties = args.CurrentPoint.Properties;

            // 左、右、および中央ボタンを使用したボタン操作を無視します
            if (properties.IsLeftButtonPressed || properties.IsRightButtonPressed ||
                properties.IsMiddleButtonPressed) return;

            // [戻る] または [進む] を押すと適切に移動します (両方同時には押しません)
            bool backPressed = properties.IsXButton1Pressed;
            bool forwardPressed = properties.IsXButton2Pressed;
            if (backPressed ^ forwardPressed)
            {
                args.Handled = true;
                if (backPressed) this.GoBackCommand.Execute();
                if (forwardPressed) this.GoForwardCommand.Execute();
            }
        }

#if WINDOWS_UWP
        /// <summary>
        /// システムの戻るが呼び出されたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private void SystemNavigationManager_BackRequested(object sender, BackRequestedEventArgs e)
        {
            OnBackRequested(e);
        }

        /// <summary>
        /// システムの戻るが呼び出されたあとに発生します。
        /// </summary>
        /// <param name="e">イベントのデータ。</param>
        protected virtual void OnBackRequested(BackRequestedEventArgs e)
        {
            if (this.GoBackCommand.CanExecute())
            {
                e.Handled = true;
                this.GoBackCommand.Execute();
            }
        }
#endif

        #endregion

        #region 表示状態の切り替え

#if WINDOWS_UWP
        /// <summary>
        /// フルスクリーンモードかの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty IsFullScreenModeProperty =
            DependencyProperty.Register("IsFullScreenMode", typeof(bool), typeof(LayoutAwarePage), new PropertyMetadata(null));

        /// <summary>
        /// フルスクリーンモードか。
        /// </summary>
        public bool IsFullScreenMode
        {
            get { return (bool)GetValue(IsFullScreenModeProperty); }
            private set { SetValue(IsFullScreenModeProperty, value); }
        }
#endif

        /// <summary>
        /// LayoutAwarePage の ActualHeight プロパティまたは ActualWidth プロパティの値が変更されると発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private void LayoutAwarePage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            OnSizeChanged(e);
#if WINDOWS_UWP
            this.IsFullScreenMode = ApplicationView.GetForCurrentView().IsFullScreenMode;
#endif
        }

        /// <summary>
        /// LayoutAwarePage の ActualHeight プロパティまたは ActualWidth プロパティの値が変更されると発生します。
        /// </summary>
        /// <param name="e">イベントのデータ。</param>
        protected virtual void OnSizeChanged(SizeChangedEventArgs e)
        {
            InvalidateVisualState();
        }

        /// <summary>
        /// DataContextが変更されたときに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="args">イベントのデータ。</param>
        private void LayoutAwarePage_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            OnDataContextChanged(args);
        }

        /// <summary>
        /// DataContextが変更されたときに発生します。
        /// </summary>
        /// <param name="args">イベントのデータ。</param>
        protected virtual void OnDataContextChanged(DataContextChangedEventArgs args)
        {
            InvalidateVisualState();
        }

        /// <summary>
        /// 論理ページナビゲーションを使用するかどうかを取得します。
        /// </summary>
        /// <param name="view">ApplicationView。</param>
        /// <returns>使用する場合はtrue。使用しない場合はfalse。</returns>
        protected virtual bool UsingLogicalPageNavigation(ApplicationView view = null)
        {
            return false;
        }

        /// <summary>
        /// ApplicationViewの状態を、ページ内の表示状態管理で使用できる文字列に変換します。
        /// </summary>
        /// <param name="view">ApplicationView。</param>
        /// <returns><see cref="VisualStateManager"/> の実行に使用される表示状態の名前</returns>
        /// <seealso cref="InvalidateVisualState"/>
        protected virtual string DetermineVisualState(ApplicationView view)
        {
            var logicalPageBack = this.UsingLogicalPageNavigation(view);
            var windowWidth = Window.Current.Bounds.Width;
            var stateName = "";
            switch (view.Orientation)
            {
                case ApplicationViewOrientation.Landscape:
                    if (windowWidth >= 1366)
                    {
                        return "FullScreenLandscapeOrWide";
                    }
                    else
                    {
                        return "FilledOrNarrow";
                    }
                case ApplicationViewOrientation.Portrait:
#pragma warning disable 618
                    if (view.IsFullScreen)
#pragma warning restore 618
                    {
                        stateName = "FullScreenPortrait";
                    }
                    else if (windowWidth > this.SnappedWidth)
                    {
                        stateName = "Portrait";
                    }
                    else
                    {
                        stateName = "Snapped";
                    }
                    break;
            }
            return logicalPageBack ? stateName + "_Detail" : stateName;
        }

        /// <summary>
        /// 適切な表示状態を使用した表示状態の変更を待機しているすべてのコントロールを更新します。
        /// </summary>
        /// <remarks>
        /// 通常、ビューステートが変更されていない場合でも、別の値が返される可能性がある事を知らせるために
        /// <see cref="DetermineVisualState"/> をオーバーライドすることで使用されます。
        /// </remarks>
        protected virtual void InvalidateVisualState()
        {
            string visualState = DetermineVisualState(ApplicationView.GetForCurrentView());
            VisualStateManager.GoToState(this, visualState, false);
            this.GoBackCommand.RaiseCanExecuteChanged();
            this.GoHomeCommand.RaiseCanExecuteChanged();
            this.GoForwardCommand.RaiseCanExecuteChanged();
#if WINDOWS_UWP
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility
                = this.CanGoBack() ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
#endif
        }

        #endregion

        #region プロセス継続時間管理

        private String _pageKey;

        /// <summary>
        /// 画面遷移の履歴をクリアします。
        /// </summary>
        public void ClearPageNavigationStack()
        {
            if (this.Frame != null)
            {
                this.Frame.BackStack.Clear();
                this.Frame.ForwardStack.Clear();
                this._pageKey = "Page-" + this.Frame.BackStackDepth;
                SuspensionManager.SessionStateForFrame(this.Frame).Clear();
                this.GoBackCommand.RaiseCanExecuteChanged();
                this.GoHomeCommand.RaiseCanExecuteChanged();
                this.GoForwardCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// 画面遷移の戻る履歴を削除します。
        /// </summary>
        /// <param name="entry">履歴。</param>
        public void RemoveBackPageStack(PageStackEntry entry)
        {
            if (entry == null) { throw new ArgumentNullException(nameof(entry)); }
            if (this.Frame == null) { return; }

            var frameState = SuspensionManager.SessionStateForFrame(this.Frame);
            var backStack = this.Frame.BackStack;

            var index = backStack.IndexOf(entry);
            if (index < 0) { return; }

            backStack.Remove(entry);
            frameState.Remove($"Page-{index}");

            while (frameState.ContainsKey($"Page-{++index}"))
            {
                frameState[$"Page-{index - 1}"] = frameState[$"Page-{index}"];
                frameState.Remove($"Page-{index}");
            }

            var page = this.Frame.Content as LayoutAwarePage;
            if (page != null)
            {
                page._pageKey = $"Page-{this.Frame.BackStackDepth}";
            }
        }

        /// <summary>
        /// このページがフレームに表示されるときに呼び出されます。
        /// </summary>
        /// <param name="e">このページにどのように到達したかを説明するイベント データ。Parameter 
        /// プロパティは、表示するグループを示します。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // ナビゲーションを通じてキャッシュ ページに戻るときに、状態の読み込みが発生しないようにします
            if (this._pageKey != null) return;

            var frameState = SuspensionManager.SessionStateForFrame(this.Frame);
            this._pageKey = "Page-" + this.Frame.BackStackDepth;
            InvalidateVisualState();

            if (e.NavigationMode == NavigationMode.New)
            {
                // 新しいページをナビゲーション スタックに追加するとき、次に進むナビゲーションの
                // 既存の状態をクリアします
                var nextPageKey = this._pageKey;
                int nextPageIndex = this.Frame.BackStackDepth;
                while (frameState.Remove(nextPageKey))
                {
                    nextPageIndex++;
                    nextPageKey = "Page-" + nextPageIndex;
                }

                // ナビゲーション パラメーターを新しいページに渡します
                this.LoadState(e.Parameter, null);
            }
            else
            {
                // ナビゲーション パラメーターおよび保存されたページの状態をページに渡します。
                // このとき、中断状態の読み込みや、キャッシュから破棄されたページの再作成と同じ対策を
                // 使用します
                this.LoadState(e.Parameter, (Dictionary<String, Object>)frameState[this._pageKey]);
            }
        }

        /// <summary>
        /// このページがフレームに表示されなくなるときに呼び出されます。
        /// </summary>
        /// <param name="e">このページにどのように到達したかを説明するイベント データ。Parameter 
        /// プロパティは、表示するグループを示します。</param>
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            var frameState = SuspensionManager.SessionStateForFrame(this.Frame);
            var pageState = new Dictionary<String, Object>();
            this.SaveState(pageState);
            frameState[_pageKey] = pageState;
        }

        /// <summary>
        /// このページには、移動中に渡されるコンテンツを設定します。前のセッションからページを
        /// 再作成する場合は、保存状態も指定されます。
        /// </summary>
        /// <param name="navigationParameter">このページが最初に要求されたときに
        /// <see cref="Frame.Navigate(Type, Object)"/> に渡されたパラメーター値。
        /// </param>
        /// <param name="pageState">前のセッションでこのページによって保存された状態の
        /// ディクショナリ。ページに初めてアクセスするとき、状態は null になります。</param>
        protected virtual void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// アプリケーションが中断される場合、またはページがナビゲーション キャッシュから破棄される場合、
        /// このページに関連付けられた状態を保存します。値は、
        /// <see cref="SuspensionManager.SessionState"/> のシリアル化の要件に準拠する必要があります。
        /// </summary>
        /// <param name="pageState">シリアル化可能な状態で作成される空のディクショナリ。</param>
        protected virtual void SaveState(Dictionary<String, Object> pageState)
        {
        }

        #endregion

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                Window.Current.CoreWindow.Dispatcher.AcceleratorKeyActivated -= CoreDispatcher_AcceleratorKeyActivated;
                Window.Current.CoreWindow.PointerPressed -= CoreWindow_PointerPressed;
#if WINDOWS_UWP
                SystemNavigationManager.GetForCurrentView().BackRequested -= SystemNavigationManager_BackRequested;
#endif
                this.SizeChanged -= LayoutAwarePage_SizeChanged;
                this.DataContextChanged -= LayoutAwarePage_DataContextChanged;
            }
        }
    }
}
