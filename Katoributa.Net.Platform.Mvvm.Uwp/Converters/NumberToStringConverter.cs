﻿using Katoributa.Net.Platform.Core.ComponentModel;
using System;
using System.Globalization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// 数値を文字列に変換するコンバーター。
    /// </summary>
    public sealed class NumberToStringConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
            {
                return value;
            }
            #region byte
            if (value is byte)
            {
                var num = (byte)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region sbyte
            if (value is sbyte)
            {
                var num = (sbyte)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region short
            if (value is short)
            {
                var num = (short)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region ushort
            if (value is ushort)
            {
                var num = (ushort)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region int
            if (value is int)
            {
                var num = (int)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region uint
            if (value is uint)
            {
                var num = (uint)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region long
            if (value is long)
            {
                var num = (long)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region ulong
            if (value is ulong)
            {
                var num = (ulong)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region float
            if (value is float)
            {
                var num = (float)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region double
            if (value is double)
            {
                var num = (double)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            #region decimal
            if (value is decimal)
            {
                var num = (decimal)value;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    return num.ToString(provider);
                }
                var format = parameter as string;
                if (format != null)
                {
                    return num.ToString(format);
                }
                return num.ToString();
            }
            #endregion
            return value;
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            var text = value as string;
            if (string.IsNullOrWhiteSpace(text))
            {
                return DependencyProperty.UnsetValue;
            }

            #region byte
            if (targetType == typeof(byte))
            {
                var num = byte.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (byte.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (byte.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region sbyte
            if (targetType == typeof(sbyte))
            {
                var num = sbyte.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (sbyte.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (sbyte.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region short
            if (targetType == typeof(short))
            {
                var num = short.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (short.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (short.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region ushort
            if (targetType == typeof(ushort))
            {
                var num = ushort.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (ushort.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (ushort.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region int
            if (targetType == typeof(int))
            {
                var num = int.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (int.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (int.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region uint
            if (targetType == typeof(uint))
            {
                var num = uint.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (uint.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (uint.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region long
            if (targetType == typeof(long))
            {
                var num = long.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (long.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (long.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region ulong
            if (targetType == typeof(ulong))
            {
                var num = ulong.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (ulong.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (ulong.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region float
            if (targetType == typeof(float))
            {
                var num = float.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (float.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (float.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region double
            if (targetType == typeof(double))
            {
                var num = double.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (double.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (double.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            #region decimal
            if (targetType == typeof(decimal))
            {
                var num = decimal.MinValue;
                var provider = parameter as IFormatProvider;
                if (provider != null)
                {
                    if (decimal.TryParse(text, NumberStyles.None, provider, out num))
                    {
                        return num;
                    }
                }
                if (decimal.TryParse(text, out num))
                {
                    return num;
                }
                return DependencyProperty.UnsetValue;
            }
            #endregion
            return DependencyProperty.UnsetValue;
        }
    }
}
