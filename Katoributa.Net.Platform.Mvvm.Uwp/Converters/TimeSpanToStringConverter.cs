﻿using System;
using System.Globalization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// TimeSpanを文字列に変換するコンバーター。
    /// </summary>
    public sealed class TimeSpanToStringConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is TimeSpan))
            {
                return value;
            }
            var timeSpan = (TimeSpan)value;

            var format = parameter as string;
            if (format != null)
            {
                return timeSpan.ToString(format);
            }

            return timeSpan.ToString();
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            var text = value as string;
            if (string.IsNullOrWhiteSpace(text))
            {
                return DependencyProperty.UnsetValue;
            }
            var timeSpan = TimeSpan.Zero;

            var format = parameter as string;
            if (format != null)
            {
                if (TimeSpan.TryParseExact(text, format, null, TimeSpanStyles.None, out timeSpan))
                {
                    return timeSpan;
                }
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
