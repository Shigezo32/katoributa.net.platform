﻿using Katoributa.Net.Platform.Core;
using Katoributa.Net.Platform.Core.ComponentModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// 比較を行い結果を返す値コンバーター。
    /// </summary>
    public sealed class ComparisonConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var condition = parameter as ConverterComparisonCondition;
            if (condition == null)
            {
                condition = new ConverterComparisonCondition() { Value = parameter, Operator = ComparisonOperator.Equal };
            }
            condition.ComparisonValue = value;
            return condition.Evaluate();
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            var condition = parameter as ConverterComparisonCondition;
            if (condition == null)
            {
                condition = new ConverterComparisonCondition()
                {
                    Value = true,
                    Operator = ComparisonOperator.Equal,
                    MatchedValue = parameter,
                    UnmatchedValue = DependencyProperty.UnsetValue,
                };
            }
            else
            {
                if (condition.Operator != ComparisonOperator.Equal)
                {
                    return DependencyProperty.UnsetValue;
                }
                var temp = condition.Value;
                condition.Value = condition.MatchedValue;
                condition.MatchedValue = temp;
                condition.UnmatchedValue = DependencyProperty.UnsetValue;
            }
            condition.ComparisonValue = value;
            var result = condition.Evaluate();

            var converter = TypeDescriptor.GetConverter(targetType);
            if (converter.CanConvertFrom(result.GetType()))
            {
                return converter.ConvertFrom(result);
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
