﻿using Katoributa.Net.Platform.Core;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// コンバーターの比較条件。
    /// </summary>
    public class ConverterComparisonCondition : DependencyObject, ICondition
    {
        /// <summary>
        /// 比較元の値の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(ConverterComparisonCondition), new PropertyMetadata(null));

        /// <summary>
        /// 比較元の値。
        /// </summary>
        public object Value
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        /// <summary>
        /// 比較演算子の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty OperatorProperty =
            DependencyProperty.Register("Operator", typeof(ComparisonOperator), typeof(ConverterComparisonCondition), new PropertyMetadata(ComparisonOperator.Equal));

        /// <summary>
        /// 比較演算子。
        /// </summary>
        public ComparisonOperator Operator
        {
            get { return (ComparisonOperator)GetValue(OperatorProperty); }
            set { SetValue(OperatorProperty, value); }
        }

        /// <summary>
        /// 比較する値。
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public object ComparisonValue { get; set; }

        /// <summary>
        /// 一致したときの値の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty MatchedValueProperty =
            DependencyProperty.Register("MatchedValue", typeof(object), typeof(ConverterComparisonCondition), new PropertyMetadata(true));

        /// <summary>
        /// 一致したときの値。
        /// </summary>
        public object MatchedValue
        {
            get { return GetValue(MatchedValueProperty); }
            set { SetValue(MatchedValueProperty, value); }
        }

        /// <summary>
        /// 一致したときの値の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty UnmatchedValueProperty =
            DependencyProperty.Register("UnmatchedValue", typeof(object), typeof(ConverterComparisonCondition), new PropertyMetadata(false));

        /// <summary>
        /// 一致しなかったときの値。
        /// </summary>
        public object UnmatchedValue
        {
            get { return GetValue(UnmatchedValueProperty); }
            set { SetValue(UnmatchedValueProperty, value); }
        }

        /// <summary>
        /// 評価します。
        /// </summary>
        /// <returns>一致したときはMatchedValue。一致しなかったときはUnmatchedValue。</returns>
        public object Evaluate()
        {
            if (((ICondition)this).Evaluate())
            {
                return this.MatchedValue;
            }
            return this.UnmatchedValue;
        }

        /// <summary>
        /// 評価します。
        /// </summary>
        /// <returns></returns>
        bool ICondition.Evaluate()
        {
            return ComparisonLogic.EvaluateImpl(this.ComparisonValue, this.Operator, this.Value);
        }
    }
}
