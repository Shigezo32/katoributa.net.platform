﻿using System;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// true を false に、および false を true に変換する値コンバーター。
    /// </summary>
    public sealed class BooleanNegationConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return !(value is bool && (bool)value);
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return !(value is bool && (bool)value);
        }
    }
}
