﻿using Katoributa.Net.Platform.Core;
using Katoributa.Net.Platform.Core.ComponentModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// Enumを比較し結果を返す値コンバーター。
    /// </summary>
    /// <remarks>
    /// int型、Nullable&lt;int&gt;型、Enum型とBindしたい場合はこのクラスを利用できます。
    /// Nullable&lt;Enum&gt;型とBindしたい場合はEnumComparisonConverter&lt;TEnum&gt;を継承して利用してください。
    /// </remarks>
    public sealed class EnumComparisonConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var condition = parameter as ConverterComparisonCondition;
            if (condition == null)
            {
                condition = new ConverterComparisonCondition() { Value = parameter, Operator = ComparisonOperator.Equal };
            }
            condition.ComparisonValue = value;
            return condition.Evaluate();
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if ((bool)value)
            {
                var converter = TypeDescriptor.GetConverter(targetType);
                if (converter.CanConvertFrom(parameter.GetType()))
                {
                    return converter.ConvertFrom(parameter);
                }
                return parameter;
            }
            else
            {
                return DependencyProperty.UnsetValue;
            }
        }
    }
}
