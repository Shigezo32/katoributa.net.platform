﻿using Katoributa.Net.Platform.Core.ComponentModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// true を <see cref="Visibility.Visible"/> に、および false を 
    /// <see cref="Visibility.Collapsed"/> に変換する値コンバーター。
    /// </summary>
    public sealed class BooleanToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">結果を反転させるかどうか。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var négation = false;
            if (parameter != null)
            {
                var converter = TypeDescriptor.GetConverter(typeof(bool));
                if (converter.CanConvertFrom(parameter.GetType()))
                {
                    négation = (bool)converter.ConvertFrom(parameter);
                }
            }
            if (!négation)
            {
                return (value is bool && (bool)value) ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                return (value is bool && (bool)value) ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">結果を反転させるかどうか。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            var négation = false;
            if (parameter != null)
            {
                var converter = TypeDescriptor.GetConverter(typeof(bool));
                if (converter.CanConvertFrom(parameter.GetType()))
                {
                    négation = (bool)converter.ConvertFrom(parameter);
                }
            }
            if (!négation)
            {
                return value is Visibility && (Visibility)value == Visibility.Visible;
            }
            else
            {
                return value is Visibility && (Visibility)value == Visibility.Collapsed;
            }
        }
    }
}
