﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// 文字列をPathGeometryにする値コンバーター。
    /// </summary>
    public sealed class StringToPathGeometryConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var data = value?.ToString();
            if (string.IsNullOrWhiteSpace(data))
            {
                return DependencyProperty.UnsetValue;
            }
            var pathEnvelope = "<Geometry xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\">{0}</Geometry>";
            return XamlReader.Load(string.Format(pathEnvelope, data)) as Geometry;
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
