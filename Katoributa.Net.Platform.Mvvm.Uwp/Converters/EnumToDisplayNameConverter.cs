﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Katoributa.Net.Platform.Core.Extensions;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// Enumを表示用文字列に変換するコンバーター。
    /// </summary>
    public sealed class EnumToDisplayNameConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var info = value.GetType().GetRuntimeField(value.ToString());
            return DisplayAttributeUtility.GetDisplayName(info);
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
