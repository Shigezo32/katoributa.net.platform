﻿using System;
using System.Globalization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// DateTimeを文字列に変換するコンバーター。
    /// </summary>
    public sealed class DateTimeToStringConverter : IValueConverter
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is DateTime))
            {
                return value;
            }
            var dateTime = (DateTime)value;

            var provider = parameter as IFormatProvider;
            if (provider != null)
            {
                return dateTime.ToString(provider);
            }

            var format = parameter as string;
            if (format != null)
            {
                return dateTime.ToString(format);
            }

            return dateTime.ToString();
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            var text = value as string;
            if (string.IsNullOrWhiteSpace(text))
            {
                return DependencyProperty.UnsetValue;
            }
            var dateTime = DateTime.MinValue;

            var provider = parameter as IFormatProvider;
            if (provider != null)
            {
                if (DateTime.TryParseExact(text, "", provider, DateTimeStyles.None, out dateTime))
                {
                    return dateTime;
                }
            }

            var format = parameter as string;
            if (format != null)
            {
                if (DateTime.TryParseExact(text, format, null, DateTimeStyles.None, out dateTime))
                {
                    return dateTime;
                }
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
