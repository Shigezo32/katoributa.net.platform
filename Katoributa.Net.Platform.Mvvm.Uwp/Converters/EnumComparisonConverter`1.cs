﻿using Katoributa.Net.Platform.Core;
using Katoributa.Net.Platform.Core.ComponentModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Katoributa.Net.Platform.Mvvm.Converters
{
    /// <summary>
    /// Enumを比較し結果を返す値コンバーター。
    /// </summary>
    /// <typeparam name="TEnum">Enum。</typeparam>
    /// <remarks>
    /// Nullable&lt;Enum&gt;型とBindしたい場合はこのクラスを継承して利用してください。
    /// </remarks>
    public class EnumComparisonConverter<TEnum> : IValueConverter
            where TEnum : struct, IComparable, IFormattable
    {
        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var condition = parameter as ConverterComparisonCondition;
            if (condition == null)
            {
                condition = new ConverterComparisonCondition() { Value = parameter, Operator = ComparisonOperator.Equal };
            }
            condition.ComparisonValue = value;
            return condition.Evaluate();
        }

        /// <summary>
        /// 値を変換します。
        /// </summary>
        /// <param name="value">変換する値。</param>
        /// <param name="targetType">対象の型。</param>
        /// <param name="parameter">比較条件、または比較する値。</param>
        /// <param name="language">言語。</param>
        /// <returns>変換された結果。</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if ((bool)value)
            {
                var converter = TypeDescriptor.GetConverter(typeof(TEnum));
                if (converter.CanConvertFrom(parameter.GetType()))
                {
                    return converter.ConvertFrom(parameter);
                }
                return parameter;
            }
            else
            {
                return DependencyProperty.UnsetValue;
            }
        }
    }
}
