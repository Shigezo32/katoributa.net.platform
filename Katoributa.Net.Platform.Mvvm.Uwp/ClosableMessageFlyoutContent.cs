﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Katoributa.Net.Platform.Mvvm.Commands;
using Katoributa.Net.Platform.Mvvm.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// 閉じることが可能なメッセージを表示するFlyoutのコンテンツ。
    /// </summary>
    public class ClosableMessageFlyoutContent : MessageFlyoutContent
    {
        /// <summary>
        /// ClosableMessageFlyoutContentのインスタンスを初期化します。
        /// </summary>
        public ClosableMessageFlyoutContent()
        {
            this.DefaultStyleKey = typeof(ClosableMessageFlyoutContent);
            this.CloseCommand = new RelayCommand(() =>
                {
                    var popup = this.FindPopup();
                    if (popup != null)
                    {
                        popup.IsOpen = false;
                    }
                });
        }

        /// <summary>
        /// 閉じるボタンの可視性の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty CloseButtonVisibilityProperty =
            DependencyProperty.Register("CloseButtonVisibility", typeof(Visibility), typeof(ClosableMessageFlyoutContent), new PropertyMetadata(Visibility.Visible));

        /// <summary>
        /// 閉じるボタンの可視性。
        /// </summary>
        public Visibility CloseButtonVisibility
        {
            get { return (Visibility)GetValue(CloseButtonVisibilityProperty); }
            set { SetValue(CloseButtonVisibilityProperty, value); }
        }

        /// <summary>
        /// 閉じるコマンドの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty CloseCommandProperty =
            DependencyProperty.Register("CloseCommand", typeof(ICommand), typeof(ClosableMessageFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// 閉じるコマンド。
        /// </summary>
        public ICommand CloseCommand
        {
            get { return (ICommand)GetValue(CloseCommandProperty); }
            set { SetValue(CloseCommandProperty, value); }
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
            }
        }
    }
}
