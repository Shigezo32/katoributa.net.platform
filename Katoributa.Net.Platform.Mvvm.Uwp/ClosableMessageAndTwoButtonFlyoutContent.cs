﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// 閉じることが可能な2つのボタンを持つメッセージを表示するFlyoutのコンテンツ。
    /// </summary>
    public sealed class ClosableMessageAndTwoButtonFlyoutContent : ClosableMessageAndButtonFlyoutContent
    {
        /// <summary>
        /// ClosableMessageAndTwoButtonFlyoutContentのインスタンスを初期化します。
        /// </summary>
        public ClosableMessageAndTwoButtonFlyoutContent()
            : base()
        {
            this.DefaultStyleKey = typeof(ClosableMessageAndTwoButtonFlyoutContent);
        }

        /// <summary>
        /// ボタンに表示する内容の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty ButtonContent2Property =
            DependencyProperty.Register("ButtonContent2", typeof(object), typeof(ClosableMessageAndTwoButtonFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// ボタンに表示する内容。
        /// </summary>
        public object ButtonContent2
        {
            get { return (object)GetValue(ButtonContent2Property); }
            set { SetValue(ButtonContent2Property, value); }
        }

        /// <summary>
        /// ボタンに設定するコマンドの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty ButtonCommand2Property =
            DependencyProperty.Register("ButtonCommand2", typeof(ICommand), typeof(ClosableMessageAndTwoButtonFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// ボタンに設定するコマンド。
        /// </summary>
        public ICommand ButtonCommand2
        {
            get { return (ICommand)GetValue(ButtonCommand2Property); }
            set { SetValue(ButtonCommand2Property, value); }
        }

        /// <summary>
        /// ボタンに適用するStyleの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty ButtonStyle2Property =
            DependencyProperty.Register("ButtonStyle2", typeof(Style), typeof(ClosableMessageAndTwoButtonFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// ボタンに適用するStyle。
        /// </summary>
        public Style ButtonStyle2
        {
            get { return (Style)GetValue(ButtonStyle2Property); }
            set { SetValue(ButtonStyle2Property, value); }
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
            }
        }
    }
}
