﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// メッセージを表示するFlyoutのコンテンツ。
    /// </summary>
    public class MessageFlyoutContent : DisposableControl
    {
        /// <summary>
        /// MessageFlyoutContentのインスタンスを初期化します。
        /// </summary>
        public MessageFlyoutContent()
        {
            this.DefaultStyleKey = typeof(MessageFlyoutContent);
        }

        /// <summary>
        /// 表示するメッセージの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(MessageFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// 表示するメッセージ。
        /// </summary>
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        /// <summary>
        /// メッセージの外側の余白の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty MessageMarginProperty =
            DependencyProperty.Register("MessageMargin", typeof(Thickness), typeof(MessageFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// メッセージの外側の余白。
        /// </summary>
        public Thickness MessageMargin
        {
            get { return (Thickness)GetValue(MessageMarginProperty); }
            set { SetValue(MessageMarginProperty, value); }
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
            }
        }
    }
}
