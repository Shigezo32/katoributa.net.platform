﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// 閉じることが可能なボタンを持つメッセージを表示するFlyoutのコンテンツ。
    /// </summary>
    public class ClosableMessageAndButtonFlyoutContent : ClosableMessageFlyoutContent
    {
        /// <summary>
        /// ClosableMessageAndButtonFlyoutContentのインスタンスを初期化します。
        /// </summary>
        public ClosableMessageAndButtonFlyoutContent()
            : base()
        {
            this.DefaultStyleKey = typeof(ClosableMessageAndButtonFlyoutContent);
        }

        /// <summary>
        /// ボタンに表示する内容の依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty ButtonContentProperty =
            DependencyProperty.Register("ButtonContent", typeof(object), typeof(ClosableMessageAndButtonFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// ボタンに表示する内容。
        /// </summary>
        public object ButtonContent
        {
            get { return (object)GetValue(ButtonContentProperty); }
            set { SetValue(ButtonContentProperty, value); }
        }

        /// <summary>
        /// ボタンに設定するコマンドの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty ButtonCommandProperty =
            DependencyProperty.Register("ButtonCommand", typeof(ICommand), typeof(ClosableMessageAndButtonFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// ボタンに設定するコマンド。
        /// </summary>
        public ICommand ButtonCommand
        {
            get { return (ICommand)GetValue(ButtonCommandProperty); }
            set { SetValue(ButtonCommandProperty, value); }
        }

        /// <summary>
        /// ボタンに適用するStyleの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty ButtonStyleProperty =
            DependencyProperty.Register("ButtonStyle", typeof(Style), typeof(ClosableMessageAndButtonFlyoutContent), new PropertyMetadata(null));

        /// <summary>
        /// ボタンに適用するStyle。
        /// </summary>
        public Style ButtonStyle
        {
            get { return (Style)GetValue(ButtonStyleProperty); }
            set { SetValue(ButtonStyleProperty, value); }
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
            }
        }
    }
}
