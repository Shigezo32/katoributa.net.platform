﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Katoributa.Net.Platform.Core.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// Dispose可能なContentControlです。
    /// </summary>
    public class DisposableContentControl : ContentControl, IDisposable
    {
        /// <summary>
        /// DisposableContentControlのインスタンスを初期化します。
        /// </summary>
        public DisposableContentControl()
        {
            this.Loaded += DisposableContentControl_Loaded;
            this.Unloaded += DisposableContentControl_Unloaded;
        }

        /// <summary>
        /// この要素が表示のために読み込まれているかどうか。
        /// </summary>
        protected bool IsLoaded { get; private set; }

        /// <summary>
        /// DisposableContentControlがLoadされたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private void DisposableContentControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.IsLoaded = true;
            OnLoaded(e);
        }

        /// <summary>
        /// DisposableContentControlがLoadされたあとに発生します。
        /// </summary>
        /// <param name="e">イベントのデータ。</param>
        protected virtual void OnLoaded(RoutedEventArgs e)
        {
        }

        /// <summary>
        /// DisposableContentControlがUnloadされたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private void DisposableContentControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.IsLoaded = false;
            OnUnloaded(e);
        }

        /// <summary>
        /// DisposableContentControlがUnloadされたあとに発生します。
        /// </summary>
        /// <param name="e">イベントのデータ。</param>
        protected virtual void OnUnloaded(RoutedEventArgs e)
        {
            this.Dispose();
        }

        /// <summary>
        /// このインスタンスがDispose済みかどうかを表すフラグ。
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// このインスタンスがDispose済みかどうかを表すフラグ。
        /// </summary>
        protected bool Disposed { get { return _disposed; } }

        /// <summary>
        /// このインスタンスがGCに回収される時に呼び出されます。
        /// </summary>
        ~DisposableContentControl()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Dispose可能な要素の一覧。
        /// </summary>
        protected List<IDisposable> DisposableCollection
        {
            get { return _disposableCollection; }
        }
        private readonly List<IDisposable> _disposableCollection = new List<IDisposable>();

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            _disposed = true;
            if (disposing)
            {
                // マネージリソースの解放処理
                this.DataContext = null;
                this.Content = null;
                _disposableCollection.ClearAndDispose();
                this.Loaded -= DisposableContentControl_Loaded;
                this.Unloaded -= DisposableContentControl_Unloaded;
            }
            // アンマネージリソースの解放処理
        }

        /// <summary>
        /// このインスタンスで使用されていたすべてのリソースが解放済みの場合に例外を発生させます。
        /// </summary>
        /// <exception cref="ObjectDisposedException">リソースが解放済みの場合に発生。</exception>
        protected void ThrowExceptionIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().ToString());
            }
        }
    }
}
