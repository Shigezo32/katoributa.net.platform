﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// Window全体に表示するControlです。
    /// </summary>
    public class OverallWindowControl : DisposableControl
    {
        /// <summary>
        /// OverallWindowControlのインスタンスを初期化します。
        /// </summary>
        public OverallWindowControl()
        {
            if (this.TargetWindow != null)
            {
                this.Width = this.TargetWindow.Bounds.Width;
                this.Height = this.TargetWindow.Bounds.Height;
                this.TargetWindow.SizeChanged += Window_SizeChanged;
            }
        }

        /// <summary>
        /// PagesProgressRingがUnloadされたあとに発生します。
        /// </summary>
        /// <param name="e">イベントのデータ。</param>
        protected override void OnUnloaded(RoutedEventArgs e)
        {
            if (this.IsDisposeAtUnload)
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// Windowの大きさが変更されたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private void Window_SizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            this.Width = e.Size.Width;
            this.Height = e.Size.Height;
        }

        #region 依存関係プロパティ

        /// <summary>
        /// 対象のWindowの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty TargetWindowProperty =
            DependencyProperty.Register("TargetWindow", typeof(Window), typeof(OverallWindowControl), new PropertyMetadata(Window.Current, OnTargetWindowChanged));

        /// <summary>
        /// 対象のWindowの依存関係プロパティが変更されたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private static void OnTargetWindowChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as OverallWindowControl;
            var oldWindow = e.OldValue as Window;
            if (oldWindow != null)
            {
                oldWindow.SizeChanged -= control.Window_SizeChanged;
            }
            var newWindow = e.NewValue as Window;
            if (newWindow != null)
            {
                control.Width = newWindow.Bounds.Width;
                control.Height = newWindow.Bounds.Height;
                newWindow.SizeChanged += control.Window_SizeChanged;
            }
        }

        /// <summary>
        /// 対象のWindow。
        /// </summary>
        public Window TargetWindow
        {
            get { return (Window)GetValue(TargetWindowProperty); }
            set { SetValue(TargetWindowProperty, value); }
        }

        /// <summary>
        /// Unload時にDisposeするかどうかの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty IsDisposeAtUnloadProperty =
            DependencyProperty.Register("IsDisposeAtUnload", typeof(bool), typeof(OverallWindowControl), new PropertyMetadata(true));

        /// <summary>
        /// Unload時にDisposeするかどうか。
        /// </summary>
        public bool IsDisposeAtUnload
        {
            get { return (bool)GetValue(IsDisposeAtUnloadProperty); }
            set { SetValue(IsDisposeAtUnloadProperty, value); }
        }

        #endregion

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Window.Current != null)
                {
                    Window.Current.SizeChanged -= Window_SizeChanged;
                }
            }
        }
    }
}
