﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Katoributa.Net.Platform.Mvvm.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Katoributa.Net.Platform.Mvvm
{
    /// <summary>
    /// ドラッグしてサイズが変更できる単一のコンテンツを含むコントロール。
    /// </summary>
    [TemplatePart(Name = ResizableContentControl.PART_LeftHandle, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ResizableContentControl.PART_TopHandle, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ResizableContentControl.PART_RightHandle, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ResizableContentControl.PART_BottomHandle, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ResizableContentControl.PART_LeftTopHandle, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ResizableContentControl.PART_RightTopHandle, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ResizableContentControl.PART_RightBottomHandle, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ResizableContentControl.PART_LeftBottomHandle, Type = typeof(FrameworkElement))]
    public sealed class ResizableContentControl : DisposableContentControl
    {
        private const string PART_LeftHandle = "PART_LeftHandle";
        private const string PART_TopHandle = "PART_TopHandle";
        private const string PART_RightHandle = "PART_RightHandle";
        private const string PART_BottomHandle = "PART_BottomHandle";
        private const string PART_LeftTopHandle = "PART_LeftTopHandle";
        private const string PART_RightTopHandle = "PART_RightTopHandle";
        private const string PART_RightBottomHandle = "PART_RightBottomHandle";
        private const string PART_LeftBottomHandle = "PART_LeftBottomHandle";

        private FrameworkElement _leftHandle = null;
        private FrameworkElement _topHandle = null;
        private FrameworkElement _rightHandle = null;
        private FrameworkElement _bottomHandle = null;
        private FrameworkElement _leftTopHandle = null;
        private FrameworkElement _rightTopHandle = null;
        private FrameworkElement _rightBottomHandle = null;
        private FrameworkElement _leftBottomHandle = null;

        private readonly List<IDisposable> _resizeByDraggingObservers = new List<IDisposable>();

        /// <summary>
        /// ResizableContentControlのインスタンスを初期化します。
        /// </summary>
        public ResizableContentControl()
        {
            //HACK:Shigeta DefaultStyleKeyを設定するとデザイナでエラーが発生する。。
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return;
            this.DefaultStyleKey = typeof(ResizableContentControl);
        }

        /// <summary>
        /// テンプレートが適用されるときに呼び出されます。
        /// </summary>
        protected override void OnApplyTemplate()
        {
            _leftHandle = GetTemplateChild(PART_LeftHandle) as FrameworkElement;
            _topHandle = GetTemplateChild(PART_TopHandle) as FrameworkElement;
            _rightHandle = GetTemplateChild(PART_RightHandle) as FrameworkElement;
            _bottomHandle = GetTemplateChild(PART_BottomHandle) as FrameworkElement;
            _leftTopHandle = GetTemplateChild(PART_LeftTopHandle) as FrameworkElement;
            _rightTopHandle = GetTemplateChild(PART_RightTopHandle) as FrameworkElement;
            _rightBottomHandle = GetTemplateChild(PART_RightBottomHandle) as FrameworkElement;
            _leftBottomHandle = GetTemplateChild(PART_LeftBottomHandle) as FrameworkElement;

            DetachResizeByDragging();
            if (this.CanResize)
            {
                AttachResizeByDragging();
            }

            base.OnApplyTemplate();
        }

        /// <summary>
        /// ドラッグしてサイズを変更する機能を添付します。
        /// </summary>
        private void AttachResizeByDragging()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return;

            if (_leftHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeLeftByDragging(_leftHandle, this));
            }
            if (_topHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeTopByDragging(_topHandle, this));
            }
            if (_rightHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeRightByDragging(_rightHandle, this));
            }
            if (_bottomHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeBottomByDragging(_bottomHandle, this));
            }
            if (_leftTopHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeLeftTopByDragging(_leftTopHandle, this));
            }
            if (_rightTopHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeRightTopByDragging(_rightTopHandle, this));
            }
            if (_rightBottomHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeRightBottomByDragging(_rightBottomHandle, this));
            }
            if (_leftBottomHandle != null)
            {
                _resizeByDraggingObservers.Add(DragAndDropUtility.AttachResizeLeftBottomByDragging(_leftBottomHandle, this));
            }
        }

        /// <summary>
        /// ドラッグしてサイズを変更する機能を解除します。
        /// </summary>
        private void DetachResizeByDragging()
        {
            foreach (var item in _resizeByDraggingObservers.Where(x => x != null))
            {
                item.Dispose();
            }
            _resizeByDraggingObservers.Clear();
        }

        /// <summary>
        /// サイズが変更できるかの依存関係プロパティです。
        /// </summary>
        public static readonly DependencyProperty CanResizeProperty =
            DependencyProperty.Register("CanResize", typeof(bool), typeof(ResizableContentControl), new PropertyMetadata(true, OnCanResizeChanged));

        /// <summary>
        /// サイズが変更できるかの依存関係プロパティが変更されたあとに発生します。
        /// </summary>
        /// <param name="sender">イベントが発生したオブジェクト。</param>
        /// <param name="e">イベントのデータ。</param>
        private static void OnCanResizeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as ResizableContentControl;
            control.DetachResizeByDragging();
            if (control.CanResize)
            {
                control.AttachResizeByDragging();
            }
        }

        /// <summary>
        /// サイズが変更できるか。
        /// </summary>
        public bool CanResize
        {
            get { return (bool)GetValue(CanResizeProperty); }
            set { SetValue(CanResizeProperty, value); }
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
            }
        }
    }
}
