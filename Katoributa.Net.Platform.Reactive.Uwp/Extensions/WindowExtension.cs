﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace Katoributa.Net.Platform.Reactive.Extensions
{
    /// <summary>
    /// Windowに対して追加する便利機能群です。
    /// </summary>
    public static class WindowExtension
    {
        /// <summary>
        /// ActivatedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, WindowActivatedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, WindowActivatedEventArgs>> ActivatedEventPatternAsObservable(this Window window)
        {
            return Observable.FromEventPattern<WindowActivatedEventHandler, WindowActivatedEventArgs>(
                    h => h.Invoke
                    , h => window.Activated += h
                    , h => window.Activated -= h)
                .Select(x => x);
        }

        /// <summary>
        /// ActivatedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;WindowActivatedEventArgs&gt;。</returns>
        public static IObservable<WindowActivatedEventArgs> ActivatedAsObservable(this Window window)
        {
            return window.ActivatedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// ClosedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, CoreWindowEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, CoreWindowEventArgs>> ClosedEventPatternAsObservable(this Window window)
        {
            return Observable.FromEventPattern<WindowClosedEventHandler, CoreWindowEventArgs>(
                    h => h.Invoke
                    , h => window.Closed += h
                    , h => window.Closed -= h)
                .Select(x => x);
        }

        /// <summary>
        /// ClosedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;CoreWindowEventArgs&gt;。</returns>
        public static IObservable<CoreWindowEventArgs> ClosedAsObservable(this Window window)
        {
            return window.ClosedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// SizeChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, WindowSizeChangedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, WindowSizeChangedEventArgs>> SizeChangedEventPatternAsObservable(this Window window)
        {
            return Observable.FromEventPattern<WindowSizeChangedEventHandler, WindowSizeChangedEventArgs>(
                    h => h.Invoke
                    , h => window.SizeChanged += h
                    , h => window.SizeChanged -= h)
                .Select(x => x);
        }

        /// <summary>
        /// SizeChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;WindowSizeChangedEventArgs&gt;。</returns>
        public static IObservable<WindowSizeChangedEventArgs> SizeChangedAsObservable(this Window window)
        {
            return window.SizeChangedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// VisibilityChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, VisibilityChangedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, VisibilityChangedEventArgs>> VisibilityChangedEventPatternAsObservable(this Window window)
        {
            return Observable.FromEventPattern<WindowVisibilityChangedEventHandler, VisibilityChangedEventArgs>(
                    h => h.Invoke
                    , h => window.VisibilityChanged += h
                    , h => window.VisibilityChanged -= h)
                .Select(x => x);
        }

        /// <summary>
        /// VisibilityChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="window">Window。</param>
        /// <returns>IObservable&lt;VisibilityChangedEventArgs&gt;。</returns>
        public static IObservable<VisibilityChangedEventArgs> VisibilityChangedAsObservable(this Window window)
        {
            return window.VisibilityChangedEventPatternAsObservable().Select(x => x.EventArgs);
        }
    }
}
