﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace Katoributa.Net.Platform.Reactive.Extensions
{
    /// <summary>
    /// UIElementに対して追加する便利機能群です。
    /// </summary>
    public static class UIElementExtension
    {
        /// <summary>
        /// DoubleTappedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, DoubleTappedRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, DoubleTappedRoutedEventArgs>> DoubleTappedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<DoubleTappedEventHandler, DoubleTappedRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.DoubleTappedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.DoubleTappedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// DoubleTappedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;DoubleTappedRoutedEventArgs&gt;。</returns>
        public static IObservable<DoubleTappedRoutedEventArgs> DoubleTappedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.DoubleTappedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// DragEnterイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, DragEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, DragEventArgs>> DragEnterEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<DragEventHandler, DragEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.DragEnterEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.DragEnterEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// DragEnterイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;DragEventArgs&gt;。</returns>
        public static IObservable<DragEventArgs> DragEnterAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.DragEnterEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// DragLeaveイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, DragEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, DragEventArgs>> DragLeaveEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<DragEventHandler, DragEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.DragLeaveEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.DragLeaveEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// DragLeaveイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;DragEventArgs&gt;。</returns>
        public static IObservable<DragEventArgs> DragLeaveAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.DragLeaveEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// DragOverイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, DragEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, DragEventArgs>> DragOverEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<DragEventHandler, DragEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.DragOverEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.DragOverEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// DragOverイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;DragEventArgs&gt;。</returns>
        public static IObservable<DragEventArgs> DragOverAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.DragOverEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// DropイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, DragEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, DragEventArgs>> DropEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<DragEventHandler, DragEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.DropEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.DropEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// DropイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;DragEventArgs&gt;。</returns>
        public static IObservable<DragEventArgs> DropAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.DropEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// GotFocusイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, RoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, RoutedEventArgs>> GotFocusEventPatternAsObservable(this UIElement element)
        {
            return Observable.FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                    h => h.Invoke
                    , h => element.GotFocus += h
                    , h => element.GotFocus -= h)
                .Select(x => x);
        }

        /// <summary>
        /// GotFocusイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <returns>IObservable&lt;RoutedEventArgs&gt;。</returns>
        public static IObservable<RoutedEventArgs> GotFocusAsObservable(this UIElement element)
        {
            return element.GotFocusEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// HoldingイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, HoldingRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, HoldingRoutedEventArgs>> HoldingEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<HoldingEventHandler, HoldingRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.HoldingEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.HoldingEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// HoldingイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;HoldingRoutedEventArgs&gt;。</returns>
        public static IObservable<HoldingRoutedEventArgs> HoldingAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.HoldingEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// KeyDownイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, KeyRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, KeyRoutedEventArgs>> KeyDownEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<KeyEventHandler, KeyRoutedEventArgs>(
                h => h.Invoke
                , h => element.AddHandler(UIElement.KeyDownEvent, h, handledEventsToo)
                , h => element.RemoveHandler(UIElement.KeyDownEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// KeyDownイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;KeyRoutedEventArgs&gt;。</returns>
        public static IObservable<KeyRoutedEventArgs> KeyDownAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.KeyDownEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// KeyUpイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, KeyRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, KeyRoutedEventArgs>> KeyUpEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<KeyEventHandler, KeyRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.KeyUpEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.KeyUpEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// KeyUpイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;KeyRoutedEventArgs&gt;。</returns>
        public static IObservable<KeyRoutedEventArgs> KeyUpAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.KeyUpEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// LostFocusイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, RoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, RoutedEventArgs>> LostFocusEventPatternAsObservable(this UIElement element)
        {
            return Observable.FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                    h => h.Invoke
                    , h => element.LostFocus += h
                    , h => element.LostFocus -= h)
                .Select(x => x);
        }

        /// <summary>
        /// LostFocusイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <returns>IObservable&lt;RoutedEventArgs&gt;。</returns>
        public static IObservable<RoutedEventArgs> LostFocusAsObservable(this UIElement element)
        {
            return element.LostFocusEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// ManipulationCompletedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, ManipulationCompletedRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, ManipulationCompletedRoutedEventArgs>> ManipulationCompletedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<ManipulationCompletedEventHandler, ManipulationCompletedRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.ManipulationCompletedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.ManipulationCompletedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// ManipulationCompletedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;ManipulationCompletedRoutedEventArgs&gt;。</returns>
        public static IObservable<ManipulationCompletedRoutedEventArgs> ManipulationCompletedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.ManipulationCompletedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// ManipulationDeltaイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, ManipulationDeltaRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, ManipulationDeltaRoutedEventArgs>> ManipulationDeltaEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<ManipulationDeltaEventHandler, ManipulationDeltaRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.ManipulationDeltaEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.ManipulationDeltaEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// ManipulationDeltaイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;ManipulationDeltaRoutedEventArgs&gt;。</returns>
        public static IObservable<ManipulationDeltaRoutedEventArgs> ManipulationDeltaAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.ManipulationDeltaEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// ManipulationInertiaStartingイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, ManipulationInertiaStartingRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, ManipulationInertiaStartingRoutedEventArgs>> ManipulationInertiaStartingEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<ManipulationInertiaStartingEventHandler, ManipulationInertiaStartingRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.ManipulationInertiaStartingEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.ManipulationInertiaStartingEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// ManipulationInertiaStartingイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;ManipulationInertiaStartingRoutedEventArgs&gt;。</returns>
        public static IObservable<ManipulationInertiaStartingRoutedEventArgs> ManipulationInertiaStartingAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.ManipulationInertiaStartingEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// ManipulationStartedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, ManipulationStartedRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, ManipulationStartedRoutedEventArgs>> ManipulationStartedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<ManipulationStartedEventHandler, ManipulationStartedRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.ManipulationStartedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.ManipulationStartedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// ManipulationStartedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;ManipulationStartedRoutedEventArgs&gt;。</returns>
        public static IObservable<ManipulationStartedRoutedEventArgs> ManipulationStartedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.ManipulationStartedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// ManipulationStartingイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, ManipulationStartingRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, ManipulationStartingRoutedEventArgs>> ManipulationStartingEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<ManipulationStartingEventHandler, ManipulationStartingRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.ManipulationStartingEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.ManipulationStartingEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// ManipulationStartingイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;ManipulationStartingRoutedEventArgs&gt;。</returns>
        public static IObservable<ManipulationStartingRoutedEventArgs> ManipulationStartingAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.ManipulationStartingEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerCanceledイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerCanceledEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerCanceledEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerCanceledEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerCanceledイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerCanceledAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerCanceledEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerCaptureLostイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerCaptureLostEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerCaptureLostEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerCaptureLostEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerCaptureLostイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerCaptureLostAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerCaptureLostEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerEnteredイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerEnteredEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerEnteredEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerEnteredEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerEnteredイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerEnteredAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerEnteredEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerExitedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerExitedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerExitedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerExitedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerExitedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerExitedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerExitedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerMovedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerMovedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerMovedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerMovedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerMovedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerMovedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerMovedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerPressedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerPressedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerPressedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerPressedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerPressedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerPressedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerPressedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerReleasedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerReleasedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerReleasedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerReleasedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerReleasedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerReleasedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerReleasedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// PointerWheelChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, PointerRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, PointerRoutedEventArgs>> PointerWheelChangedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<PointerEventHandler, PointerRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.PointerWheelChangedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.PointerWheelChangedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// PointerWheelChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;PointerRoutedEventArgs&gt;。</returns>
        public static IObservable<PointerRoutedEventArgs> PointerWheelChangedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.PointerWheelChangedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// RightTappedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, RightTappedRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, RightTappedRoutedEventArgs>> RightTappedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<RightTappedEventHandler, RightTappedRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.RightTappedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.RightTappedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// RightTappedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;RightTappedRoutedEventArgs&gt;。</returns>
        public static IObservable<RightTappedRoutedEventArgs> RightTappedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.RightTappedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }

        /// <summary>
        /// TappedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, TappedRoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, TappedRoutedEventArgs>> TappedEventPatternAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return Observable.FromEventPattern<TappedEventHandler, TappedRoutedEventArgs>(
                    h => h.Invoke
                    , h => element.AddHandler(UIElement.TappedEvent, h, handledEventsToo)
                    , h => element.RemoveHandler(UIElement.TappedEvent, h))
                .Select(x => x);
        }

        /// <summary>
        /// TappedイベントをObservableに変換します。
        /// </summary>
        /// <param name="element">UIElement。</param>
        /// <param name="handledEventsToo">処理済みのイベントでも起動するようにするか。</param>
        /// <returns>IObservable&lt;TappedRoutedEventArgs&gt;。</returns>
        public static IObservable<TappedRoutedEventArgs> TappedAsObservable(this UIElement element, bool handledEventsToo = false)
        {
            return element.TappedEventPatternAsObservable(handledEventsToo).Select(x => x.EventArgs);
        }
    }
}
