﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Katoributa.Net.Platform.Reactive.Extensions
{
    /// <summary>
    /// PasswordBoxに対して追加する便利機能群です。
    /// </summary>
    public static class PasswordBoxExtension
    {
        /// <summary>
        /// ContextMenuOpeningイベントをObservableに変換します。
        /// </summary>
        /// <param name="passwordBox">PasswordBox。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, ContextMenuEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, ContextMenuEventArgs>> ContextMenuOpeningEventPatternAsObservable(this PasswordBox passwordBox)
        {
            return Observable.FromEventPattern<ContextMenuOpeningEventHandler, ContextMenuEventArgs>(
                    h => h.Invoke
                    , h => passwordBox.ContextMenuOpening += h
                    , h => passwordBox.ContextMenuOpening -= h)
                .Select(x => x);
        }

        /// <summary>
        /// ContextMenuOpeningイベントをObservableに変換します。
        /// </summary>
        /// <param name="passwordBox">PasswordBox。</param>
        /// <returns>IObservable&lt;ContextMenuEventArgs&gt;。</returns>
        public static IObservable<ContextMenuEventArgs> ContextMenuOpeningAsObservable(this PasswordBox passwordBox)
        {
            return passwordBox.ContextMenuOpeningEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// PasswordChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="passwordBox">PasswordBox。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, RoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, RoutedEventArgs>> PasswordChangedEventPatternAsObservable(this PasswordBox passwordBox)
        {
            return Observable.FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                    h => h.Invoke
                    , h => passwordBox.PasswordChanged += h
                    , h => passwordBox.PasswordChanged -= h)
                .Select(x => x);
        }

        /// <summary>
        /// PasswordChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="passwordBox">PasswordBox。</param>
        /// <returns>IObservable&lt;RoutedEventArgs&gt;。</returns>
        public static IObservable<RoutedEventArgs> PasswordChangedAsObservable(this PasswordBox passwordBox)
        {
            return passwordBox.PasswordChangedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// PasswordChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="passwordBox">PasswordBox。</param>
        /// <returns>IObservable&lt;string&gt;。</returns>
        public static IObservable<string> PasswordChangedTrimTextAsObservable(this PasswordBox passwordBox)
        {
            return passwordBox.PasswordChangedEventPatternAsObservable().Select(x => (x.Sender as PasswordBox)?.Password?.Trim());
        }

        /// <summary>
        /// PasteイベントをObservableに変換します。
        /// </summary>
        /// <param name="passwordBox">PasswordBox。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, TextControlPasteEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, TextControlPasteEventArgs>> PasteEventPatternAsObservable(this PasswordBox passwordBox)
        {
            return Observable.FromEventPattern<TextControlPasteEventHandler, TextControlPasteEventArgs>(
                    h => h.Invoke
                    , h => passwordBox.Paste += h
                    , h => passwordBox.Paste -= h)
                .Select(x => x);
        }

        /// <summary>
        /// PasteイベントをObservableに変換します。
        /// </summary>
        /// <param name="passwordBox">PasswordBox。</param>
        /// <returns>IObservable&lt;TextControlPasteEventArgs&gt;。</returns>
        public static IObservable<TextControlPasteEventArgs> PasteAsObservable(this PasswordBox passwordBox)
        {
            return passwordBox.PasteEventPatternAsObservable().Select(x => x.EventArgs);
        }
    }
}
