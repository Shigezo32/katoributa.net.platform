﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;

namespace Katoributa.Net.Platform.Reactive.Extensions
{
    /// <summary>
    /// FrameworkElementに対して追加する便利機能群です。
    /// </summary>
    public static class FrameworkElementExtension
    {
        /// <summary>
        /// <see cref="FrameworkElement.DataContextChanged"/>イベントを<see cref="IObservable{T}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{T}"/>。</returns>
        public static IObservable<EventPattern<object, DataContextChangedEventArgs>> DataContextChangedEventPatternAsObservable(this FrameworkElement element)
        {
            return Observable.FromEventPattern<TypedEventHandler<FrameworkElement, DataContextChangedEventArgs>, DataContextChangedEventArgs>(
                    h => h.Invoke
                    , h => element.DataContextChanged += h
                    , h => element.DataContextChanged -= h)
                .Select(x => x);
        }

        /// <summary>
        /// <see cref="FrameworkElement.DataContextChanged"/>イベントを<see cref="IObservable{DataContextChangedEventArgs}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{DataContextChangedEventArgs}"/>。</returns>
        public static IObservable<DataContextChangedEventArgs> DataContextChangedAsObservable(this FrameworkElement element)
        {
            return element.DataContextChangedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// <see cref="FrameworkElement.LayoutUpdated"/>イベントを<see cref="IObservable{T}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{T}"/>。</returns>
        public static IObservable<EventPattern<object, object>> LayoutUpdatedEventPatternAsObservable(this FrameworkElement element)
        {
            return Observable.FromEventPattern<EventHandler<object>, object>(
                    h => h.Invoke
                    , h => element.LayoutUpdated += h
                    , h => element.LayoutUpdated -= h)
                .Select(x => x);
        }

        /// <summary>
        /// <see cref="FrameworkElement.LayoutUpdated"/>イベントを<see cref="IObservable{LayoutUpdatedEventArgs}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{LayoutUpdatedEventArgs}"/>。</returns>
        public static IObservable<object> LayoutUpdatedAsObservable(this FrameworkElement element)
        {
            return element.LayoutUpdatedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// <see cref="FrameworkElement.Loaded"/>イベントを<see cref="IObservable{T}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{T}"/>。</returns>
        public static IObservable<EventPattern<object, RoutedEventArgs>> LoadedEventPatternAsObservable(this FrameworkElement element)
        {
            return Observable.FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                    h => h.Invoke
                    , h => element.Loaded += h
                    , h => element.Loaded -= h)
                .Select(x => x);
        }

        /// <summary>
        /// <see cref="FrameworkElement.Loaded"/>イベントを<see cref="IObservable{LoadedEventArgs}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{LoadedEventArgs}"/>。</returns>
        public static IObservable<RoutedEventArgs> LoadedAsObservable(this FrameworkElement element)
        {
            return element.LoadedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// <see cref="FrameworkElement.SizeChanged"/>イベントを<see cref="IObservable{T}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{T}"/>。</returns>
        public static IObservable<EventPattern<object, SizeChangedEventArgs>> SizeChangedEventPatternAsObservable(this FrameworkElement element)
        {
            return Observable.FromEventPattern<SizeChangedEventHandler, SizeChangedEventArgs>(
                    h => h.Invoke
                    , h => element.SizeChanged += h
                    , h => element.SizeChanged -= h)
                .Select(x => x);
        }

        /// <summary>
        /// <see cref="FrameworkElement.SizeChanged"/>イベントを<see cref="IObservable{SizeChangedEventArgs}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{SizeChangedEventArgs}"/>。</returns>
        public static IObservable<SizeChangedEventArgs> SizeChangedAsObservable(this FrameworkElement element)
        {
            return element.SizeChangedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// <see cref="FrameworkElement.Unloaded"/>イベントを<see cref="IObservable{T}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{T}"/>。</returns>
        public static IObservable<EventPattern<object, RoutedEventArgs>> UnloadedEventPatternAsObservable(this FrameworkElement element)
        {
            return Observable.FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                    h => h.Invoke
                    , h => element.Unloaded += h
                    , h => element.Unloaded -= h)
                .Select(x => x);
        }

        /// <summary>
        /// <see cref="FrameworkElement.Unloaded"/>イベントを<see cref="IObservable{UnloadedEventArgs}"/>に変換します。
        /// </summary>
        /// <param name="element"><see cref="FrameworkElement"/>。</param>
        /// <returns><see cref="IObservable{UnloadedEventArgs}"/>。</returns>
        public static IObservable<RoutedEventArgs> UnloadedAsObservable(this FrameworkElement element)
        {
            return element.UnloadedEventPatternAsObservable().Select(x => x.EventArgs);
        }
    }
}
