﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Katoributa.Net.Platform.Reactive.Extensions
{
    /// <summary>
    /// TextBoxに対して追加する便利機能群です。
    /// </summary>
    public static class TextBoxExtension
    {
        /// <summary>
        /// ContextMenuOpeningイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, ContextMenuEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, ContextMenuEventArgs>> ContextMenuOpeningEventPatternAsObservable(this TextBox textBox)
        {
            return Observable.FromEventPattern<ContextMenuOpeningEventHandler, ContextMenuEventArgs>(
                    h => h.Invoke
                    , h => textBox.ContextMenuOpening += h
                    , h => textBox.ContextMenuOpening -= h)
                .Select(x => x);
        }

        /// <summary>
        /// ContextMenuOpeningイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;ContextMenuEventArgs&gt;。</returns>
        public static IObservable<ContextMenuEventArgs> ContextMenuOpeningAsObservable(this TextBox textBox)
        {
            return textBox.ContextMenuOpeningEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// PasteイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, TextControlPasteEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, TextControlPasteEventArgs>> PasteEventPatternAsObservable(this TextBox textBox)
        {
            return Observable.FromEventPattern<TextControlPasteEventHandler, TextControlPasteEventArgs>(
                    h => h.Invoke
                    , h => textBox.Paste += h
                    , h => textBox.Paste -= h)
                .Select(x => x);
        }

        /// <summary>
        /// PasteイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;TextControlPasteEventArgs&gt;。</returns>
        public static IObservable<TextControlPasteEventArgs> PasteAsObservable(this TextBox textBox)
        {
            return textBox.PasteEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// SelectionChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, RoutedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, RoutedEventArgs>> SelectionChangedEventPatternAsObservable(this TextBox textBox)
        {
            return Observable.FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                    h => h.Invoke
                    , h => textBox.SelectionChanged += h
                    , h => textBox.SelectionChanged -= h)
                .Select(x => x);
        }

        /// <summary>
        /// SelectionChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;RoutedEventArgs&gt;。</returns>
        public static IObservable<RoutedEventArgs> SelectionChangedAsObservable(this TextBox textBox)
        {
            return textBox.SelectionChangedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// TextChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;EventPattern&lt;object, TextChangedEventArgs&gt;&gt;。</returns>
        public static IObservable<EventPattern<object, TextChangedEventArgs>> TextChangedEventPatternAsObservable(this TextBox textBox)
        {
            return Observable.FromEventPattern<TextChangedEventHandler, TextChangedEventArgs>(
                    h => h.Invoke
                    , h => textBox.TextChanged += h
                    , h => textBox.TextChanged -= h)
                .Select(x => x);
        }

        /// <summary>
        /// TextChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;TextChangedEventArgs&gt;。</returns>
        public static IObservable<TextChangedEventArgs> TextChangedAsObservable(this TextBox textBox)
        {
            return textBox.TextChangedEventPatternAsObservable().Select(x => x.EventArgs);
        }

        /// <summary>
        /// TextChangedイベントをObservableに変換します。
        /// </summary>
        /// <param name="textBox">TextBox。</param>
        /// <returns>IObservable&lt;string&gt;。</returns>
        public static IObservable<string> TextChangedTrimTextAsObservable(this TextBox textBox)
        {
            return textBox.TextChangedEventPatternAsObservable().Select(x => (x.Sender as TextBox)?.Text?.Trim());
        }
    }
}
