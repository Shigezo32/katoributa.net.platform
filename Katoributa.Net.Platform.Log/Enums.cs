﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ログレベル。
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// 致命的なエラー。
        /// </summary>
        Fatal = 0,

        /// <summary>
        /// エラー。
        /// </summary>
        Error = 1,

        /// <summary>
        /// 警告。
        /// </summary>
        Warn = 2,

        /// <summary>
        /// 操作ログ情報。
        /// </summary>
        Info = 3,

        /// <summary>
        /// 開発用デバッグ情報。
        /// </summary>
        Debug = 4,

        /// <summary>
        /// トレースログ情報。
        /// </summary>
        Trace = 5
    }

    /// <summary>
    /// 変換対象日付タイプ。
    /// </summary>
    public enum ConvertDateFormat
    {
        /// <summary>
        /// 年 (4 桁の数値)。
        /// </summary>
        yyyy,

        /// <summary>
        /// 年 (3 桁以上)。
        /// </summary>
        yyy,

        /// <summary>
        /// 年 (00 ～ 99)。
        /// </summary>
        yy,

        /// <summary>
        /// 年 (0 ～ 99)。
        /// </summary>
        y,

        /// <summary>
        /// 月 (01 ～ 12)。
        /// </summary>
        MM,

        /// <summary>
        /// 月 (1 ～ 12)。
        /// </summary>
        M,

        /// <summary>
        /// 曜日の省略名。
        /// </summary>
        ddd,

        /// <summary>
        /// 月の日にち (01 ～ 31)。
        /// </summary>
        dd,

        /// <summary>
        /// 月の日にち (1 ～ 31)。
        /// </summary>
        d,

        /// <summary>
        /// 24 時間形式の時間 (00 ～ 23)。
        /// </summary>
        HH,

        /// <summary>
        /// 24 時間形式の時間 (0 ～ 23)。
        /// </summary>
        H,

        /// <summary>
        /// 12 時間形式の時間 (01 ～ 12)。
        /// </summary>
        hh,

        /// <summary>
        /// 12 時間形式の時間 (1 ～ 12)。
        /// </summary>
        h,

        /// <summary>
        /// 分 (00 ～ 59)。
        /// </summary>
        mm,

        /// <summary>
        /// 分 (0 ～ 59)。
        /// </summary>
        m,

        /// <summary>
        /// 秒 (00 ～ 59)。
        /// </summary>
        ss,

        /// <summary>
        /// 秒 (0 ～ 59)。
        /// </summary>
        s
    }
}