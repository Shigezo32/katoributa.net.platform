﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log.Extensions
{
    /// <summary>
    /// ログに関係する便利機能群。
    /// </summary>
    public class LogUtility : IDisposable
    {
        /// <summary>
        /// Logger。
        /// </summary>
        public static ILogger Logger { get; set; }

        #region Trace

        /// <summary>
        /// このインスタンスがDispose済みかどうかを表すフラグ。
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// パラメータの一覧の文字列。
        /// </summary>
        private readonly string _parametersText;

        /// <summary>
        /// 呼び出し元のソースファイルの完全パス。
        /// </summary>
        private readonly string _file;

        /// <summary>
        /// 呼び出し元のソースファイルの行番号。
        /// </summary>
        private readonly int _line;

        /// <summary>
        /// 呼び出し元のメソッド名。
        /// </summary>
        private readonly string _member;

        /// <summary>
        /// LogUtilityのインスタンスを初期化します。
        /// </summary>
        /// <param name="parametersText">パラメータの一覧の文字列。</param>
        /// <param name="file">呼び出し元のソースファイルの完全パス。</param>
        /// <param name="line">呼び出し元のソースファイルの行番号。</param>
        /// <param name="member">呼び出し元のメソッド名。</param>
        private LogUtility(string parametersText = null, string file = "", int line = 0, string member = "")
        {
            _parametersText = parametersText;
            _file = file;
            _line = line;
            _member = member;
        }

        /// <summary>
        /// トレースを開始します。
        /// </summary>
        /// <param name="parameters">パラメータの一覧。</param>
        /// <param name="file">呼び出し元のソースファイルの完全パス。</param>
        /// <param name="line">呼び出し元のソースファイルの行番号。</param>
        /// <param name="member">呼び出し元のメソッド名。</param>
        /// <returns>トレースを終了するためのIDisposable。</returns>
        public static IDisposable StartTrace(IEnumerable<KeyValuePair<string, object>> parameters = null, [CallerFilePath] string file = "", [CallerLineNumber] int line = 0, [CallerMemberName] string member = "")
        {
            var parametersText = CreateParametersText(parameters);
            if (LogUtility.Logger != null)
            {
                var index = file.LastIndexOf('\\');
                var fileName = file.Substring(index + 1, file.Length - index - 1);
                LogUtility.Logger.Trace($"Start:{member}({fileName},{line}){parametersText}");
            }
            return new LogUtility(parametersText, file, line, member);
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (LogUtility.Logger != null)
            {
                var index = _file.LastIndexOf('\\');
                var fileName = _file.Substring(index + 1, _file.Length - index - 1);
                LogUtility.Logger.Trace($"End  :{_member}({fileName},{_line}){_parametersText}");
            }
            _disposed = true;
            if (disposing)
            {
            }
        }

        #endregion

        #region Warn

        /// <summary>
        /// 警告ログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="parameters">パラメータの一覧。</param>
        /// <param name="file">呼び出し元のソースファイルの完全パス。</param>
        /// <param name="line">呼び出し元のソースファイルの行番号。</param>
        /// <param name="member">呼び出し元のメソッド名。</param>
        public static void Warn(object message, IEnumerable<KeyValuePair<string, object>> parameters = null, [CallerFilePath] string file = "", [CallerLineNumber] int line = 0, [CallerMemberName] string member = "")
        {
            if (LogUtility.Logger == null) { return; }
            var index = file.LastIndexOf('\\');
            var fileName = file.Substring(index + 1, file.Length - index - 1);
            LogUtility.Logger.Warn($"{member}({fileName},{line}){CreateParametersText(parameters)}{message}");
        }

        /// <summary>
        /// 例外発生時の警告ログを出力します。
        /// </summary>
        /// <param name="exception">例外。</param>
        /// <param name="parameters">パラメータの一覧。</param>
        /// <param name="file">呼び出し元のソースファイルの完全パス。</param>
        /// <param name="line">呼び出し元のソースファイルの行番号。</param>
        /// <param name="member">呼び出し元のメソッド名。</param>
        public static void Warn(Exception exception, IEnumerable<KeyValuePair<string, object>> parameters = null, [CallerFilePath] string file = "", [CallerLineNumber] int line = 0, [CallerMemberName] string member = "")
        {
            if (LogUtility.Logger == null) { return; }
            var index = file.LastIndexOf('\\');
            var fileName = file.Substring(index + 1, file.Length - index - 1);
            LogUtility.Logger.Warn($"{member}({fileName},{line}){CreateParametersText(parameters)}", exception);
        }

        #endregion

        #region Error

        /// <summary>
        /// エラーログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="parameters">パラメータの一覧。</param>
        /// <param name="file">呼び出し元のソースファイルの完全パス。</param>
        /// <param name="line">呼び出し元のソースファイルの行番号。</param>
        /// <param name="member">呼び出し元のメソッド名。</param>
        public static void Error(object message, IEnumerable<KeyValuePair<string, object>> parameters = null, [CallerFilePath] string file = "", [CallerLineNumber] int line = 0, [CallerMemberName] string member = "")
        {
            if (LogUtility.Logger == null) { return; }
            var index = file.LastIndexOf('\\');
            var fileName = file.Substring(index + 1, file.Length - index - 1);
            LogUtility.Logger.Error($"{member}({fileName},{line}){CreateParametersText(parameters)}{message}");
        }

        /// <summary>
        /// 例外発生時のエラーログを出力します。
        /// </summary>
        /// <param name="exception">例外。</param>
        /// <param name="parameters">パラメータの一覧。</param>
        /// <param name="file">呼び出し元のソースファイルの完全パス。</param>
        /// <param name="line">呼び出し元のソースファイルの行番号。</param>
        /// <param name="member">呼び出し元のメソッド名。</param>
        public static void Error(Exception exception, IEnumerable<KeyValuePair<string, object>> parameters = null, [CallerFilePath] string file = "", [CallerLineNumber] int line = 0, [CallerMemberName] string member = "")
        {
            if (LogUtility.Logger == null) { return; }
            var index = file.LastIndexOf('\\');
            var fileName = file.Substring(index + 1, file.Length - index - 1);
            LogUtility.Logger.Error($"{member}({fileName},{line}){CreateParametersText(parameters)}", exception);
        }

        #endregion

        /// <summary>
        /// パラメータの一覧からログ出力用の文字列を作成します。
        /// </summary>
        /// <param name="parameters">パラメータの一覧。</param>
        /// <returns>ログ出力用の文字列。</returns>
        private static string CreateParametersText(IEnumerable<KeyValuePair<string, object>> parameters)
        {
            if (parameters == null)
            {
                return null;
            }
            return string.Format("[{0}]", string.Join(",", parameters.Select(x =>
                {
                    var result = "";
                    var num = 0.0;
                    if (x.Value == null)
                    {
                        result = "null";
                    }
                    else if (x.Value is string
                        || x.Value is DateTime
                        || x.Value is Guid
                        || x.Value is Uri
                        || double.TryParse(x.Value.ToString(), out num)
                        || (x.Value is ValueType && x.Value is IComparable && x.Value is IFormattable))
                    {
                        result = x.Value.ToString();
                    }
                    else if (x.Value is IEnumerable<KeyValuePair<string, string>>)
                    {
                        var keyValuePairs = x.Value as IEnumerable<KeyValuePair<string, string>>;
                        var sb = new StringBuilder();
                        sb.Append("[");
                        sb.Append(string.Join(",", keyValuePairs.Select(_ => $"{_.Key}:{_.Value}")));
                        sb.Append("]");
                        result = sb.ToString();
                    }
                    else
                    {
                        TrySerialize(x.Value, out result);
                    }
                    return $"{x.Key}:{result}";
                })));
        }

        /// <summary>
        /// 対象のオブジェクトをXML文字列にシリアライズします。
        /// </summary>
        /// <typeparam name="T">対象の型。</typeparam>
        /// <param name="target">シリアライズするオブジェクト。</param>
        /// <returns>シリアライズされた文字列。</returns>
        private static string Serialize<T>(T target)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }
            using (var ms = new MemoryStream())
            {
                var serializer = new DataContractSerializer(target.GetType());
                serializer.WriteObject(ms, target);
                var buffer = ms.ToArray();
                return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            }
        }

        /// <summary>
        /// 対象のオブジェクトをXML文字列にシリアライズします。戻り値は、シリアライズが成功したかどうかを示します。
        /// </summary>
        /// <typeparam name="T">対象の型。</typeparam>
        /// <param name="target">シリアライズするオブジェクト。</param>
        /// <param name="result">シリアライズされた文字列。</param>
        /// <returns>シリアライズが成功した場合はtrue、失敗した場合はfalse。</returns>
        private static bool TrySerialize<T>(T target, out string result)
        {
            try
            {
                result = Serialize(target);
                return true;
            }
            catch (ArgumentNullException) { }
            catch (SerializationException) { }
            catch { }
            result = null;
            return false;
        }
    }
}
