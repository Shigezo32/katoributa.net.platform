﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log.Extensions
{
    /// <summary>
    /// Taskに対して追加する便利機能群です。
    /// </summary>
    public static class TaskExtension
    {
        /// <summary>
        /// このトークンに対して取り消しが要求された場合、<see cref="OperationCanceledException"/> をスローします。
        /// </summary>
        /// <param name="token">CancellationToken。</param>
        /// <param name="parameters">パラメータの一覧。</param>
        /// <param name="file">呼び出し元のソースファイルの完全パス。</param>
        /// <param name="line">呼び出し元のソースファイルの行番号。</param>
        /// <param name="member">呼び出し元のメソッド名。</param>
        /// <exception cref="OperationCanceledException">トークンに対して取り消しが要求されました。</exception>
        /// <exception cref="ObjectDisposedException">関連する <see cref="CancellationTokenSource"/> は破棄されました。</exception>
        public static void ThrowIfCancellationRequested(this CancellationToken token, IEnumerable<KeyValuePair<string, object>> parameters = null, [CallerFilePath] string file = "", [CallerLineNumber] int line = 0, [CallerMemberName] string member = "")
        {
            try
            {
                token.ThrowIfCancellationRequested();
            }
            catch (Exception ex)
            {
                LogUtility.Warn(ex, parameters, file, line, member);
                throw;
            }
        }
    }
}
