﻿using System;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ログ出力の基底クラス。
    /// </summary>
    public abstract class LoggerBase : ILogger, IAsyncLogger
    {
        /// <summary>
        /// このインスタンスがDispose済みかどうか。
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// このインスタンスがDispose済みかどうか。
        /// </summary>
        protected bool Disposed { get { return _disposed; } }

        /// <summary>
        /// LoggerBaseのインスタンスを初期化します。
        /// </summary>
        public LoggerBase()
        {
#if DEBUG
            this.LogLevel = LogLevel.Trace;
#else
            this.LogLevel = LogLevel.Info;
#endif
            this.SystemDateTimeFormat = "yyyy/MM/dd HH:mm:ss";
            this.ThrowIfExceptionOccured = false;
        }

        /// <summary>
        /// このインスタンスがGCに回収される時に呼び出されます。
        /// </summary>
        ~LoggerBase()
        {
            this.Dispose(false);
        }

        #region プロパティ

        /// <summary>
        /// 出力するログのレベル。
        /// </summary>
        public LogLevel LogLevel { get; set; }

        /// <summary>
        /// ログ出力用日付フォーマット。
        /// </summary>
        public string SystemDateTimeFormat { get; set; }

        /// <summary>
        /// 例外が発生したときに投げるかどうか。
        /// </summary>
        public bool ThrowIfExceptionOccured { get; set; }

        #endregion

        #region Trace

        /// <summary>
        /// メッセージを指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        public void Trace(object message)
        {
            this.OnTrace(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        public void Trace(object message, Exception exception)
        {
            this.OnTrace(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのTraceログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        public void TraceFormat(string format, params object[] args)
        {
            this.OnTrace(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected virtual void OnTrace(object message, Exception exception)
        {
            this.OnLog(LogLevel.Trace, message, exception);
        }

        /// <summary>
        /// メッセージを指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        public Task TraceAsync(object message)
        {
            return this.OnTraceAsync(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        public Task TraceAsync(object message, Exception exception)
        {
            return this.OnTraceAsync(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのTraceログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        public Task TraceFormatAsync(string format, params object[] args)
        {
            return this.OnTraceAsync(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected virtual Task OnTraceAsync(object message, Exception exception)
        {
            return this.OnLogAsync(LogLevel.Trace, message, exception);
        }

        #endregion

        #region Debug

        /// <summary>
        /// メッセージを指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        public void Debug(object message)
        {
            this.OnDebug(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        public void Debug(object message, Exception exception)
        {
            this.OnDebug(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのDebugログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        public void DebugFormat(string format, params object[] args)
        {
            this.OnDebug(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected virtual void OnDebug(object message, Exception exception)
        {
            this.OnLog(LogLevel.Debug, message, exception);
        }

        /// <summary>
        /// メッセージを指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        public Task DebugAsync(object message)
        {
            return this.OnDebugAsync(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        public Task DebugAsync(object message, Exception exception)
        {
            return this.OnDebugAsync(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのDebugログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        public Task DebugFormatAsync(string format, params object[] args)
        {
            return this.OnDebugAsync(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected virtual Task OnDebugAsync(object message, Exception exception)
        {
            return this.OnLogAsync(LogLevel.Debug, message, exception);
        }

        #endregion

        #region Info

        /// <summary>
        /// メッセージを指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        public void Info(object message)
        {
            this.OnInfo(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        public void Info(object message, Exception exception)
        {
            this.OnInfo(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのInfoログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        public void InfoFormat(string format, params object[] args)
        {
            this.OnInfo(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected virtual void OnInfo(object message, Exception exception)
        {
            this.OnLog(LogLevel.Info, message, exception);
        }

        /// <summary>
        /// メッセージを指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        public Task InfoAsync(object message)
        {
            return this.OnInfoAsync(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        public Task InfoAsync(object message, Exception exception)
        {
            return this.OnInfoAsync(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのInfoログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        public Task InfoFormatAsync(string format, params object[] args)
        {
            return this.OnInfoAsync(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected virtual Task OnInfoAsync(object message, Exception exception)
        {
            return this.OnLogAsync(LogLevel.Info, message, exception);
        }

        #endregion

        #region Warn

        /// <summary>
        /// メッセージを指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        public void Warn(object message)
        {
            this.OnWarn(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        public void Warn(object message, Exception exception)
        {
            this.OnWarn(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのWarnログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        public void WarnFormat(string format, params object[] args)
        {
            this.OnWarn(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected virtual void OnWarn(object message, Exception exception)
        {
            this.OnLog(LogLevel.Warn, message, exception);
        }

        /// <summary>
        /// メッセージを指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        public Task WarnAsync(object message)
        {
            return this.OnWarnAsync(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        public Task WarnAsync(object message, Exception exception)
        {
            return this.OnWarnAsync(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのWarnログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        public Task WarnFormatAsync(string format, params object[] args)
        {
            return this.OnWarnAsync(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected virtual Task OnWarnAsync(object message, Exception exception)
        {
            return this.OnLogAsync(LogLevel.Warn, message, exception);
        }

        #endregion

        #region Error

        /// <summary>
        /// メッセージを指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        public void Error(object message)
        {
            this.OnError(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        public void Error(object message, Exception exception)
        {
            this.OnError(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのErrorログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        public void ErrorFormat(string format, params object[] args)
        {
            this.OnError(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected virtual void OnError(object message, Exception exception)
        {
            this.OnLog(LogLevel.Error, message, exception);
        }

        /// <summary>
        /// メッセージを指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        public Task ErrorAsync(object message)
        {
            return this.OnErrorAsync(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        public Task ErrorAsync(object message, Exception exception)
        {
            return this.OnErrorAsync(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのErrorログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        public Task ErrorFormatAsync(string format, params object[] args)
        {
            return this.OnErrorAsync(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected virtual Task OnErrorAsync(object message, Exception exception)
        {
            return this.OnLogAsync(LogLevel.Error, message, exception);
        }

        #endregion

        #region Fatal

        /// <summary>
        /// メッセージを指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        public void Fatal(object message)
        {
            this.OnFatal(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        public void Fatal(object message, Exception exception)
        {
            this.OnFatal(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのFatalログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        public void FatalFormat(string format, params object[] args)
        {
            this.OnFatal(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected virtual void OnFatal(object message, Exception exception)
        {
            this.OnLog(LogLevel.Fatal, message, exception);
        }

        /// <summary>
        /// メッセージを指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        public Task FatalAsync(object message)
        {
            return this.OnFatalAsync(message, null);
        }

        /// <summary>
        /// メッセージと例外を指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        public Task FatalAsync(object message, Exception exception)
        {
            return this.OnFatalAsync(message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのFatalログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        public Task FatalFormatAsync(string format, params object[] args)
        {
            return this.OnFatalAsync(string.Format(format, args), null);
        }

        /// <summary>
        /// メッセージと例外を指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected virtual Task OnFatalAsync(object message, Exception exception)
        {
            return this.OnLogAsync(LogLevel.Fatal, message, exception);
        }

        #endregion

        #region Log

        /// <summary>
        /// ログレベルとメッセージを指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        public void Log(LogLevel logLevel, object message)
        {
            this.OnLog(logLevel, message, null);
        }

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        public void Log(LogLevel logLevel, object message, Exception exception)
        {
            this.OnLog(logLevel, message, exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージを、ログレベルを指定して出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        public void LogFormat(LogLevel logLevel, string format, params object[] args)
        {
            this.OnLog(logLevel, string.Format(format, args), null);
        }

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected abstract void OnLog(LogLevel logLevel, object message, Exception exception);

        /// <summary>
        /// ログレベルとメッセージを指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        public Task LogAsync(LogLevel logLevel, object message)
        {
            return this.OnLogAsync(logLevel, message, null);
        }

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        public Task LogAsync(LogLevel logLevel, object message, Exception exception)
        {
            return this.OnLogAsync(logLevel, message.ToString(), exception);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージを、ログレベルを指定して出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        public Task LogFormatAsync(LogLevel logLevel, string format, params object[] args)
        {
            return this.OnLogAsync(logLevel, string.Format(format, args), null);
        }

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected abstract Task OnLogAsync(LogLevel logLevel, object message, Exception exception);

        #endregion

        /// <summary>
        /// 指定したログレベルがログ出力対象か判定します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <returns>ログを出力する場合はtrue、出力しない場合はfalse。</returns>
        /// <exception cref="ArgumentException">引数が無効な場合に発生します。</exception>
        protected virtual bool CanWriteLog(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Fatal:
                case LogLevel.Error:
                case LogLevel.Warn:
                case LogLevel.Info:
                case LogLevel.Debug:
                case LogLevel.Trace:
                    return this.LogLevel >= logLevel;
                default:
                    //引数不正
                    throw new ArgumentException(string.Format("LogLevelが不正な値です。LogLevel : {0}", logLevel), "LogLevel");
            }
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            _disposed = true;
            if (disposing)
            {
                // マネージリソースの解放処理
            }
            // アンマネージリソースの解放処理
        }

        /// <summary>
        /// このインスタンスで使用されていたすべてのリソースが解放済みの場合に例外を発生させます。
        /// </summary>
        /// <exception cref="ObjectDisposedException">リソースが解放済みの場合に発生。</exception>
        protected void ThrowExceptionIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().ToString());
            }
        }
    }
}