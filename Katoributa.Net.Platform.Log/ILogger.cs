﻿using System;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ログ出力処理のインターフェース。
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// 出力するログのレベル。
        /// </summary>
        LogLevel LogLevel { get; set; }

        /// <summary>
        /// メッセージを指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        void Trace(object message);

        /// <summary>
        /// メッセージと例外を指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        void Trace(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのTraceログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        void TraceFormat(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        void Debug(object message);

        /// <summary>
        /// メッセージと例外を指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        void Debug(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのDebugログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        void DebugFormat(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        void Info(object message);

        /// <summary>
        /// メッセージと例外を指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        void Info(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのInfoログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        void InfoFormat(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        void Warn(object message);

        /// <summary>
        /// メッセージと例外を指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        void Warn(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのWarnログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        void WarnFormat(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        void Error(object message);

        /// <summary>
        /// メッセージと例外を指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        void Error(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのErrorログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        void ErrorFormat(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        void Fatal(object message);

        /// <summary>
        /// メッセージと例外を指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        void Fatal(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのFatalログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        void FatalFormat(string format, params object[] args);

        /// <summary>
        /// ログレベルとメッセージを指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        void Log(LogLevel logLevel, object message);

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        void Log(LogLevel logLevel, object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージを、ログレベルを指定して出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        void LogFormat(LogLevel logLevel, string format, params object[] args);
    }
}