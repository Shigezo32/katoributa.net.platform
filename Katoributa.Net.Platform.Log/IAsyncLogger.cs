﻿using System;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// 非同期ログ出力処理のインターフェース。
    /// </summary>
    public interface IAsyncLogger
    {
        /// <summary>
        /// 出力するログのレベル。
        /// </summary>
        LogLevel LogLevel { get; set; }

        /// <summary>
        /// メッセージを指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        Task TraceAsync(object message);

        /// <summary>
        /// メッセージと例外を指定してTraceログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        Task TraceAsync(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのTraceログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        Task TraceFormatAsync(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        Task DebugAsync(object message);

        /// <summary>
        /// メッセージと例外を指定してDebugログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        Task DebugAsync(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのDebugログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        Task DebugFormatAsync(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        Task InfoAsync(object message);

        /// <summary>
        /// メッセージと例外を指定してInfoログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        Task InfoAsync(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのInfoログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        Task InfoFormatAsync(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        Task WarnAsync(object message);

        /// <summary>
        /// メッセージと例外を指定してWarnログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        Task WarnAsync(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのWarnログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        Task WarnFormatAsync(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        Task ErrorAsync(object message);

        /// <summary>
        /// メッセージと例外を指定してErrorログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        Task ErrorAsync(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのErrorログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        Task ErrorFormatAsync(string format, params object[] args);

        /// <summary>
        /// メッセージを指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        Task FatalAsync(object message);

        /// <summary>
        /// メッセージと例外を指定してFatalログを出力します。
        /// </summary>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        Task FatalAsync(object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージのFatalログを出力します。
        /// </summary>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        Task FatalFormatAsync(string format, params object[] args);

        /// <summary>
        /// ログレベルとメッセージを指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <returns>Task。</returns>
        Task LogAsync(LogLevel logLevel, object message);

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        Task LogAsync(LogLevel logLevel, object message, Exception exception);

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換したメッセージを、ログレベルを指定して出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="format">複合書式指定文字列。</param>
        /// <param name="args">0 個以上の書式設定対象オブジェクトを含んだオブジェクト配列。</param>
        /// <returns>Task。</returns>
        Task LogFormatAsync(LogLevel logLevel, string format, params object[] args);
    }
}