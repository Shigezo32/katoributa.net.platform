﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ログをコンソールに出力するクラス。
    /// </summary>
    public class DebugLogger : LoggerBase
    {
        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected override void OnLog(LogLevel logLevel, object message, Exception exception)
        {
            this.WriteLog(logLevel, message, exception);
        }

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected override Task OnLogAsync(LogLevel logLevel, object message, Exception exception)
        {
            return Task.Run(() => this.WriteLog(logLevel, message, exception));
        }

        /// <summary>
        /// ログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        private void WriteLog(LogLevel logLevel, object message, Exception exception)
        {
            if (CanWriteLog(logLevel))
            {
                var text = this.CreateOutputText(logLevel, message, exception);
                System.Diagnostics.Debug.WriteLine(text);
            }
        }

        /// <summary>
        /// 出力用のメッセージを作成します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>出力用のメッセージ。</returns>
        private string CreateOutputText(LogLevel logLevel, object message, Exception exception)
        {
            var dateTime = DateTime.Now.ToString(this.SystemDateTimeFormat);
            var logLevelText = logLevel.ToString().ToUpper();
            if (exception == null)
            {
                return string.Format("{0} {1} {2}", dateTime, logLevelText, message);
            }
            else
            {
                return string.Format("{0} {1} {2} {3}", dateTime, logLevelText, message, exception);
            }
        }
    }
}
