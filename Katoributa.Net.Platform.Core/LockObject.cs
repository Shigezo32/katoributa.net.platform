﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// ロックに使用するオブジェクト。
    /// </summary>
    public class LockObject : IDisposable
    {
        /// <summary>
        /// このインスタンスがDispose済みかどうかを表すフラグ。
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// LockObjectのインスタンスを初期化します。
        /// </summary>
        /// <param name="readerWriterLock">ロックを取得したReaderWriterLock。</param>
        public LockObject(ReaderWriterLock readerWriterLock)
        {
            this.Lock = readerWriterLock;
        }

        /// <summary>
        /// ロックを取得したReaderWriterLock。
        /// </summary>
        public ReaderWriterLock Lock { get; private set; }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            _disposed = true;
            if (disposing)
            {
                this.Lock.ExitLock(this);
            }
        }
    }
}
