﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Security
{
    /// <summary>
    /// パスワード資格情報のストア。
    /// </summary>
    public class PasswordCredential
    {
        /// <summary>
        /// PasswordCredentialのインスタンスを初期化します。
        /// </summary>
        public PasswordCredential()
        {
        }

        /// <summary>
        /// 提供された資格情報のデータを含むPasswordCredentialのインスタンスを初期化します。
        /// </summary>
        /// <param name="resource">資格情報が使用されるリソース。</param>
        /// <param name="userName">資格情報に含まれていなければならないユーザー名。</param>
        /// <param name="password">作成された資格情報のためのパスワード。</param>
        public PasswordCredential(string resource, string userName, string password)
        {
            this.Resource = resource;
            this.UserName = userName;
            this.Password = password;
        }

        /// <summary>
        /// 資格情報のリソース。
        /// </summary>
        public string Resource { get; set; }

        /// <summary>
        /// 資格情報のユーザー名。
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 資格情報のパスワード文字列。
        /// </summary>
        public string Password { get; set; }
    }
}
