﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core.Security
{
    /// <summary>
    /// セキュリティに関係する便利メソッド群です。
    /// </summary>
    public interface ISecurityUtility
    {
        /// <summary>
        /// ソルトを作るための秘密の文字列。
        /// </summary>
        string SaltKey { get; set; }

        /// <summary>
        /// パスワード用のSaltを取得します。
        /// </summary>
        /// <param name="id">ID。</param>
        /// <returns>パスワード用のSalt。</returns>
        string GetSalt(string id);

        /// <summary>
        /// 指定した文字列のSHA1を計算します。
        /// </summary>
        /// <param name="value">SHA1を計算する文字列。</param>
        /// <returns>SHA1。</returns>
        byte[] ToSha1Hash(string value);

        /// <summary>
        /// 指定したバイト配列のSHA256を計算します。
        /// </summary>
        /// <param name="array">SHA256を計算するバイト配列。</param>
        /// <returns>SHA256。</returns>
        byte[] ToSha256Hash(byte[] array);

        /// <summary>
        /// 指定した文字列のSHA256を計算します。
        /// </summary>
        /// <param name="value">SHA256を計算する文字列。</param>
        /// <returns>SHA256。</returns>
        byte[] ToSha256Hash(string value);

        /// <summary>
        /// 指定した文字列を指定した回数SHA256で計算したハッシュ値を取得します。
        /// </summary>
        /// <param name="value">SHA256を計算する文字列。</param>
        /// <param name="count">回数。</param>
        /// <returns>SHA256。</returns>
        byte[] GetHash(string value, int count);

        /// <summary>
        /// 指定されたパスワードとソルトをハッシュ化した文字列を返す
        /// </summary>
        /// <param name="pass">生パスワード</param>
        /// <param name="salt"></param>
        /// <returns>ハッシュ化されたパスワード</returns>
        string GetHashedPassword(string pass, string salt);
    }
}
