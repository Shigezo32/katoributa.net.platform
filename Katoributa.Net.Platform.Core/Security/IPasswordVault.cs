﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Security
{
    /// <summary>
    /// 資格情報用の資格情報保管ボックスを表します。
    /// </summary>
    public interface IPasswordVault
    {
        /// <summary>
        /// 資格情報保管ボックスに資格情報を追加します。
        /// </summary>
        /// <param name="credential">追加する資格情報。</param>
        void Add(PasswordCredential credential);

        /// <summary>
        /// 指定したリソースに一致する資格情報に対応する資格情報保管ボックスを検索します。
        /// </summary>
        /// <param name="resource">検索対象のリソース。</param>
        /// <returns>資格情報オブジェクトの一覧。</returns>
        IReadOnlyList<PasswordCredential> FindAllByResource(string resource);

        /// <summary>
        /// 資格情報保管ボックスから資格情報を削除します。
        /// </summary>
        /// <param name="credential">削除する資格情報。</param>
        void Remove(PasswordCredential credential);

        /// <summary>
        /// 資格情報保管ボックスから資格情報を読み取ります。
        /// </summary>
        /// <param name="resource">資格情報が使用されるリソース。</param>
        /// <param name="userName">資格情報に含まれていなければならないユーザー名。</param>
        /// <returns>すべてのデータを含む返された資格情報。</returns>
        PasswordCredential Retrieve(string resource, string userName);
    }
}
