﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// バインド可能なBufferBlock。
    /// </summary>
    /// <typeparam name="T">バッファリングされたデータの型。</typeparam>
    public class BindableBufferBlock<T> : BindableBase
    {
        /// <summary>
        /// データを格納するバッファ。
        /// </summary>
        private readonly BufferBlock<T> _bufferBlock = new BufferBlock<T>();

        /// <summary>
        /// 現在バッファに格納されているアイテムの数。
        /// </summary>
        public int Count
        {
            get { return _bufferBlock.Count; }
        }

        /// <summary>
        /// バッファに格納されるのを待っている数。
        /// </summary>
        public int WaitingCount
        {
            get { return _waitingCount; }
        }
        private int _waitingCount;

        /// <summary>
        /// ソースから値を受け取ります。
        /// </summary>
        /// <returns>受け取った値。</returns>
        public Task<T> ReceiveAsync()
        {
            return ReceiveAsync(TimeSpan.FromMilliseconds(-1), CancellationToken.None);
        }

        /// <summary>
        /// ソースから値を受け取ります。
        /// </summary>
        /// <param name="cancellationToken">操作を取り消す通知。</param>
        /// <returns>受け取った値。</returns>
        public Task<T> ReceiveAsync(CancellationToken cancellationToken)
        {
            return ReceiveAsync(TimeSpan.FromMilliseconds(-1), cancellationToken);
        }

        /// <summary>
        /// ソースから値を受け取ります。
        /// </summary>
        /// <param name="timeout">タイムアウトする時間。</param>
        /// <returns>受け取った値。</returns>
        public Task<T> ReceiveAsync(TimeSpan timeout)
        {
            return ReceiveAsync(timeout, CancellationToken.None);
        }

        /// <summary>
        /// ソースから値を受け取ります。
        /// </summary>
        /// <param name="timeout">タイムアウトする時間。</param>
        /// <param name="cancellationToken">操作を取り消す通知。</param>
        /// <returns>受け取った値。</returns>
        public Task<T> ReceiveAsync(TimeSpan timeout, CancellationToken cancellationToken)
        {
            var item = _bufferBlock.ReceiveAsync(timeout, cancellationToken);
            Interlocked.Increment(ref _waitingCount);
            this.OnPropertyChanged(() => this.Count);
            this.OnPropertyChanged(() => this.WaitingCount);
            return item;
        }

        /// <summary>
        /// ソースに値を送ります。
        /// </summary>
        /// <param name="item">送る値。</param>
        /// <returns>送るのに成功した場合はtrue。失敗した場合はfalse。</returns>
        public bool Post(T item)
        {
            var result = _bufferBlock.Post(item);
            if (result)
            {
                Interlocked.Decrement(ref _waitingCount);
                this.OnPropertyChanged(() => this.Count);
                this.OnPropertyChanged(() => this.WaitingCount);
            }
            return result;
        }

        /// <summary>
        /// ソースに値を送ります。
        /// </summary>
        /// <param name="item">送る値。</param>
        /// <returns>送るのに成功した場合はtrue。失敗した場合はfalse。</returns>
        public Task<bool> SendAsync(T item)
        {
            return SendAsync(item, CancellationToken.None);
        }

        /// <summary>
        /// ソースに値を送ります。
        /// </summary>
        /// <param name="item">送る値。</param>
        /// <param name="cancellationToken">操作を取り消す通知。</param>
        /// <returns>送るのに成功した場合はtrue。失敗した場合はfalse。</returns>
        public async Task<bool> SendAsync(T item, CancellationToken cancellationToken)
        {
            var result = await _bufferBlock.SendAsync(item, cancellationToken);
            if (result)
            {
                Interlocked.Decrement(ref _waitingCount);
                this.OnPropertyChanged(() => this.Count);
                this.OnPropertyChanged(() => this.WaitingCount);
            }
            return result;
        }
    }
}
