﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Katoributa.Net.Platform.Core.ComponentModel
{
    /// <summary>
    /// 列挙型の型コンバーター。
    /// </summary>
    public class EnumConverter : TypeConverter
    {
        /// <summary>
        /// 列挙型の型。
        /// </summary>
        private Type _enumType = default(Type);

        /// <summary>
        /// 列挙型の型を指定してEnumConverterのインスタンスを作成します。
        /// </summary>
        /// <param name="enumType">列挙型の型。</param>
        /// <exception cref="ArgumentException">列挙型でない型を指定した場合に発生します。</exception>
        public EnumConverter(Type enumType)
        {
            if (enumType == null)
            {
                throw new ArgumentNullException("enumType");
            }
            if (!enumType.GetTypeInfo().IsEnum)
            {
                throw new ArgumentException(string.Format("{0}は列挙型ではありません。", "enumType"), "enumType");
            }
            _enumType = enumType;
        }

        /// <summary>
        /// コンバーターが特定の型のオブジェクトをコンバーターの型に変換できるかどうかを示す値を返します。
        /// </summary>
        /// <param name="sourceType">変換前の型を表す System.Type。</param>
        /// <returns>コンバーターが変換を実行できる場合は true。それ以外の場合は false。</returns>
        protected override bool OnCanConvertFrom(Type sourceType)
        {
            var acceptableTypes = new List<Type>
            {
                typeof(string),
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
            };
            if (acceptableTypes.Any(x => x == sourceType))
            {
                return true;
            }
            return sourceType.GetTypeInfo().IsEnum;
        }

        /// <summary>
        /// 指定した値をコンバーターの型に変換します。
        /// </summary>
        /// <param name="value">変換対象の System.Object。</param>
        /// <returns>変換後の値を表す System.Object。</returns>
        /// <exception cref="System.NotSupportedException">変換を実行できません。</exception>
        protected override object OnConvertFrom(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            var text = value as string;
            if (!string.IsNullOrWhiteSpace(text))
            {
                return Convert.ChangeType(Enum.Parse(_enumType, text, true), _enumType, null);
            }
            return Enum.ToObject(_enumType, value);
        }
    }
}
