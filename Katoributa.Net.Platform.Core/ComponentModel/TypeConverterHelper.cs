﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Katoributa.Net.Platform.Core.ComponentModel
{
    /// <summary>
    /// TypeConverterに対するサポートクラス。
    /// </summary>
    public static class TypeConverterHelper
    {
        /// <summary>
        /// 指定した型のコンバーターを取得します。
        /// </summary>
        /// <param name="type">型。</param>
        /// <returns>型コンバーター。</returns>
        public static TypeConverter GetTypeConverter(Type type)
        {
            return TypeDescriptor.GetConverter(type);
        }

        /// <summary>
        /// 指定した型コンバーターを利用してオブジェクトを変換します。
        /// </summary>
        /// <param name="converter">型コンバーター。</param>
        /// <param name="value">変換するオブジェクト。</param>
        /// <returns>変換されたオブジェクト。</returns>
        public static object DoConversionFrom(TypeConverter converter, object value)
        {
            object convertValue = value;
            try
            {
                if (((converter != null) && (value != null)) && converter.CanConvertFrom(value.GetType()))
                {
                    convertValue = converter.ConvertFrom(value);
                }
            }
            catch (Exception exception)
            {
                if (!ShouldEatException(exception))
                {
                    throw;
                }
            }
            return convertValue;
        }

        /// <summary>
        /// 指定した例外を検証します。
        /// </summary>
        /// <param name="e">検証する例外。</param>
        /// <returns>例外にFormatExceptionが含まれる場合はtrue、それ以外の場合はfalse。</returns>
        private static bool ShouldEatException(Exception e)
        {
            bool flag = false;
            if (e.InnerException != null)
            {
                flag |= ShouldEatException(e.InnerException);
            }
            return (flag | (e is FormatException));
        }

        ///// <summary>
        ///// オブジェクトを指定した型に変換します。
        ///// </summary>
        ///// <param name="type">変換する型。</param>
        ///// <param name="value">変換するオブジェクト。</param>
        ///// <returns>変換されたオブジェクト。</returns>
        //public static object DoConversionFrom(Type type, object value)
        //{
        //    var convertValue = value;
        //    var typeConverter = TypeConverterHelper.GetTypeConverter(type);

        //    if (convertValue != null)
        //    {
        //        if ((typeConverter != null) && typeConverter.CanConvertTo(type))
        //        {
        //            convertValue = typeConverter.ConvertTo(convertValue, type);
        //        }
        //    }
        //    return convertValue;
        //}

        ///// <summary>
        ///// オブジェクトを指定したプロパティの型に変換します。
        ///// </summary>
        ///// <param name="property">変換するプロパティ。</param>
        ///// <param name="value">変換するオブジェクト。</param>
        ///// <returns>変換されたオブジェクト。</returns>
        //public static object DoConversionFrom(PropertyInfo property, object value)
        //{
        //    var convertValue = value;
        //    TypeConverter typeConverter = TypeConverterHelper.GetTypeConverter(property.PropertyType);

        //    if (convertValue != null)
        //    {
        //        if ((typeConverter != null) && typeConverter.CanConvertFrom(convertValue.GetType()))
        //        {
        //            convertValue = typeConverter.ConvertFrom(convertValue);
        //        }
        //        else
        //        {
        //            typeConverter = TypeConverterHelper.GetTypeConverter(convertValue.GetType());
        //            if ((typeConverter != null) && typeConverter.CanConvertTo(property.PropertyType))
        //            {
        //                convertValue = typeConverter.ConvertTo(convertValue, property.PropertyType);
        //            }
        //        }
        //    }
        //    return convertValue;
        //}
    }
}
