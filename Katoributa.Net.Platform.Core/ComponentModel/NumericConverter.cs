﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Katoributa.Net.Platform.Core.ComponentModel
{
    /// <summary>
    /// 数値型の型コンバーター。
    /// </summary>
    public class NumericConverter : TypeConverter
    {
        /// <summary>
        /// 数値型の型。
        /// </summary>
        private Type _numericType = default(Type);

        /// <summary>
        /// 数値型の型を指定してEnumConverterのインスタンスを作成します。
        /// </summary>
        /// <param name="numericType">数値型の型。</param>
        /// <exception cref="ArgumentException">数値型でない型を指定した場合に発生します。</exception>
        public NumericConverter(Type numericType)
        {
            var acceptableTypes = new List<Type>
            {
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(char),
                typeof(float),
                typeof(double),
                typeof(decimal),
            };
            if (!acceptableTypes.Any(x => x == numericType))
            {
                throw new ArgumentException(string.Format("{0}は数値型ではありません。", "numericType"), "numericType");
            }

            _numericType = numericType;
        }

        /// <summary>
        /// コンバーターが特定の型のオブジェクトをコンバーターの型に変換できるかどうかを示す値を返します。
        /// </summary>
        /// <param name="sourceType">変換前の型を表す System.Type。</param>
        /// <returns>コンバーターが変換を実行できる場合は true。それ以外の場合は false。</returns>
        protected override bool OnCanConvertFrom(Type sourceType)
        {
            var acceptableTypes = new List<Type>()
            {
                typeof(string),
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(char),
                typeof(float),
                typeof(double),
                typeof(decimal),
            };
            if (acceptableTypes.Any(x => x == sourceType))
            {
                return true;
            }

            if (!sourceType.GetTypeInfo().IsEnum)
            {
                return false;
            }

            var enumAcceptableTypes = new List<Type>()
            {
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
            };
            return enumAcceptableTypes.Any(x => x == _numericType);
        }

        /// <summary>
        /// 指定した値をコンバーターの型に変換します。
        /// </summary>
        /// <param name="value">変換対象の System.Object。</param>
        /// <returns>変換後の値を表す System.Object。</returns>
        /// <exception cref="System.NotSupportedException">変換を実行できません。</exception>
        protected override object OnConvertFrom(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            return Convert.ChangeType(value, _numericType, null);
        }
    }
}
