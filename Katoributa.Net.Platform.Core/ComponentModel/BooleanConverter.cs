﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core.ComponentModel
{
    /// <summary>
    /// bool型の型コンバーター。
    /// </summary>
    public class BooleanConverter : TypeConverter
    {
        /// <summary>
        /// コンバーターが特定の型のオブジェクトをコンバーターの型に変換できるかどうかを示す値を返します。
        /// </summary>
        /// <param name="sourceType">変換前の型を表す System.Type。</param>
        /// <returns>コンバーターが変換を実行できる場合は true。それ以外の場合は false。</returns>
        protected override bool OnCanConvertFrom(Type sourceType)
        {
            var acceptableTypes = new List<Type>()
            {
                typeof(string),
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(char),
                typeof(float),
                typeof(double),
                typeof(decimal),
            };
            return acceptableTypes.Any(x => x == sourceType);
        }

        /// <summary>
        /// 指定した値をコンバーターの型に変換します。
        /// </summary>
        /// <param name="value">変換対象の System.Object。</param>
        /// <returns>変換後の値を表す System.Object。</returns>
        /// <exception cref="System.NotSupportedException">変換を実行できません。</exception>
        protected override object OnConvertFrom(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            return Convert.ToBoolean(value);
        }
    }
}
