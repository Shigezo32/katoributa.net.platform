﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Katoributa.Net.Platform.Core.ComponentModel
{
    /// <summary>
    /// 属性、プロパティ、イベントなど、コンポーネントの特性に関する情報を提供します。このクラスは継承できません。
    /// </summary>
    /// <remarks>
    /// UNDONE:Shigeta ポータブルクラスライブラリでTypeConverterが使用できないため最低限必要な部分だけ実装する。
    ///                 機能が必要になったらその時点で追加。
    /// </remarks>
    public static class TypeDescriptor
    {
        /// <summary>
        /// ロック用オブジェクト。
        /// </summary>
        private static readonly object _lockObject = new object();

        /// <summary>
        /// コンバーターをキャッシュするハッシュテーブル。
        /// </summary>
        private static Dictionary<Type, TypeConverter> _converterHashtable = new Dictionary<Type, TypeConverter>();

        /// <summary>
        /// TypeDescriptorを初期化します。
        /// </summary>
        static TypeDescriptor()
        {
            _converterHashtable.Add(typeof(bool), new BooleanConverter());
            _converterHashtable.Add(typeof(string), new StringConverter());
            var numericTypes = new List<Type>
            {
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(char),
                typeof(float),
                typeof(double),
                typeof(decimal),
            };
            foreach (var type in numericTypes)
            {
                _converterHashtable.Add(type, new NumericConverter(type));
            }
        }

        /// <summary>
        /// 指定した型の型コンバーターを返します。
        /// </summary>
        /// <param name="type">対象のコンポーネントの System.Type。</param>
        /// <returns>指定した型の Katoributa.Net.Platform.Core.ComponentModel.TypeConverter。</returns>
        public static TypeConverter GetConverter(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            var typeInfo = type.GetTypeInfo();
            if (typeInfo.IsEnum)
            {
                return new EnumConverter(type);
            }

            if (_converterHashtable.ContainsKey(type))
            {
                return _converterHashtable[type];
            }

            var types = _converterHashtable.Where(x => typeInfo.IsSubclassOf(x.Key));
            if (types.Any())
            {
                return types.OrderByDescending(x => types.Count(y => x.Key.GetTypeInfo().IsSubclassOf(y.Key))).First().Value;
            }
            return new TypeConverter();
        }

        /// <summary>
        /// 指定した型のコンバーターを登録します。
        /// </summary>
        /// <param name="type">対象のコンポーネントの System.Type。</param>
        /// <param name="converter">指定した型の Katoributa.Net.Platform.Core.ComponentModel.TypeConverter。</param>
        public static void AttachConverter(Type type, TypeConverter converter)
        {
            lock (_lockObject)
            {
                _converterHashtable.Add(type, converter);
            }
        }

        /// <summary>
        /// 指定した型のコンバーターを解除します。
        /// </summary>
        /// <param name="type">対象のコンポーネントの System.Type。</param>
        public static void DetachConverter(Type type)
        {
            lock (_lockObject)
            {
                _converterHashtable.Remove(type);
            }
        }
    }
}
