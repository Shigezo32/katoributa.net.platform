﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core.ComponentModel
{
    /// <summary>
    ///  値の型を他の型に変換し、標準値とサブプロパティにアクセスするための統一的な方法を提供します。
    /// </summary>
    /// <remarks>
    /// UNDONE:Shigeta ポータブルクラスライブラリでTypeConverterが使用できないため最低限必要な部分だけ実装する。
    ///                 機能が必要になったらその時点で追加。
    /// </remarks>
    public class TypeConverter
    {
        /// <summary>
        /// コンバーターが特定の型のオブジェクトをコンバーターの型に変換できるかどうかを示す値を返します。
        /// </summary>
        /// <param name="sourceType">変換前の型を表す System.Type。</param>
        /// <returns>コンバーターが変換を実行できる場合は true。それ以外の場合は false。</returns>
        public bool CanConvertFrom(Type sourceType)
        {
            return OnCanConvertFrom(sourceType);
        }

        /// <summary>
        /// コンバーターが特定の型のオブジェクトをコンバーターの型に変換できるかどうかを示す値を返します。
        /// </summary>
        /// <param name="sourceType">変換前の型を表す System.Type。</param>
        /// <returns>コンバーターが変換を実行できる場合は true。それ以外の場合は false。</returns>
        protected virtual bool OnCanConvertFrom(Type sourceType)
        {
            return false;
        }

        /// <summary>
        /// コンバーターがオブジェクトを指定した型に変換できるかどうかを示す値を返します。
        /// </summary>
        /// <param name="destinationType">変換後の型を表す System.Type。</param>
        /// <returns>コンバーターが変換を実行できる場合は true。それ以外の場合は false。</returns>
        public bool CanConvertTo(Type destinationType)
        {
            return OnCanConvertTo(destinationType);
        }

        /// <summary>
        /// コンバーターがオブジェクトを指定した型に変換できるかどうかを示す値を返します。
        /// </summary>
        /// <param name="destinationType">変換後の型を表す System.Type。</param>
        /// <returns>コンバーターが変換を実行できる場合は true。それ以外の場合は false。</returns>
        protected virtual bool OnCanConvertTo(Type destinationType)
        {
            return false;
        }

        /// <summary>
        /// 指定した値をコンバーターの型に変換します。
        /// </summary>
        /// <param name="value">変換対象の System.Object。</param>
        /// <returns>変換後の値を表す System.Object。</returns>
        /// <exception cref="System.NotSupportedException">変換を実行できません。</exception>
        public object ConvertFrom(object value)
        {
            return OnConvertFrom(value);
        }

        /// <summary>
        /// 指定した値をコンバーターの型に変換します。
        /// </summary>
        /// <param name="value">変換対象の System.Object。</param>
        /// <returns>変換後の値を表す System.Object。</returns>
        /// <exception cref="System.NotSupportedException">変換を実行できません。</exception>
        protected virtual object OnConvertFrom(object value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 引数を使用して、指定した値オブジェクトを、指定した型に変換します。
        /// </summary>
        /// <param name="value">変換対象の System.Object。</param>
        /// <param name="destinationType">value パラメータの変換後の System.Type。</param>
        /// <returns>変換後の値を表す System.Object。</returns>
        /// <exception cref="System.NotSupportedException">変換を実行できません。</exception>
        public object ConvertTo(object value, Type destinationType)
        {
            return OnConvertTo(value, destinationType);
        }

        /// <summary>
        /// 引数を使用して、指定した値オブジェクトを、指定した型に変換します。
        /// </summary>
        /// <param name="value">変換対象の System.Object。</param>
        /// <param name="destinationType">value パラメータの変換後の System.Type。</param>
        /// <returns>変換後の値を表す System.Object。</returns>
        /// <exception cref="System.NotSupportedException">変換を実行できません。</exception>
        protected virtual object OnConvertTo(object value, Type destinationType)
        {
            throw new NotSupportedException();
        }
    }
}
