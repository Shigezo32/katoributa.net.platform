﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// 指定した型のモデルを生成可能なことを表します。
    /// </summary>
    /// <typeparam name="T">生成可能なモデルの型。</typeparam>
    public interface IModelCreator<T>
    {
        /// <summary>
        /// モデルを生成します。
        /// </summary>
        /// <returns>生成されたモデル。</returns>
        T CreateModel();
    }
}
