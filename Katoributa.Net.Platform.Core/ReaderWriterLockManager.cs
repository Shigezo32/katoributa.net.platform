﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// 読みとりまたは書き込みのロックを管理するクラス。
    /// </summary>
    public class ReaderWriterLockManager
    {
        /// <summary>
        /// ReaderWriterLockを管理するDictionary。
        /// </summary>
        private readonly ConcurrentDictionary<object, ReaderWriterLock> _lockDictionary = new ConcurrentDictionary<object, ReaderWriterLock>();

        /// <summary>
        /// Keyを指定してLockを取得します。
        /// </summary>
        /// <param name="key">Key。</param>
        /// <returns>ReaderWriterLock。</returns>
        public ReaderWriterLock GetLock(object key)
        {
            if (_lockDictionary.ContainsKey(key))
            {
                return _lockDictionary[key];
            }
            var lockObject = new ReaderWriterLock();
            if (!_lockDictionary.TryAdd(key, lockObject))
            {
                return _lockDictionary[key];
            }
            return lockObject;
        }
    }
}
