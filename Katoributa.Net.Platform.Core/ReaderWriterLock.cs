﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// 読みとりまたは書き込みのロックを取得するクラス。
    /// </summary>
    /// <remarks>
    /// Taskなどで排他ロックしようとすると同一スレッドで動作する？ため通常の排他制御が動作しない。
    /// オブジェクト単位でロックをかけ解除待ちはポーリングさせることで実現。
    /// </remarks>
    public class ReaderWriterLock
    {
        /// <summary>
        /// このクラスで利用するロック用オブジェクト。
        /// </summary>
        private readonly object _lockObject = new object();

        /// <summary>
        /// ロック取得待ちのキュー。
        /// </summary>
        private readonly ConcurrentQueue<LockObject> _requestQueue = new ConcurrentQueue<LockObject>();

        /// <summary>
        /// 読み取りロックを取得しているオブジェクトの一覧。
        /// </summary>
        private readonly List<LockObject> _readObjects = new List<LockObject>();

        /// <summary>
        /// 書き込みロックを取得しているオブジェクト。
        /// </summary>
        private object _writeObject = null;

        /// <summary>
        /// ReaderWriterLockのインスタンスを初期化します。
        /// </summary>
        public ReaderWriterLock()
        {
        }

        /// <summary>
        /// 書き込みロックされているかどうか。
        /// </summary>
        public bool IsWriteLocking
        {
            get
            {
                return _writeObject != null;
            }
        }

        /// <summary>
        /// 指定したオブジェクトが読み取りロックを取得しているかどうかを取得します。
        /// </summary>
        /// <param name="taregt">対象のオブジェクト。</param>
        /// <returns>読み取りロックを取得している場合はtrue、それ以外の場合はfalse。</returns>
        public bool IsReadLocking(LockObject taregt)
        {
            lock (_lockObject)
            {
                return _writeObject == taregt || _readObjects.Contains(taregt);
            }
        }

        /// <summary>
        /// 読み取りモードでロックに入ることを試みます。
        /// </summary>
        /// <returns>ロックを取得したオブジェクト。</returns>
        public async Task<LockObject> EnterReadLockAsync()
        {
            var obj = new LockObject(this);
            _requestQueue.Enqueue(obj);

            while (true)
            {
                lock (_lockObject)
                {
                    LockObject request = null;
                    _requestQueue.TryPeek(out request);
                    if (obj == request && !this.IsWriteLocking)
                    {
                        _readObjects.Add(obj);
                        if (!_requestQueue.TryDequeue(out request))
                        {
                            throw new Exception();
                        }
                        NotifyLockReleased();
                        return obj;
                    }
                    NotifyLockReleased();
                }
                await WaitUntilLockReleasedAsync();
            }
        }

        /// <summary>
        /// 書き込みモードでロックに入ることを試みます。
        /// </summary>
        /// <returns>ロックを取得したオブジェクト。</returns>
        public async Task<LockObject> EnterWriteLockAsync()
        {
            var obj = new LockObject(this);
            _requestQueue.Enqueue(obj);

            while (true)
            {
                lock (_lockObject)
                {
                    LockObject request = null;
                    _requestQueue.TryPeek(out request);
                    if (obj == request && !this.IsWriteLocking && !_readObjects.Any())
                    {
                        _writeObject = request;
                        if (!_requestQueue.TryDequeue(out request))
                        {
                            throw new Exception();
                        }
                        return obj;
                    }
                    else if (!_readObjects.Any())
                    {
                        NotifyLockReleased();
                    }
                }
                await WaitUntilLockReleasedAsync();
            }
        }

        /// <summary>
        /// 指定したオブジェクトのロックを解除します。
        /// </summary>
        /// <param name="lockObject">ロックを取得しているオブジェクト。</param>
        public void ExitLock(LockObject lockObject)
        {
            if (lockObject == null)
            {
                throw new ArgumentNullException("lockObject");
            }
            lock (_lockObject)
            {
                if (_writeObject == lockObject)
                {
                    _writeObject = null;
                }
                else if (!_readObjects.Remove(lockObject))
                {
                    throw new InvalidOperationException();
                }
                NotifyLockReleased();
            }
        }

        #region ロック解放通知

        /// <summary>
        /// ロック解放通知用のBufferBlock。
        /// </summary>
        private readonly BufferBlock<object> _bufferBlock = new BufferBlock<object>();

        /// <summary>
        /// ロックが解放されたことを通知します。
        /// </summary>
        private void NotifyLockReleased()
        {
            if (_requestQueue.Any() && !this.IsWriteLocking && _bufferBlock.Count == 0)
            {
                _bufferBlock.Post(new object());
            }
        }

        /// <summary>
        /// ロックが解放されるまで待機します。
        /// </summary>
        /// <returns>Task。</returns>
        private async Task WaitUntilLockReleasedAsync()
        {
            await Task.Delay(1);
            if (_requestQueue.Count() > 1 || this.IsWriteLocking || _readObjects.Any())
            {
                await _bufferBlock.ReceiveAsync();
            }
        }

        #endregion
    }
}
