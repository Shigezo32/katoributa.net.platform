﻿using System;
using System.Reflection;
using Katoributa.Net.Platform.Core.Extensions;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// 単一のModelを受け入れるシンプルなViewModelです。
    /// </summary>
    /// <typeparam name="T">受け入れさせるModelの型。</typeparam>
    public abstract class SimpleViewModel<T> : ViewModelBase, IModelAcceptable<T>, IModelCreator<T>
        where T : new()
    {
        /// <summary>
        /// 指定したモデルを与えます。
        /// </summary>
        /// <param name="model">モデル。</param>
        void IModelAcceptable.SupplyModel(object model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            if (typeof(T).GetTypeInfo().IsAssignableFrom(model.GetType().GetTypeInfo()))
            {
                throw new ArgumentException("指定したモデルは与えられません。");
            }
            SupplyModel((T)model);
        }

        /// <summary>
        /// 指定したモデルを与えます。
        /// </summary>
        /// <param name="model">モデル。</param>
        public void SupplyModel(T model)
        {
            OnSupplyModel(model);
        }

        /// <summary>
        /// 指定したモデルを与えます。
        /// </summary>
        /// <param name="model">モデル。</param>
        protected virtual void OnSupplyModel(T model)
        {
            this.ShallowCopyProperties(model);
        }

        /// <summary>
        /// モデルを生成します。
        /// </summary>
        /// <returns>生成されたモデル。</returns>
        public T CreateModel()
        {
            return OnCreateModel();
        }

        /// <summary>
        /// モデルを生成します。
        /// </summary>
        /// <returns>生成されたモデル。</returns>
        protected virtual T OnCreateModel()
        {
            //TODO Shigeta 実装方法検討
            var model = new T();
            return model.ShallowCopyProperties(this);
            //return default(T);
        }
    }
}
