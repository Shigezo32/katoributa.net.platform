﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// モデルを受け入れ可能なことを表します。
    /// </summary>
    public interface IModelAcceptable
    {
        /// <summary>
        /// 指定したモデルを与えます。
        /// </summary>
        /// <param name="model">モデル。</param>
        void SupplyModel(object model);
    }

    /// <summary>
    /// 指定した型のモデルを受け入れ可能なことを表します。
    /// </summary>
    /// <typeparam name="T">受け入れ可能なモデルの型。</typeparam>
    public interface IModelAcceptable<T> : IModelAcceptable
    {
        /// <summary>
        /// 指定したモデルを与えます。
        /// </summary>
        /// <param name="model">モデル。</param>
        void SupplyModel(T model);
    }
}
