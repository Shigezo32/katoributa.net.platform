﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Katoributa.Net.Platform.Core.Extensions;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// ViewModelの基底クラスです。
    /// </summary>
    [DataContract]
    public abstract class ViewModelBase : BindableBase, IDisposable
    {
        /// <summary>
        /// このインスタンスがDispose済みかどうかを表すフラグ。
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// このインスタンスがDispose済みかどうかを表すフラグ。
        /// </summary>
        protected bool Disposed { get { return _disposed; } }

        /// <summary>
        /// このインスタンスがGCに回収される時に呼び出されます。
        /// </summary>
        ~ViewModelBase()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Dispose可能な要素の一覧。
        /// </summary>
        protected List<IDisposable> DisposableCollection
        {
            get { return _disposableCollection; }
        }
        private readonly List<IDisposable> _disposableCollection = new List<IDisposable>();

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// このインスタンスによって使用されているすべてのリソースを解放します。
        /// </summary>
        /// <param name="disposing">マネージリソースを解放するかどうかを表す真偽値。</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            _disposed = true;
            if (disposing)
            {
                // マネージリソースの解放処理
                _disposableCollection.ClearAndDispose();
            }
            // アンマネージリソースの解放処理
        }

        /// <summary>
        /// このインスタンスで使用されていたすべてのリソースが解放済みの場合に例外を発生させます。
        /// </summary>
        /// <exception cref="ObjectDisposedException">リソースが解放済みの場合に発生。</exception>
        protected void ThrowExceptionIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().ToString());
            }
        }
    }
}
