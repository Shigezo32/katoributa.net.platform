﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// DisplayAttributeに関係する便利メソッド群です。
    /// </summary>
    public static class DisplayAttributeUtility
    {
        /// <summary>
        /// enumの表示名を取得します。DisplayAttributeが設定されていればそれを使います。
        /// </summary>
        /// <typeparam name="TEnum">enum。</typeparam>
        /// <param name="t">enumの値。</param>
        /// <returns>表示名。</returns>
        public static string GetDisplayName<TEnum>(this TEnum t)
            where TEnum : struct, IComparable, IFormattable
        {
            var info = typeof(TEnum).GetRuntimeField(t.ToString());
            return GetDisplayName(info);
        }

        /// <summary>
        /// フィールドの表示名を取得します。DisplayAttributeが設定されていればそれを使います。
        /// </summary>
        /// <param name="info">フィールドの属性。</param>
        /// <returns>表示名。</returns>
        public static string GetDisplayName(FieldInfo info)
        {
            if (info == null)
            {
                return null;
            }
            var d = info.GetCustomAttribute<DisplayAttribute>();
            return d == null
                ? info.Name
                : d.Name;
        }


        /// <summary>
        /// enumの短縮名を取得します。
        /// </summary>
        /// <typeparam name="TEnum">enum。</typeparam>
        /// <param name="t">enumの値。</param>
        /// <returns>短縮名。</returns>
        public static string GetShortName<TEnum>(this TEnum t)
            where TEnum : struct, IComparable, IFormattable
        {
            var info = typeof(TEnum).GetRuntimeField(t.ToString());
            return GetShortName(info);
        }

        /// <summary>
        /// フィールドの短縮名を取得します。
        /// </summary>
        /// <param name="info">フィールドの属性。</param>
        /// <returns>短縮名。</returns>
        public static string GetShortName(FieldInfo info)
        {
            if (info == null)
            {
                return null;
            }
            var d = info.GetCustomAttribute<DisplayAttribute>();
            return d == null
                ? null
                : d.ShortName;
        }

        /// <summary>
        /// enumの説明を取得します。
        /// </summary>
        /// <typeparam name="TEnum">enum。</typeparam>
        /// <param name="t">enumの値。</param>
        /// <returns>説明。</returns>
        public static string GetDescription<TEnum>(this TEnum t)
            where TEnum : struct, IComparable, IFormattable
        {
            var info = typeof(TEnum).GetRuntimeField(t.ToString());
            return GetDescription(info);
        }

        /// <summary>
        /// フィールドの説明を取得します。
        /// </summary>
        /// <param name="info">フィールドの属性。</param>
        /// <returns>説明。</returns>
        public static string GetDescription(FieldInfo info)
        {
            if (info == null)
            {
                return null;
            }
            var d = info.GetCustomAttribute<DisplayAttribute>();
            return d == null
                ? null
                : d.Description;
        }
    }
}
