﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// ネットワークに関係する便利メソッド群です。
    /// </summary>
    public interface INetworkUtility
    {
        /// <summary>
        /// インターネットへ接続しているかどうかを表します。
        /// </summary>
        /// <returns>インターネットに接続している場合はtrue。それ以外の場合はfalse。</returns>
        bool IsInternetAccess();

        /// <summary>
        /// ネットワークへ接続しているかどうかを表します。
        /// </summary>
        /// <returns>ネットワークに接続している場合はtrue。それ以外の場合はfalse。</returns>
        bool IsNetworkAccess();
    }
}
