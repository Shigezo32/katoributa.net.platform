﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// Streamに対して追加する便利機能群です。
    /// </summary>
    public static class StreamExtension
    {
        /// <summary>
        /// バッファーから読み取ったデータを使用して、現在のストリームにバイトのブロックを書き込みます。
        /// </summary>
        /// <param name="target">対象のストリーム。</param>
        /// <param name="buffer">データの書き込み元となるバッファー。</param>
        public static void Write(this Stream target, byte[] buffer)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }
            target.Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// バッファーから読み取ったデータを使用して、現在のストリームにバイトのブロックを書き込みます。
        /// </summary>
        /// <param name="target">対象のストリーム。</param>
        /// <param name="buffer">データの書き込み元となるバッファー。</param>
        /// <returns>Task。</returns>
        public static async Task WriteAsync(this Stream target, byte[] buffer)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }
            await target.WriteAsync(buffer, 0, buffer.Length);
        }
    }
}
