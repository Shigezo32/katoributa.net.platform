﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// ハードウェアに関係する便利メソッド群です。
    /// </summary>
    public interface IHardwareUtility
    {
        /// <summary>
        /// 現在のハードウェアを表すハードウェア識別子を取得します。
        /// </summary>
        /// <returns>ハードウェア識別子。</returns>
        string GetHardwareId();
    }
}
