﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// コレクションに対して追加する便利機能群です。
    /// </summary>
    public static class CollectionExtension
    {
        /// <summary>
        /// 指定したコレクションの要素を末尾に追加します。
        /// </summary>
        /// <typeparam name="T">コレクションの型。</typeparam>
        /// <param name="target">要素が追加されるコレクション。</param>
        /// <param name="collection">対象の末尾に追加する要素のコレクション。</param>
        public static void AddRange<T>(this ICollection<T> target, IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            foreach (var item in collection)
            {
                target.Add(item);
            }
        }

        /// <summary>
        /// 指定したコレクションのすべての要素を解放したあと削除します。
        /// </summary>
        /// <typeparam name="T">IDisposable。</typeparam>
        /// <param name="target">対象となるコレクション。</param>
        public static void ClearAndDispose<T>(this ICollection<T> target)
            where T : IDisposable
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            foreach (var item in target)
            {
                item.Dispose();
            }
            target.Clear();
        }
    }
}
