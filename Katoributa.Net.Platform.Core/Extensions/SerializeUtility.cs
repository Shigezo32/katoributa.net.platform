﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// シリアライズに関係する便利メソッド群です。
    /// </summary>
    public static class SerializeUtility
    {
        /// <summary>
        /// 対象のオブジェクトをXML文字列にシリアライズします。
        /// </summary>
        /// <typeparam name="T">対象の型。</typeparam>
        /// <param name="target">シリアライズするオブジェクト。</param>
        /// <returns>シリアライズされた文字列。</returns>
        public static string Serialize<T>(this T target)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            using (var ms = new MemoryStream())
            {
                var serializer = new DataContractSerializer(target.GetType());
                serializer.WriteObject(ms, target);
                var buffer = ms.ToArray();
                return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            }
        }

        /// <summary>
        /// 対象のオブジェクトをXML文字列にシリアライズします。戻り値は、シリアライズが成功したかどうかを示します。
        /// </summary>
        /// <typeparam name="T">対象の型。</typeparam>
        /// <param name="target">シリアライズするオブジェクト。</param>
        /// <param name="result">シリアライズされた文字列。</param>
        /// <returns>シリアライズが成功した場合はtrue、失敗した場合はfalse。</returns>
        public static bool TrySerialize<T>(this T target, out string result)
        {
            try
            {
                result = target.Serialize();
                return true;
            }
            catch (ArgumentNullException)
            {
            }
            catch (SerializationException)
            {
            }
            catch
            {
            }
            result = null;
            return false;
        }

        /// <summary>
        /// シリアライズされた文字列をオブジェクトに復元します。
        /// </summary>
        /// <typeparam name="T">復元するオブジェクトの型。</typeparam>
        /// <param name="target">シリアライズされた文字列。</param>
        /// <returns>復元されたオブジェクト。</returns>
        public static T Deserialize<T>(this string target)
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(target)))
            {
                var serializer = new DataContractSerializer(typeof(T));
                return (T)serializer.ReadObject(ms);
            }
        }

        /// <summary>
        /// シリアライズされた文字列をオブジェクトに復元します。戻り値は、復元に成功したかどうかを示します。
        /// </summary>
        /// <typeparam name="T">復元するオブジェクトの型。</typeparam>
        /// <param name="target">シリアライズされた文字列。</param>
        /// <param name="result">復元されたオブジェクト。</param>
        /// <returns>復元に成功した場合はtrue、失敗した場合はfalse。</returns>
        public static bool TryDeserialize<T>(this string target, out T result)
        {
            try
            {
                result = target.Deserialize<T>();
                return true;
            }
            catch (SerializationException)
            {
            }
            catch
            {
            }
            result = default(T);
            return false;
        }
    }
}
