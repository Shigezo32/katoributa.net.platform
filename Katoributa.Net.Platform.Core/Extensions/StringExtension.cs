﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// stringに対して追加する便利機能群です。
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// 指定したエンコードで指定したバイト数以下に安全に文字列を切り詰めます。
        /// </summary>
        /// <param name="s">対象の文字列。</param>
        /// <param name="encoding"><see cref="Encoding"/>。</param>
        /// <param name="maxByteCount">最大のバイト数。</param>
        /// <returns>切り詰めた文字列。</returns>
        public static string LeftB(this string s, Encoding encoding, int maxByteCount)
        {
            var bytes = encoding.GetBytes(s);
            if (bytes.Length <= maxByteCount) return s;

            var result = s.Substring(0, encoding.GetString(bytes, 0, maxByteCount).Length);

            while (encoding.GetByteCount(result) > maxByteCount)
            {
                result = result.Substring(0, result.Length - 1);
            }
            return result;
        }
    }
}
