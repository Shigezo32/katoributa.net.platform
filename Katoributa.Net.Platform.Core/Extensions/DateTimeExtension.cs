﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// DateTimeに対して追加する便利機能群です。
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        /// UNIXのエポック時刻。
        /// </summary>
        private static readonly DateTime _unixEpochDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// 指定したDateTimeのUNIXの時刻を取得します。
        /// </summary>
        /// <param name="target">DateTime。</param>
        public static long GetUnixTime(this DateTime target)
        {
            target = target.ToUniversalTime();
            return (long)target.Subtract(_unixEpochDateTime).TotalSeconds;
        }
    }
}
