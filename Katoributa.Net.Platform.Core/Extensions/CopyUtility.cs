﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// コピーに関係する便利メソッド群です。
    /// </summary>
    public static class CopyUtility
    {
        /// <summary>
        /// 指定したオブジェクトをディープコピーします。
        /// </summary>
        /// <typeparam name="T">コピーするオブジェクトの型。</typeparam>
        /// <param name="target">コピーするオブジェクト。</param>
        /// <returns>コピーされたオブジェクト。</returns>
        /// <remarks>
        /// 下記を参考、Serializable属性でなくDataContract属性を利用。
        /// http://d.hatena.ne.jp/tekk/20100131/1264913887
        /// </remarks>
        public static T DeepCopy<T>(this T target)
        {
            using (var ms = new MemoryStream())
            {
                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(ms, target);
                ms.Position = 0;
                return (T)serializer.ReadObject(ms);
            }
        }

        /// <summary>
        /// 指定したオブジェクトのプロパティをシャローコピーします。
        /// </summary>
        /// <typeparam name="T">コピー先のオブジェクトの型。</typeparam>
        /// <param name="target">コピー先のオブジェクト。</param>
        /// <param name="source">コピー元のオブジェクト。</param>
        /// <returns>コピーされたオブジェクト。</returns>
        public static T ShallowCopyProperties<T>(this T target, object source)
        {
            return ShallowCopyProperties(target, source, null);
        }

        /// <summary>
        /// 指定したオブジェクトのプロパティをシャローコピーします。
        /// </summary>
        /// <typeparam name="T">コピー先のオブジェクトの型。</typeparam>
        /// <param name="target">コピー先のオブジェクト。</param>
        /// <param name="source">コピー元のオブジェクト。</param>
        /// <param name="ignorePropertyNames">無視するプロパティ名のリスト。</param>
        /// <returns>コピーされたオブジェクト。</returns>
        public static T ShallowCopyProperties<T>(this T target, object source, IEnumerable<string> ignorePropertyNames)
        {
            return (T)ShallowCopyProperties((object)target, source, ignorePropertyNames);
        }

        /// <summary>
        /// 指定したオブジェクトのプロパティをシャローコピーします。
        /// </summary>
        /// <param name="target">コピー先のオブジェクト。</param>
        /// <param name="source">コピー元のオブジェクト。</param>
        /// <returns>コピーされたオブジェクト。</returns>
        public static object ShallowCopyProperties(object target, object source)
        {
            return ShallowCopyProperties(target, source, null);

        }

        /// <summary>
        /// 指定したオブジェクトのプロパティをシャローコピーします。
        /// </summary>
        /// <param name="target">コピー先のオブジェクト。</param>
        /// <param name="source">コピー元のオブジェクト。</param>
        /// <param name="ignorePropertyNames">無視するプロパティ名のリスト。</param>
        /// <returns>コピーされたオブジェクト。</returns>
        public static object ShallowCopyProperties(this object target, object source, IEnumerable<string> ignorePropertyNames)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            //HACK:Shigeta 手抜き。リフレクションを使用しているが式木を利用したコードに変更した方が良い。その際にキャッシュの仕組みを作ると更に良い
            foreach (var targetProperty in target.GetType().GetRuntimeProperties())
            {
                if (ignorePropertyNames != null && ignorePropertyNames.Any(x => x == targetProperty.Name))
                {
                    continue;
                }

                var sourceProperty = source.GetType().GetRuntimeProperty(targetProperty.Name);
                if (sourceProperty == null)
                {
                    continue;
                }

                if (!targetProperty.PropertyType.GetTypeInfo().IsAssignableFrom(sourceProperty.PropertyType.GetTypeInfo()))
                {
                    continue;
                }

                var methodInfo = targetProperty.SetMethod;
                if (methodInfo == null)
                {
                    continue;
                }

                methodInfo.Invoke(target, new object[] { sourceProperty.GetValue(source, null) });
            }
            return target;
        }
    }
}
