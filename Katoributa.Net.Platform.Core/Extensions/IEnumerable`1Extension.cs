﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core.Extensions
{
    /// <summary>
    /// IEnumerable`1に関する便利クラス。
    /// </summary>
    public static class IEnumerable_1Extension
    {
        /// <summary>
        /// 入力シーケンスの各要素をランダムに並び替えます。
        /// </summary>
        /// <typeparam name="T">sourceの要素の型。</typeparam>
        /// <param name="src">ランダムに並び替えるシーケンス。</param>
        /// <returns>ランダムに並び替えられたシーケンス。</returns>
        /// <remarks>
        /// http://d.hatena.ne.jp/saiya_moebius/20090311/1236712899
        /// </remarks>
        public static T[] Shuffle<T>(this IEnumerable<T> src)
        {
            return Shuffle(src, null);
        }

        /// <summary>
        /// 入力シーケンスの各要素をランダムに並び替えます。
        /// </summary>
        /// <typeparam name="T">sourceの要素の型。</typeparam>
        /// <param name="src">ランダムに並び替えるシーケンス。</param>
        /// <param name="rand">使用する疑似乱数ジェネレーター。</param>
        /// <returns>ランダムに並び替えられたシーケンス。</returns>
        public static T[] Shuffle<T>(this IEnumerable<T> src, Random rand)
        {
            if (rand == null) rand = new Random();

            var result = src.ToArray();
            for (int i = result.Length - 1; i >= 1; i--)
            {
                var n = rand.Next(i + 1);

                var item = result[i];
                result[i] = result[n];
                result[n] = item;
            }
            return result;
        }

        /// <summary>
        /// シーケンスの要素を指定のサイズごとに分割します。
        /// </summary>
        /// <typeparam name="TSource">source の要素の型。</typeparam>
        /// <param name="source">分割するシーケンス。</param>
        /// <param name="size">分割する要素数。</param>
        /// <returns>分割されたIEnumerable&lt;IEnumerable&lt;TSource&gt;&gt;。</returns>
        public static IEnumerable<IEnumerable<TSource>> Split<TSource>(this IEnumerable<TSource> source, int size)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            if (size <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }
            var count = source.Count();
            return Enumerable.Range(0, (count / size) + ((count % size) > 0 ? 1 : 0)).Select(x => source.Skip(x * size).Take(size));
        }
    }
}
