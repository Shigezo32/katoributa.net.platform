﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Core.NativeExtensions
{
    /// <summary>
    /// NativeMethodsを利用するstringに対して追加する便利機能群です。
    /// </summary>
    public static class NativeStringExtension
    {
        /// <summary>
        /// 指定した文字列を変換します。
        /// </summary>
        /// <param name="s">対象の文字列。</param>
        /// <param name="flags">文字列マッピング時の変換方法、または並び替えキーを生成するかどうか指定する一連のフラグ。</param>
        /// <returns>変換された文字列。</returns>
        private static string Convert(this string s, NativeMethods.DwMapFlags flags)
        {
            if (s == null)
            {
                throw new ArgumentNullException(nameof(s));
            }
            var locale = NativeMethods.GetUserDefaultLCID();
            var result = new string(' ', s.Length * 2);
            var length = NativeMethods.LCMapStringW(locale, flags, s, s.Length, result, result.Length);
            return result.Substring(0, length);
        }

        /// <summary>
        /// 指定した文字列を全角に変換します。
        /// </summary>
        /// <param name="s">対象の文字列。</param>
        /// <returns>変換された文字列。</returns>
        public static string ToZenkaku(this string s)
        {
            return Convert(s, NativeMethods.DwMapFlags.LCMAP_FULLWIDTH).Replace('\\', '￥');
        }

        /// <summary>
        /// 指定した文字列を半角に変換します。
        /// </summary>
        /// <param name="s">対象の文字列。</param>
        /// <returns>変換された文字列。</returns>
        public static string ToHankaku(this string s)
        {
            if (s?.IndexOf('￥') >= 0)
            {
                return Convert(s.Replace('￥', '\\'), NativeMethods.DwMapFlags.LCMAP_HALFWIDTH | NativeMethods.DwMapFlags.LCMAP_KATAKANA);
            }
            return Convert(s, NativeMethods.DwMapFlags.LCMAP_HALFWIDTH | NativeMethods.DwMapFlags.LCMAP_KATAKANA);
        }
    }
}
