﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// 比較演算子。
    /// </summary>
    public enum ComparisonOperator
    {
        /// <summary>
        /// 等しい。
        /// </summary>
        Equal,

        /// <summary>
        /// 等しくない。
        /// </summary>
        NotEqual,

        /// <summary>
        /// より小さい。
        /// </summary>
        LessThan,

        /// <summary>
        /// 以下。
        /// </summary>
        LessThanOrEqual,

        /// <summary>
        /// より大きい。
        /// </summary>
        GreaterThan,

        /// <summary>
        /// 以上。
        /// </summary>
        GreaterThanOrEqual
    }

    /// <summary>
    /// 論理演算子。
    /// </summary>
    public enum LogicalOperator
    {
        /// <summary>
        /// 論理積。
        /// </summary>
        And,

        /// <summary>
        /// 論理和。
        /// </summary>
        Or
    }
}
