﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// 比較条件のインターフェース。
    /// </summary>
    public interface ICondition
    {
        /// <summary>
        /// 評価します。
        /// </summary>
        /// <returns>条件に該当する場合はtrue、該当しない場合はfalse。</returns>
        bool Evaluate();
    }
}
