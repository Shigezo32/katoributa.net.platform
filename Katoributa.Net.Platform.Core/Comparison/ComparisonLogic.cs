﻿using System;
using System.Globalization;
using Katoributa.Net.Platform.Core.ComponentModel;

namespace Katoributa.Net.Platform.Core
{
    /// <summary>
    /// 比較条件のロジック。
    /// </summary>
    public static class ComparisonLogic
    {
        /// <summary>
        /// 指定したIComparableを評価します。
        /// </summary>
        /// <param name="leftOperand">左の値。</param>
        /// <param name="operatorType">比較条件。</param>
        /// <param name="rightOperand">右の値。</param>
        /// <returns>評価条件に該当する場合はtrue、該当しない場合はfalse。</returns>
        private static bool EvaluateComparable(IComparable leftOperand, ComparisonOperator operatorType, IComparable rightOperand)
        {
            object obj2 = null;
            try
            {
                obj2 = Convert.ChangeType(rightOperand, leftOperand.GetType(), CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            if (obj2 == null)
            {
                return (operatorType == ComparisonOperator.NotEqual);
            }
            int num = leftOperand.CompareTo((IComparable)obj2);
            switch (operatorType)
            {
                case ComparisonOperator.Equal:
                    return (num == 0);

                case ComparisonOperator.NotEqual:
                    return (num != 0);

                case ComparisonOperator.LessThan:
                    return (num < 0);

                case ComparisonOperator.LessThanOrEqual:
                    return (num <= 0);

                case ComparisonOperator.GreaterThan:
                    return (num > 0);

                case ComparisonOperator.GreaterThanOrEqual:
                    return (num >= 0);
            }
            return false;
        }

        /// <summary>
        /// 評価の実装。
        /// </summary>
        /// <param name="leftOperand">左の値。</param>
        /// <param name="operatorType">比較条件。</param>
        /// <param name="rightOperand">右の値。</param>
        /// <returns>比較条件に該当する場合はtrue、該当しない場合はfalse。</returns>
        public static bool EvaluateImpl(object leftOperand, ComparisonOperator operatorType, object rightOperand)
        {
            if (leftOperand != null)
            {
                Type type = leftOperand.GetType();
                if (rightOperand != null)
                {
                    rightOperand = TypeConverterHelper.DoConversionFrom(TypeConverterHelper.GetTypeConverter(type), rightOperand);
                }
            }
            IComparable comparable = leftOperand as IComparable;
            IComparable comparable2 = rightOperand as IComparable;
            if ((comparable != null) && (comparable2 != null))
            {
                return EvaluateComparable(comparable, operatorType, comparable2);
            }
            switch (operatorType)
            {
                case ComparisonOperator.Equal:
                    return object.Equals(leftOperand, rightOperand);

                case ComparisonOperator.NotEqual:
                    return !object.Equals(leftOperand, rightOperand);

                case ComparisonOperator.LessThan:
                case ComparisonOperator.LessThanOrEqual:
                case ComparisonOperator.GreaterThan:
                case ComparisonOperator.GreaterThanOrEqual:
                    if ((comparable == null) && (comparable2 == null))
                    {
                        //throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ExceptionStringTable.InvalidOperands, new object[] { (leftOperand != null) ? leftOperand.GetType().Name : "null", (rightOperand != null) ? rightOperand.GetType().Name : "null", operatorType.ToString() }));
                    }
                    if (comparable == null)
                    {
                        //throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ExceptionStringTable.InvalidLeftOperand, new object[] { (leftOperand != null) ? leftOperand.GetType().Name : "null", operatorType.ToString() }));
                    }
                    //throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ExceptionStringTable.InvalidRightOperand, new object[] { (rightOperand != null) ? rightOperand.GetType().Name : "null", operatorType.ToString() }));
                    throw new ArgumentException();
            }
            return false;
        }
    }
}
