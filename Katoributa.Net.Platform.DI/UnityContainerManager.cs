﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace Katoributa.Net.Platform.DI
{
    /// <summary>
    /// UnityContainerの管理者。
    /// </summary>
    public static class UnityContainerManager
    {
        /// <summary>
        /// 標準で使用されるKey。
        /// </summary>
        private static readonly object _defaultKey = new Object();

        /// <summary>
        /// UnityContainerを管理するDictionary。
        /// </summary>
        private static readonly ConcurrentDictionary<object, IUnityContainer> _containerDictionary = new ConcurrentDictionary<object, IUnityContainer>();

        /// <summary>
        /// UnityContainerを取得します。
        /// </summary>
        /// <returns>UnityContainer。</returns>
        public static IUnityContainer GetUnityContainer()
        {
            return GetUnityContainer(_defaultKey);
        }

        /// <summary>
        /// Keyを指定してUnityContainerを取得します。
        /// </summary>
        /// <param name="key">Key。</param>
        /// <returns>UnityContainer。</returns>
        public static IUnityContainer GetUnityContainer(object key)
        {
            if (_containerDictionary.ContainsKey(key))
            {
                return _containerDictionary[key];
            }
            var container = new UnityContainer();
            if (!_containerDictionary.TryAdd(key, container))
            {
                return _containerDictionary[key];
            }
            return container;
        }

        /// <summary>
        /// UnityContainerを解除します。
        /// </summary>
        public static void Detach()
        {
            Detach(_defaultKey);
        }

        /// <summary>
        /// Keyを指定してUnityContainerを解除します。
        /// </summary>
        /// <param name="key">Key。</param>
        public static void Detach(object key)
        {
            if (_containerDictionary.ContainsKey(key))
            {
                IUnityContainer container = null;
                if (_containerDictionary.TryRemove(key, out container))
                {
                    container.Dispose();
                }
            }
        }
    }
}
