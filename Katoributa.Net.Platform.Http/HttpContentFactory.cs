﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Katoributa.Net.Platform.Http
{
    /// <summary>
    /// HttpContentFactoryのFactoryクラス。
    /// </summary>
    public static class HttpContentFactory
    {
        /// <summary>
        /// 指定した名前と値のペアのコレクションとエンコードを指定してHttpContentを作成します。
        /// </summary>
        /// <param name="nameValueCollectionm">指定した名前と値のペアのコレクション。</param>
        /// <param name="encoding">エンコード。</param>
        public static HttpContent CreateFormUrlEncodedContent(IEnumerable<KeyValuePair<string, string>> nameValueCollectionm, Encoding encoding)
        {
            if (nameValueCollectionm == null)
            {
                throw new ArgumentNullException("nameValueCollectionm");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }

            var postData = new StringBuilder();
            foreach (var item in nameValueCollectionm)
            {
                if (postData.Length > 0)
                {
                    postData.Append("&");
                }
                postData.Append(WebUtility.UrlEncode(item.Key));
                postData.Append("=");
                postData.Append(WebUtility.UrlEncode(item.Value));
            }
            var buffer = encoding.GetBytes(postData.ToString());
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            return content;
        }
    }
}
