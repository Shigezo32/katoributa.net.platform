﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Katoributa.Net.Platform.TestUtility
{
    public abstract class TestBase
    {
        [TestInitialize]
        public void TestInitialize()
        {
            OnTestInitialize();
        }

        protected virtual void OnTestInitialize()
        {
        }

        [TestCleanup]
        public void TestCelean()
        {
            OnTestCelean();
        }

        protected virtual void OnTestCelean()
        {
        }
    }
}
