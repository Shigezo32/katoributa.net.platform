﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Katoributa.Net.Platform.TestUtility
{
    public static class AssertEx
    {
        public static bool PropertiesEquals(object expected, object actual, IEnumerable<string> ignorePropertyNames = null)
        {
            if (expected == null && actual == null)
            {
                return true;
            }
            Assert.IsNotNull(expected);
            Assert.IsNotNull(actual);

            //HACK:Shigeta 手抜き。リフレクションを使用しているが式木を利用したコードに変更した方が良い。その際にキャッシュの仕組みを作ると更に良い
            foreach (var expectedProperty in expected.GetType().GetProperties())
            {
                if (ignorePropertyNames != null && ignorePropertyNames.Any(x => x == expectedProperty.Name))
                {
                    continue;
                }

                var actualProperty = actual.GetType().GetProperty(expectedProperty.Name);
                if (actualProperty == null)
                {
                    continue;
                }

                if (!expectedProperty.PropertyType.IsAssignableFrom(actualProperty.PropertyType))
                {
                    continue;
                }

                var expectedValue = expectedProperty.GetValue(expected);
                var actualValue = actualProperty.GetValue(actual);

                var isCollection = false;
                try
                {
                    expectedProperty.PropertyType.GetInterfaceMap(typeof(ICollection));
                    isCollection = true;
                }
                catch (ArgumentException)
                {
                }

                if (isCollection)
                {
                    CollectionAssert.AreEqual(expectedValue as ICollection, actualValue as ICollection);
                }
                else
                {
                    Assert.AreEqual(expectedValue, actualValue);
                }
            }
            return true;
        }
    }
}
