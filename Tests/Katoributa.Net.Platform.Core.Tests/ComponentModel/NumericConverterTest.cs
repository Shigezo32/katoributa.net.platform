﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Katoributa.Net.Platform.TestUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Katoributa.Net.Platform.Core.ComponentModel.Tests
{
    [TestClass]
    public class NumericConverterTest : TestBase
    {
        private enum EnumA : byte { A, B, C, D, E }

        protected override void OnTestInitialize()
        {
            base.OnTestInitialize();
        }

        [TestMethod]
        public void ConstructorTest()
        {
            var acceptableTypes = new List<Type>
            {
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(char),
                typeof(float),
                typeof(double),
                typeof(decimal),
            };
            foreach (var type in acceptableTypes)
            {
                new NumericConverter(type);
            }
        }

        [TestMethod]
        public void ConstructorTest_異常系()
        {
            //数値以外
            try
            {
                new NumericConverter(typeof(string));
                Assert.Fail();
            }
            catch (ArgumentException)
            {
            }
        }

        [TestMethod]
        public void CanConvertFromTest()
        {
            var converter = new NumericConverter(typeof(byte));
            Assert.IsTrue(converter.CanConvertFrom(typeof(string)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(byte)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(sbyte)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(short)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(ushort)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(int)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(uint)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(long)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(ulong)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(char)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(float)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(double)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(decimal)));
            Assert.IsFalse(converter.CanConvertFrom(typeof(object)));
        }

        [TestMethod]
        public void CanConvertFromTest_列挙型()
        {
            var converter = new NumericConverter(typeof(byte));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(sbyte));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(short));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(ushort));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(int));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(uint));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(long));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(ulong));
            Assert.IsTrue(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(char));
            Assert.IsFalse(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(float));
            Assert.IsFalse(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(double));
            Assert.IsFalse(converter.CanConvertFrom(typeof(EnumA)));
            converter = new NumericConverter(typeof(decimal));
            Assert.IsFalse(converter.CanConvertFrom(typeof(EnumA)));
        }

        [TestMethod]
        public void ConvertFromTest()
        {
            var converter = new NumericConverter(typeof(byte));
            Assert.AreEqual(converter.ConvertFrom("0"), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((byte)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((sbyte)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((short)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((ushort)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((int)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((uint)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((long)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((ulong)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((char)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((float)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((double)0), (byte)0);
            Assert.AreEqual(converter.ConvertFrom((decimal)0), (byte)0);
        }

        [TestMethod]
        public void ConvertFromTest_異常系()
        {
            var target = new NumericConverter(typeof(int));

            //null
            try
            {
                target.ConvertFrom(null);
                Assert.Fail();
            }
            catch (ArgumentNullException)
            {
            }
        }
    }
}
