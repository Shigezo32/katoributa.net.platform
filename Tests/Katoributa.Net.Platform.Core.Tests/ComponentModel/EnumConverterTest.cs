﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Katoributa.Net.Platform.TestUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Katoributa.Net.Platform.Core.ComponentModel.Tests
{
    [TestClass]
    public class EnumConverterTest : TestBase
    {
        private enum EnumA : byte { A, B, C, D, E }
        private enum EnumB : sbyte { A, B, C, D, E }
        private enum EnumC : short { A, B, C, D, E }
        private enum EnumD : ushort { A, B, C, D, E }
        private enum EnumE : int { A, B, C, D, E }
        private enum EnumF : uint { A, B, C, D, E }
        private enum EnumG : long { A, B, C, D, E }
        private enum EnumH : ulong { A, B, C, D, E }
        [Flags]
        private enum EnumI { A, B, C, D, E }

        protected override void OnTestInitialize()
        {
            base.OnTestInitialize();
        }

        [TestMethod]
        public void ConstructorTest()
        {
            new EnumConverter(typeof(EnumA));
            new EnumConverter(typeof(EnumB));
            new EnumConverter(typeof(EnumC));
            new EnumConverter(typeof(EnumD));
            new EnumConverter(typeof(EnumE));
            new EnumConverter(typeof(EnumF));
            new EnumConverter(typeof(EnumG));
            new EnumConverter(typeof(EnumH));
            new EnumConverter(typeof(EnumI));
        }

        [TestMethod]
        public void ConstructorTest_異常系()
        {
            //null
            try
            {
                new EnumConverter(null);
                Assert.Fail();
            }
            catch (ArgumentNullException)
            {
            }

            //enum以外
            try
            {
                new EnumConverter(typeof(int));
                Assert.Fail();
            }
            catch (ArgumentException)
            {
            }
        }

        [TestMethod]
        public void CanConvertFromTest()
        {
            var converter = new EnumConverter(typeof(EnumA));
            Assert.IsTrue(converter.CanConvertFrom(typeof(string)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(byte)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(sbyte)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(short)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(ushort)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(int)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(uint)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(long)));
            Assert.IsTrue(converter.CanConvertFrom(typeof(ulong)));
            Assert.IsFalse(converter.CanConvertFrom(typeof(object)));
        }

        [TestMethod]
        public void ConvertFromTest()
        {
            var converter = new EnumConverter(typeof(EnumA));
            Assert.AreEqual(converter.ConvertFrom("C"), EnumA.C);
            Assert.AreEqual(converter.ConvertFrom("2"), EnumA.C);
            Assert.AreEqual(converter.ConvertFrom((byte)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom((sbyte)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom((short)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom((ushort)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom((int)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom((uint)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom((long)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom((ulong)0), EnumA.A);
            Assert.AreEqual(converter.ConvertFrom(EnumB.A), EnumA.A);
        }

        [TestMethod]
        public void ConvertFromTest_異常系()
        {
            var target = new EnumConverter(typeof(EnumA));

            //null
            try
            {
                target.ConvertFrom(null);
                Assert.Fail();
            }
            catch (ArgumentNullException)
            {
            }
        }
    }
}
