﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Katoributa.Net.Platform.TestUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Katoributa.Net.Platform.Core.ComponentModel.Tests
{
    [TestClass]
    public class TypeDescriptorTest : TestBase
    {
        private enum EnumA : byte { }
        private enum EnumB : sbyte { }
        private enum EnumC : short { }
        private enum EnumD : ushort { }
        private enum EnumE : int { }
        private enum EnumF : uint { }
        private enum EnumG : long { }
        private enum EnumH : ulong { }
        [Flags]
        private enum EnumI { }

        private class ClassA { }
        private class ClassB : ClassA { }
        private class ClassC : ClassB { }
        private class ClassD : ClassC { }
        private class ClassE : ClassD { }

        protected override void OnTestInitialize()
        {
            base.OnTestInitialize();
        }

        protected override void OnTestCelean()
        {
            base.OnTestCelean();
            TypeDescriptor.DetachConverter(typeof(object));
        }

        [TestMethod]
        public void GetConverterTest()
        {
            TypeConverter actual = null;

            actual = TypeDescriptor.GetConverter(typeof(string));
            Assert.IsInstanceOfType(actual, typeof(StringConverter));

            actual = TypeDescriptor.GetConverter(typeof(object));
            Assert.IsInstanceOfType(actual, typeof(TypeConverter));
        }

        [TestMethod]
        public void GetConverterTest_数値型()
        {
            var numericTypes = new List<Type>
            {
                typeof(byte),
                typeof(sbyte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(char),
                typeof(float),
                typeof(double),
                typeof(decimal),
            };

            foreach (var type in numericTypes)
            {
                Assert.IsInstanceOfType(TypeDescriptor.GetConverter(type), typeof(NumericConverter));
            }
        }

        [TestMethod]
        public void GetConverterTest_列挙型()
        {
            var converter = TypeDescriptor.GetConverter(typeof(EnumA));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumB));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumC));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumD));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumE));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumF));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumG));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumH));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));

            converter = TypeDescriptor.GetConverter(typeof(EnumI));
            Assert.IsInstanceOfType(converter, typeof(EnumConverter));
        }

        [TestMethod]
        public void GetConverterTest_継承関係()
        {
            var objectConverter = new TypeConverter();
            TypeDescriptor.AttachConverter(typeof(object), objectConverter);
            var aConverter = new TypeConverter();
            TypeDescriptor.AttachConverter(typeof(ClassA), aConverter);
            var bConverter = new TypeConverter();
            //TypeDescriptor.AttachConverter(typeof(ClassB), bConverter);
            var cConverter = new TypeConverter();
            TypeDescriptor.AttachConverter(typeof(ClassC), cConverter);
            var dConverter = new TypeConverter();
            //TypeDescriptor.AttachConverter(typeof(ClassD), dConverter);
            var eConverter = new TypeConverter();
            TypeDescriptor.AttachConverter(typeof(ClassE), eConverter);

            var result = TypeDescriptor.GetConverter(typeof(object));
            Assert.AreEqual(result, objectConverter);

            result = TypeDescriptor.GetConverter(typeof(ClassA));
            Assert.AreEqual(result, aConverter);

            result = TypeDescriptor.GetConverter(typeof(ClassB));
            Assert.AreEqual(result, aConverter);

            result = TypeDescriptor.GetConverter(typeof(ClassC));
            Assert.AreEqual(result, cConverter);

            result = TypeDescriptor.GetConverter(typeof(ClassD));
            Assert.AreEqual(result, cConverter);

            result = TypeDescriptor.GetConverter(typeof(ClassE));
            Assert.AreEqual(result, eConverter);
        }

        [TestMethod]
        public void GetConverterTest_異常系()
        {
            //null
            try
            {
                TypeDescriptor.GetConverter(null);
                Assert.Fail();
            }
            catch (ArgumentNullException)
            {
            }
        }

        [TestMethod]
        public void AttachConverterTest()
        {
            TypeDescriptor.AttachConverter(typeof(object), new TypeConverter());
        }

        [TestMethod]
        public void DetachConverterTest()
        {
            TypeDescriptor.DetachConverter(typeof(object));
        }
    }
}
