﻿using System;
using System.Windows.Input;

namespace Katoributa.Net.Platform.Mvvm.Commands
{
    /// <summary>
    /// 引数の無い汎用コマンドです。
    /// </summary>
    /// <remarks>
    /// Livetのコードを流用、プロパティ変更通知処理を基底クラスへ移動。
    /// http://ugaya40.net/livet
    /// </remarks>
    public sealed class RelayCommand : CommandBase, ICommand
    {
        /// <summary>
        /// コマンドが実行するAction。
        /// </summary>
        private Action _execute;

        /// <summary>
        /// コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。
        /// </summary>
        private Func<bool> _canExecute;

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        public RelayCommand(Action execute) : this(execute, null) { }

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        /// <param name="canExecute">コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。</param>
        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            if (execute == null) throw new ArgumentNullException("execute");
            _execute = execute;
            _canExecute = canExecute;
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        public void Execute()
        {
            _execute();
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        public bool CanExecute()
        {
            return _canExecute == null ? true : _canExecute();
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <param name="parameter">使用されません。</param>
        void ICommand.Execute(object parameter)
        {
            Execute();
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        /// <param name="parameter">使用されません。</param>
        /// <returns>コマンドが実行可能な場合はtrue。それ以外の場合はfalse。</returns>
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }
    }
}
