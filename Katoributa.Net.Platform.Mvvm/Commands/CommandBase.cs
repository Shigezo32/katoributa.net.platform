﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;

namespace Katoributa.Net.Platform.Mvvm.Commands
{
    /// <summary>
    /// Commandの基底クラスです。
    /// </summary>
    /// <remarks>
    /// Livetのコードを流用、プロパティ変更通知処理とスレッド絡みの動作を変更。
    /// http://ugaya40.net/livet
    /// </remarks>
    public abstract class CommandBase : INotifyPropertyChanged
    {
        private List<WeakReference<EventHandler>> _canExecuteChangedHandlers = new List<WeakReference<EventHandler>>();
        private List<WeakReference<PropertyChangedEventHandler>> _propertyChangedHandlers = new List<WeakReference<PropertyChangedEventHandler>>();

        /// <summary>
        /// コマンドが実行可能かどうかが変化した時に発生します。
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                _canExecuteChangedHandlers.Add(new WeakReference<EventHandler>(value));
            }
            remove
            {
                foreach (var weakReference in _canExecuteChangedHandlers
                    .Where(r =>
                    {
                        EventHandler result;
                        if (r.TryGetTarget(out result) && result == value)
                        {
                            return true;
                        }
                        return false;
                    }).ToList())
                {
                    _canExecuteChangedHandlers.Remove(weakReference);
                }
            }
        }

        /// <summary>
        /// コマンドが実行可能かどうかが変化した時に呼び出されます。
        /// </summary>
        protected void OnCanExecuteChanged()
        {
            foreach (var handlerWeakReference in _canExecuteChangedHandlers.ToList())
            {
                EventHandler result;

                if (handlerWeakReference.TryGetTarget(out result))
                {
                    //UIスレッドで実行
                    var context = SynchronizationContext.Current;
                    context.Post(state => result(this, EventArgs.Empty), null);
                }
                else
                {
                    _canExecuteChangedHandlers.Remove(handlerWeakReference);
                }
            }
        }

        /// <summary>
        /// コマンドが実行可能かどうかが変化した時に発生します。
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                _propertyChangedHandlers.Add(new WeakReference<PropertyChangedEventHandler>(value));
            }
            remove
            {
                foreach (var weakReference in _propertyChangedHandlers
                    .Where(r =>
                    {
                        PropertyChangedEventHandler result;
                        if (r.TryGetTarget(out result) && result == value)
                        {
                            return true;
                        }
                        return false;
                    }).ToList())
                {
                    _propertyChangedHandlers.Remove(weakReference);
                }
            }
        }

        /// <summary>
        /// コマンドが実行可能かどうかが変化した時に呼び出されます。
        /// </summary>
        private void OnPropertyChanged()
        {
            foreach (var handlerWeakReference in _propertyChangedHandlers.ToList())
            {
                PropertyChangedEventHandler result;

                if (handlerWeakReference.TryGetTarget(out result))
                {
                    //UIスレッドで実行
                    var context = SynchronizationContext.Current;
                    context.Post(state => result(this, new PropertyChangedEventArgs("CanExecute")), null);
                }
                else
                {
                    _propertyChangedHandlers.Remove(handlerWeakReference);
                }
            }
        }

        /// <summary>
        /// コマンドが実行可能かどうかが変化したことを通知します。
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            OnPropertyChanged();
            OnCanExecuteChanged();
        }
    }
}
