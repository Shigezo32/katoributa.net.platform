﻿using System;
using System.Windows.Input;

namespace Katoributa.Net.Platform.Mvvm.Commands
{
    /// <summary>
    /// 引数が指定できる汎用コマンドです。
    /// </summary>
    /// <typeparam name="T">Viewから受け取るオブジェクトの型。</typeparam>
    /// <remarks>
    /// Livetのコードを流用、プロパティ変更通知処理を基底クラスへ移動。
    /// http://ugaya40.net/livet
    /// </remarks>
    public sealed class RelayCommand<T> : CommandBase, ICommand
    {
        /// <summary>
        /// コマンドが実行するAction。
        /// </summary>
        private Action<T> _execute;

        /// <summary>
        /// コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。
        /// </summary>
        private Func<T, bool> _canExecute;

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        public RelayCommand(Action<T> execute) : this(execute, null) { }

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        /// <param name="canExecute">コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。</param>
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute)
        {
            if (execute == null) throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <param name="parameter">Viewから渡されたオブジェクト。</param>
        public void Execute(T parameter)
        {
            _execute(parameter);
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        /// <param name="parameter">Viewから渡されたオブジェクト。</param>
        public bool CanExecute(T parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <param name="parameter">Viewから渡されたオブジェクト。</param>
        void ICommand.Execute(object parameter)
        {
            if (parameter == null)
            {
                Execute(default(T));
            }
            else
            {
                Execute((T)parameter);
            }
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        /// <param name="parameter">使用されません。</param>
        /// <returns>コマンドが実行可能な場合はtrue。それ以外の場合はfalse。</returns>
        bool ICommand.CanExecute(object parameter)
        {
            if (parameter == null)
            {
                return CanExecute(default(T));
            }
            else
            {
                return CanExecute((T)parameter);
            }
        }
    }
}
