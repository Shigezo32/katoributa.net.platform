﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Katoributa.Net.Platform.Mvvm.Commands
{
    /// <summary>
    /// 引数の無い非同期で実行する汎用コマンドです。
    /// </summary>
    public sealed class AsyncRelayCommand : CommandBase, ICommand
    {
        /// <summary>
        /// コマンドが実行するAction。
        /// </summary>
        private Func<Task> _execute;

        /// <summary>
        /// コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。
        /// </summary>
        private Func<bool> _canExecute;

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        public AsyncRelayCommand(Func<Task> execute) : this(execute, null) { }

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        /// <param name="canExecute">コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。</param>
        public AsyncRelayCommand(Func<Task> execute, Func<bool> canExecute)
        {
            if (execute == null) throw new ArgumentNullException("execute");
            _execute = execute;
            _canExecute = canExecute;
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <returns>Task。</returns>
        public async Task ExecuteAsync()
        {
            await _execute();
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        public bool CanExecute()
        {
            return _canExecute == null ? true : _canExecute();
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <param name="parameter">使用されません。</param>
        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync();
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        /// <param name="parameter">使用されません。</param>
        /// <returns>コマンドが実行可能な場合はtrue。それ以外の場合はfalse。</returns>
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }
    }
}
