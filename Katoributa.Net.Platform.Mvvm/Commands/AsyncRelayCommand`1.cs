﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Katoributa.Net.Platform.Mvvm.Commands
{
    /// <summary>
    /// 引数が指定できる非同期で実行する汎用コマンドです。
    /// </summary>
    /// <typeparam name="T">Viewから受け取るオブジェクトの型。</typeparam>
    public sealed class AsyncRelayCommand<T> : CommandBase, ICommand
    {
        /// <summary>
        /// コマンドが実行するAction。
        /// </summary>
        private Func<T, Task> _execute;

        /// <summary>
        /// コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。
        /// </summary>
        private Func<T, bool> _canExecute;

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        public AsyncRelayCommand(Func<T, Task> execute) : this(execute, null) { }

        /// <summary>
        /// インスタンスを初期化します。
        /// </summary>
        /// <param name="execute">コマンドが実行するAction。</param>
        /// <param name="canExecute">コマンドが実行可能かどうかをあらわすFunc&lt;bool&gt;。</param>
        public AsyncRelayCommand(Func<T, Task> execute, Func<T, bool> canExecute)
        {
            if (execute == null) throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <param name="parameter">Viewから渡されたオブジェクト。</param>
        /// <returns>Task。</returns>
        public async Task ExecuteAsync(T parameter)
        {
            await _execute(parameter);
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        /// <param name="parameter">Viewから渡されたオブジェクト。</param>
        public bool CanExecute(T parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <param name="parameter">Viewから渡されたオブジェクト。</param>
        async void ICommand.Execute(object parameter)
        {
            if (parameter == null)
            {
                await ExecuteAsync(default(T));
            }
            else
            {
                await ExecuteAsync((T)parameter);
            }
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します。
        /// </summary>
        /// <param name="parameter">使用されません。</param>
        /// <returns>コマンドが実行可能な場合はtrue。それ以外の場合はfalse。</returns>
        bool ICommand.CanExecute(object parameter)
        {
            if (parameter == null)
            {
                return CanExecute(default(T));
            }
            else
            {
                return CanExecute((T)parameter);
            }
        }
    }
}
