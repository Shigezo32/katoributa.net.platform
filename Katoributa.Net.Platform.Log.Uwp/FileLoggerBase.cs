﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Katoributa.Net.Platform.Core;
using Windows.Storage;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ログをファイルに出力する基底クラス。
    /// </summary>
    public abstract class FileLoggerBase : LoggerBase
    {
        /// <summary>
        /// ロックの管理者。
        /// </summary>
        private static readonly ReaderWriterLockManager _lockManager = new ReaderWriterLockManager();

        /// <summary>
        /// FileLoggerBaseのインスタンスを初期化します。
        /// </summary>
        public FileLoggerBase()
        {
            _fileProhibitedCharacters = Path.GetInvalidFileNameChars();
            var folderProhibitedCharacters = Path.GetInvalidPathChars().ToList();
            folderProhibitedCharacters.Add(':');
            folderProhibitedCharacters.Add('*');
            folderProhibitedCharacters.Add('?');
            _folderProhibitedCharacters = folderProhibitedCharacters;
            this.Encoding = new UTF8Encoding(false);
        }

        #region プロパティ

        /// <summary>
        /// ファイル名の禁止文字。
        /// </summary>
        protected virtual IReadOnlyList<char> FileProhibitedCharacters
        {
            get { return _fileProhibitedCharacters; }
        }
        private IReadOnlyList<char> _fileProhibitedCharacters;

        /// <summary>
        /// フォルダー名の禁止文字。
        /// </summary>
        public IReadOnlyList<char> FolderProhibitedCharacters
        {
            get { return _folderProhibitedCharacters; }
        }
        private IReadOnlyList<char> _folderProhibitedCharacters;

        /// <summary>
        /// 拡張子を除いたログのファイル名。
        /// </summary>
        /// <remarks>y～yyyy,M,MM,d～ddd,H,HH,h,hh,m,mm,s,ssはシステム日時に変換します。</remarks>
        /// <exception cref="ArgumentException">指定の文字列が禁止文字を含むか、null、空、空白のいずれかである場合に発生する例外。</exception>
        public string FileNameWithoutExtension
        {
            get { return this._fileNameWithoutExtension; }
            set
            {
                this.ValidateFileName(value, "value");
                this._fileNameWithoutExtension = value;
            }
        }
        private string _fileNameWithoutExtension = "yyyyMMdd";

        /// <summary>
        /// ログの拡張子。
        /// </summary>
        /// <exception cref="ArgumentException">指定の文字列が禁止文字を含むか、null、空、空白のいずれかである場合に発生する例外。</exception>
        public string FileExtension
        {
            get { return this._fileExtension; }
            set
            {
                this.ValidateFileName(value, "value");
                if (value.StartsWith("."))
                {
                    this._fileExtension = value;
                }
                else
                {
                    this._fileExtension = string.Format(".{0}", value);
                }
            }
        }
        private string _fileExtension = ".log";

        /// <summary>
        /// ファイルの出力先。
        /// </summary>
        /// <remarks>ms-appdata:///local/を指定した場合はローカルフォルダ、ms-appdata:///roaming/はローミングフォルダ、ms-appdata:///temp/は一時フォルダ、da-document:///はドキュメントフォルダをルートフォルダとしてファイルを出力します。任意のフォルダ名をスラッシュ区切りで続けることで、ルートフォルダ配下にフォルダを作成します。指定した値の末尾がスラッシュでなかった場合はスラッシュを付与して設定します。</remarks>
        /// <exception cref="ArgumentException">指定の文字列が禁止文字を含む、またはnull、空、空白のいずれか、出力不可ディレクトリである場合に発生する例外。</exception>
        public string OutputFolder
        {
            get { return this._outputFolder; }
            set
            {
                this.ValidateOutputFolder(value, "value");
                if (value.EndsWith("/"))
                {
                    this._outputFolder = value;
                }
                else
                {
                    this._outputFolder = string.Format("{0}/", value);
                }
            }
        }
        private string _outputFolder = string.Format("{0}Logs/", OutputDestination.Local.ObtainOutputDestination());

        /// <summary>
        /// 出力するメッセージのEncodingを指定します。
        /// </summary>
        public Encoding Encoding { get; set; }

        #endregion

#pragma warning disable 4014
        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        protected override void OnLog(LogLevel logLevel, object message, Exception exception)
        {
            OnLogAsync(logLevel, message, exception);
        }

        /// <summary>
        /// ログレベル、メッセージ、例外を指定してログを出力します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>Task。</returns>
        protected override async Task OnLogAsync(LogLevel logLevel, object message, Exception exception)
        {
            if (CanWriteLog(logLevel))
            {
                var outputText = this.CreateOutputText(logLevel, message, exception);
                var dateTime = DateTime.Now;
                await this.OutputLogFileAsync(outputText, this.ConvertFormatToSystemDate(this.FileNameWithoutExtension, dateTime), this.OutputFolder);
            }
        }

        /// <summary>
        /// 出力用のメッセージを作成します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>出力用のメッセージ。</returns>
        protected abstract string CreateOutputText(LogLevel logLevel, object message, Exception exception);

        /// <summary>
        /// ログファイルを出力します。
        /// </summary>
        /// <param name="message">出力用のメッセージ。</param>
        /// <param name="fileName">ファイル名。</param>
        /// <param name="folderPath">フォルダーのパス。</param>
        /// <returns>Task。</returns>
        /// <exception cref="Exception">ファイル出力処理で発生した例外。ThrowIfExceptionOccured=trueの場合に発生します。</exception>
        private async Task OutputLogFileAsync(string message, string fileName, string folderPath)
        {
            try
            {
                var outputFileName = string.Format("{0}{1}", fileName, this.FileExtension);
                var readerWriterLock = _lockManager.GetLock(outputFileName);
                using (var lockObject = await readerWriterLock.EnterWriteLockAsync())
                {
                    var folder = await this.CreateFolderOrOpenIfExistsAsync(folderPath);
                    //HACK:Shigeta ファイルのキャッシュの仕組みを検討
                    var file = await folder.CreateFileAsync(outputFileName, CreationCollisionOption.OpenIfExists);
                    using (var stream = await file.OpenStreamForWriteAsync())
                    using (var writer = new StreamWriter(stream, this.Encoding))
                    {
                        stream.Seek(0, SeekOrigin.End);
                        writer.WriteLine(message);
                        await writer.FlushAsync();
                    }
                }
            }
            catch
            {
                if (this.ThrowIfExceptionOccured)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 指定したパスのフォルダーを作成します。同じ名前のフォルダーが既に存在する場合は、既存の項目を返します。
        /// </summary>
        /// <param name="folderPath">作成するフォルダーのパス。</param>
        /// <returns>作成されたフォルダー。</returns>
        private Task<StorageFolder> CreateFolderOrOpenIfExistsAsync(string folderPath)
        {
            var uri = new Uri(folderPath);
            var path = folderPath.Replace(string.Format("{0}:///", uri.Scheme), "");
            var folderNames = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            StorageFolder rootFolder = null;
            if (folderPath.StartsWith(OutputDestination.Local.ObtainOutputDestination()))
            {
                rootFolder = ApplicationData.Current.LocalFolder;
                folderNames.RemoveAt(0);
            }
            else if (folderPath.StartsWith(OutputDestination.Roaming.ObtainOutputDestination()))
            {
                rootFolder = ApplicationData.Current.RoamingFolder;
                folderNames.RemoveAt(0);
            }
            else if (folderPath.StartsWith(OutputDestination.Temp.ObtainOutputDestination()))
            {
                rootFolder = ApplicationData.Current.TemporaryFolder;
                folderNames.RemoveAt(0);
            }
            else if (folderPath.StartsWith(OutputDestination.Document.ObtainOutputDestination()))
            {
                rootFolder = KnownFolders.DocumentsLibrary;
            }
            return this.CreateFolderOrOpenIfExistsAsync(rootFolder, folderNames);
        }

        /// <summary>
        /// 指定したパスのフォルダーを作成します。同じ名前のフォルダーが既に存在する場合は、既存の項目を返します。
        /// </summary>
        /// <param name="rootFolder">ルートフォルダー。</param>
        /// <param name="folderNames">フォルダの名前の一覧。</param>
        /// <returns>作成されたフォルダー。</returns>
        private async Task<StorageFolder> CreateFolderOrOpenIfExistsAsync(StorageFolder rootFolder, IList<string> folderNames)
        {
            var outputFolder = rootFolder;
            for (int i = 0; i < folderNames.Count; i++)
            {
                outputFolder = await outputFolder.CreateFolderAsync(folderNames[i], CreationCollisionOption.OpenIfExists);
            }
            return outputFolder;
        }

        /// <summary>
        /// 指定した文字列に含まれる日付フォーマットをシステム日時に変換します。
        /// </summary>
        /// <param name="target">変換対象の文字列。</param>
        /// <param name="dateTime">システム日時。</param>
        /// <returns>日付フォーマットをシステム日付に変換した文字列。</returns>
        private string ConvertFormatToSystemDate(string target, DateTime dateTime)
        {
            var convertText = target;
            foreach (var format in Enum.GetValues(typeof(ConvertDateFormat)))
            {
                var dateFormat = format.ToString();
                if (convertText.Contains(dateFormat))
                {
                    //他の文字列を含まないと変換結果が変わる書式があるため、先頭に禁止文字を加えて変換後に除去する。
                    var provisional = dateTime.ToString(string.Format("{0}{1}", '*', dateFormat));
                    convertText = convertText.Replace(dateFormat, provisional.TrimStart('*'));
                }
            }
            return convertText;
        }

        /// <summary>
        /// 指定の文字列に禁止文字が含まれているかを判定します。
        /// </summary>
        /// <param name="target">判定文字列。</param>
        /// <param name="prohibitedCharacters">禁止文字のリスト。</param>
        /// <returns>禁止文字が含まれているかどうか。指定した文字列が禁止文字を含む場合にtrueを返します。</returns>
        private bool HasProhibitedCharacters(string target, IEnumerable<char> prohibitedCharacters)
        {
            return target.Any(x => prohibitedCharacters.Contains(x));
        }

        /// <summary>
        /// 指定の文字列がファイル名として有効か検証します。
        /// </summary>
        /// <param name="value">判定する文字列。</param>
        /// <param name="paramName">例外の原因となったパラメーターの名前。</param>
        /// <exception cref="ArgumentNullException">指定の文字列がnullの場合に発生します。</exception>
        /// <exception cref="ArgumentException">指定の文字列が禁止文字を含むか、空、空白のいずれかである場合に発生します。</exception>
        private void ValidateFileName(string value, string paramName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName);
            }
            else if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(string.Format("空もしくは空白文字が指定されています。value : {0}", value), paramName);
            }
            else if (this.HasProhibitedCharacters(value, this.FileProhibitedCharacters))
            {
                throw new ArgumentException(string.Format("禁止文字を含む文字列が指定されています。value : {0}", value), paramName);
            }
        }

        /// <summary>
        /// 指定の文字列が出力フォルダとして有効か検証します。
        /// </summary>
        /// <param name="value">パス。</param>
        /// <param name="paramName">変数の名前。</param>
        /// <exception cref="ArgumentNullException">指定の文字列がnullの場合に発生します。</exception>
        /// <exception cref="ArgumentException">指定の文字列が禁止文字を含むか、空、空白のいずれかである場合に発生します。</exception>
        private void ValidateOutputFolder(string value, string paramName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName);
            }
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(string.Format("空もしくは空白文字が指定されています。value : {0}", value), paramName);
            }
            if (!this.ValidateFolderPath(value))
            {
                throw new ArgumentException(string.Format("指定したファイルの出力先のパスが不正です。value : {0}", value), paramName);
            }

            var uri = new Uri(value);
            var folder = value.Replace(string.Format("{0}:///", uri.Scheme), "");
            if (this.HasProhibitedCharacters(folder, this.FolderProhibitedCharacters))
            {
                throw new ArgumentException(string.Format("禁止文字を含む文字列が指定されています。value : {0}", value), paramName);
            }
        }

        /// <summary>
        /// 出力できるパスかどうかを検証します。
        /// </summary>
        /// <param name="path">パス。</param>
        /// <returns>出力できる場合はtrue、出力できない場合はfalse。</returns>
        protected virtual bool ValidateFolderPath(string path)
        {
            if (path.StartsWith(OutputDestination.Local.ObtainOutputDestination())
                || path.StartsWith(OutputDestination.Roaming.ObtainOutputDestination())
                || path.StartsWith(OutputDestination.Temp.ObtainOutputDestination())
                || path.StartsWith(OutputDestination.Document.ObtainOutputDestination()))
            {
                return true;
            }
            return false;
        }
    }
}