﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ログをCSVファイルに出力するクラス。
    /// </summary>
    public class CsvFileLogger : FileLoggerBase
    {
        /// <summary>
        /// CsvFileLoggerのインスタンスを初期化します。
        /// </summary>
        public CsvFileLogger()
        {
            this.FileExtension = ".csv";
            this.Encoding = Encoding.UTF8;
        }

        /// <summary>
        /// 出力用のメッセージを作成します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>出力用のメッセージ。</returns>
        protected override string CreateOutputText(LogLevel logLevel, object message, Exception exception)
        {
            var dateTime = DateTime.Now.ToString(this.SystemDateTimeFormat);
            var logLevelText = logLevel.ToString().ToUpper();
            var sb = new StringBuilder();
            sb.Append(EnclosedInDoubleQuotes(dateTime));
            sb.Append(",");
            sb.Append(EnclosedInDoubleQuotes(logLevelText));
            sb.Append(",");
            sb.Append(EnclosedInDoubleQuotes(message));
            sb.Append(",");
            sb.Append(EnclosedInDoubleQuotes(exception));
            return sb.ToString();
        }

        /// <summary>
        /// 指定した値をダブルクオーテーションで囲った文字列を返します。
        /// </summary>
        /// <param name="value">値。</param>
        private string EnclosedInDoubleQuotes(object value)
        {
            if (value == null)
            {
                return @"""""";
            }
            return string.Format(@"""{0}""", value.ToString().Replace(@"""", @""""""));
        }
    }
}