﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ログをファイルに出力するクラス。
    /// </summary>
    public class FileLogger : FileLoggerBase
    {
        /// <summary>
        /// FileLoggerのインスタンスを初期化します。
        /// </summary>
        public FileLogger()
        {
        }

        /// <summary>
        /// 出力用のメッセージを作成します。
        /// </summary>
        /// <param name="logLevel">ログレベル。</param>
        /// <param name="message">メッセージ。</param>
        /// <param name="exception">例外。</param>
        /// <returns>出力用のメッセージ。</returns>
        protected override string CreateOutputText(LogLevel logLevel, object message, Exception exception)
        {
            var dateTime = DateTime.Now.ToString(this.SystemDateTimeFormat);
            var logLevelText = logLevel.ToString().ToUpper();
            if (exception == null)
            {
                return string.Format("{0} {1} {2}", dateTime, logLevelText, message);
            }
            else
            {
                return string.Format("{0} {1} {2} {3}", dateTime, logLevelText, message, exception);
            }
        }
    }
}