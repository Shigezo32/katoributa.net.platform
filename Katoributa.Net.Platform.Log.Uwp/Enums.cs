﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// ファイルの出力先。
    /// </summary>
    public enum OutputDestination
    {
        /// <summary>
        /// ローカルフォルダ
        /// </summary>
        Local,

        /// <summary>
        /// ローミングフォルダ
        /// </summary>
        Roaming,

        /// <summary>
        /// 一時フォルダ
        /// </summary>
        Temp,

        /// <summary>
        /// ドキュメントフォルダ
        /// </summary>
        Document,
    }
}