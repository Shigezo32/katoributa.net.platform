﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katoributa.Net.Platform.Log
{
    /// <summary>
    /// 列挙型に対して追加する便利機能群です。
    /// </summary>
    internal static class EnumsExtension
    {
        /// <summary>
        /// ファイルの出力先のパスを取得します。
        /// </summary>
        /// <param name="value">ファイルの出力先。</param>
        /// <returns>ファイルの出力先のパス。</returns>
        internal static string ObtainOutputDestination(this OutputDestination value)
        {
            switch (value)
            {
                case OutputDestination.Local:
                    return "ms-appdata:///local/";
                case OutputDestination.Roaming:
                    return "ms-appdata:///roaming/";
                case OutputDestination.Temp:
                    return "ms-appdata:///temp/";
                case OutputDestination.Document:
                    return "kn-document:///";
                default:
                    return null;
            }
        }
    }
}